########################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

DESTDIR ?= /
PREFIX ?= usr/local

CFLAGS ?= -O2 -pedantic -Wall -Wextra -Wfatal-errors -D_REENTRANT -fPIC
LDFLAGS ?=
LDFLAGS += -lm
WIDOBJS =
MAINOBJS =

DEBUG ?= 0
ENABLE_X11 ?= 1
ENABLE_CONTROL ?= 1
ENABLE_IMAGES ?= 1

ifeq (1, $(DEBUG))
CFLAGS = -ggdb -pedantic -Wall -Wextra -Wfatal-errors -fno-omit-frame-pointer -O0 -DDEBUG
endif

ifeq (1, $(ENABLE_X11))
CFLAGS += -DENABLE_X11
LDFLAGS += -L/usr/X11R6/lib -lX11
WIDOBJS += \
	src/widget.o \
	src/exec.o \
	src/widgets/button.o \
	src/widgets/input.o \
	src/widgets/page.o \
	src/widgets/label.o \
	src/widgets/checkbox.o \
	src/widgets/trans.o

ifeq (1, $(ENABLE_CONTROL))
CFLAGS += -DENABLE_CONTROL
LDFLAGS += -lXtst
endif
ifeq (1, $(ENABLE_IMAGES))
CFLAGS += -DENABLE_IMAGES
LDFLAGS += $(shell pkg-config --cflags --libs imlib2)
endif
endif

MAINOBJS += \
	src/dectypes.o \
	src/debug.o \
	src/sourcedriver.o \
	src/tokenizer.o \
	src/parser.o \
	src/evaluator.o \
	src/main.o \
	src/syntax.o

all: guish

guish: $(MAINOBJS) $(WIDOBJS)
	$(CC) -o guish $(CFLAGS) $(MAINOBJS) $(WIDOBJS) $(LDFLAGS)

src/dectypes.o: src/dectypes.c src/dectypes.h src/cutils.h
src/debug.o: src/debug.c src/debug.h src/dectypes.h src/tokenizer.h src/evaluator.h
src/main.o: src/main.c src/main.h src/cutils.h src/dectypes.h src/parser.h src/evaluator.h src/sourcedriver.h src/tokenizer.h src/syntax.h src/debug.h src/widget.h src/exec.h
src/evaluator.o: src/evaluator.c src/evaluator.h src/parser.h src/widgets.h src/main.h src/dectypes.h src/cutils.h src/sourcedriver.h src/tokenizer.h src/syntax.h src/debug.h src/exec.h
src/parser.o: src/parser.c src/parser.h src/evaluator.h src/dectypes.h src/cutils.h src/tokenizer.h src/debug.h
src/tokenizer.o: src/tokenizer.c src/tokenizer.h src/dectypes.h src/evaluator.h src/main.h src/sourcedriver.h  src/debug.h
src/sourcedriver.o: src/sourcedriver.c src/sourcedriver.h src/dectypes.h src/tokenizer.h src/main.h src/debug.h
src/syntax.o: src/syntax.c src/syntax.h src/dectypes.h

src/widget.o: src/widget.c src/widget.h src/widgets.h src/dectypes.h src/syntax.h src/evaluator.h src/debug.h src/main.h
src/exec.o: src/exec.c src/exec.h src/widgets.h src/dectypes.h src/tokenizer.h src/sourcedriver.h src/syntax.h src/evaluator.h src/debug.h
src/widgets/button.o: src/widgets/button.c src/widgets/button.h src/dectypes.h
src/widgets/input.o: src/widgets/input.c src/widgets/input.h src/syntax.h src/dectypes.h src/evaluator.h
src/widgets/page.o: src/widgets/page.c src/widgets/page.h src/dectypes.h
src/widgets/label.o: src/widgets/label.c src/widgets/label.h src/dectypes.h
src/widgets/checkbox.o: src/widgets/checkbox.c src/widgets/checkbox.h src/dectypes.h src/evaluator.h
src/widgets/trans.o: src/widgets/trans.c src/widgets/trans.h src/dectypes.h

.PHONY: install
install: all
	mkdir -p $(DESTDIR)/$(PREFIX)/bin && \
	mkdir -p $(DESTDIR)/$(PREFIX)/share/man/man1 && \
	mkdir -p $(DESTDIR)/$(PREFIX)/share/doc/guish && \
	cp guish $(DESTDIR)/$(PREFIX)/bin && \
	chmod 755 $(DESTDIR)/$(PREFIX)/bin/guish && \
	cp man/guish.1 $(DESTDIR)/$(PREFIX)/share/man/man1 && \
	cp README.md $(DESTDIR)/$(PREFIX)/share/doc/guish && \
	cp changelog.md $(DESTDIR)/$(PREFIX)/share/doc/guish && \
	cp COPYING $(DESTDIR)/$(PREFIX)/share/doc/guish && \
	cp -R examples $(DESTDIR)/$(PREFIX)/share/doc/guish

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)/$(PREFIX)/bin/guish \
	rm -f $(DESTDIR)/$(PREFIX)/share/man/man1/guish.1

.PHONY: tests
tests:
	sh tests/unit.sh
	sh tests/functional.sh
	sh tests/leaks.sh

clean:
	rm -f src/*.o src/widgets/*.o guish
