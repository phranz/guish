# NAME

guish - A language to make and modify GUIs

# SYNOPSIS

**guish** \[**-c**
\<code\>\]\[**-s**\]\[**-q**\]\[**-k**\]\[**-t**\]\[**-f**\]\[**-v**\]\[**-b**\]\[**-h**\]\[\<file\>\]\[\<arguments\>\]

# DESCRIPTION

**guish** is a language to create and modify GUIs; it can be used to tie
different programs together by embedding, or to make simple GUIs.

This is version 2.x, which depends just on Xlib as all toolkits have
been removed (there are some optional dependencies).

# OPTIONS

**-c \<code\>**

:   read and execute commands from command line.

**-s**

:   display elements when created.

**-q**

:   quit when closing last element/window.

**-k**

:   terminate all external programs when quitting.

**-t**

:   fallback on tty after reading data from other inputs.

**-f**

:   show available X11 fonts.

**-v**

:   show version.

**-b**

:   show build (compiled in) options.

**-h**

:   guess.

# INPUTS AND SOURCES

guish reads commands from multiple source sequentially; these source
are: command line (with **-c** option), file, standard input, and if
**-t** option is given, controlling terminal. After reading commands
from a source, it tries to switch to another one, and when there are no
more sources, it simply stays idle.

After executing from file or command line or standard input, then
finally it switchs to controlling terminal (if **-t** option is given)
and executes from there.

If the file given is a named pipe, then it will be opened in
non-blocking mode, allowing to issue commands from the other end of the
pipe.

# SYNTAX OVERVIEW

Being a command interpreter, guish has a little set of syntax rules.
They comprises expressions, commands, quotes and special operators.
Generic commands and signals are common to all elements, while there are
ones which are specific to each element. A phrase is the execution unit,
and ends if the program encounters a newline-like character, or a
semicolon (\";\").

## Comments

Single line comments begin with a **\#**, and end at newline like
character:

     a = 4 # this is a comment

Multiline comments instead are embedded inside **#\[** and **\]#** (or
end at the end of source):

     a = 4 #[ This,
     is a multiline comment,
     ending here. ]# puts "a: @{a}"

## Elements and element expressions

Elements are basically widgets and have some commands and signals
associated with them; they can be created using element expressions,
enclosing element name within **\|\|**:

     |b|+

In this example, \"\|b\|\" is an expression which creates an element of
type button, and returns its X11 window id (ex, \"65011718\").

The **+** command will then show the button (all elements are hidden by
default unless **-s** option is given).

## Subject and implied subject

To refer to the last created/modified element, you can use it without
naming it explicitly, for example:

     |i|;+

The \"+\" command will by applied to element created by \"\|i\|\"
expression automatically.

## Variables and variable substitution

We can refer to elements by using variable substitution, instead of
using their window ids:

     bt = |b|

and then:

     @bt < "new button"

## Commands

Commands instead can be of three distinct types: special, generic and
normal.

Special commands are meant to change guish behaviour, generic ones are
meant to modify an element of any type and normal ones are for a
specific element type (every element can have specialized commands).

## Signals

Signals make it possible to run actions on UI changes (or, more
generally, on some events).

     |b|=>c{run free}

Here, the user creates a button that when clicked will make guish
execute a system command (free in this case) (see **run** in SPECIAL
COMMANDS).

     |b|<generator =>c{|b|<clone}

A button which is a factory of buttons (run this with **-s** option).

Here the evaluation of the code block is done when the click signal is
triggered.

## Scopes and closures

All variables (functions too, as blocks can be assigned to variables)
have a specific scope:

     a = 1
     {b = 2}()
     puts "@{a}:@{b}"

Here the variable \"a\" is defined in the global scope, while \"b\" is
defined in a temporarily block scope; hence just \"a\" is printed to
stdout.

When accessing reading/defining a variable in a local scope, if the
variable is already defined in the enclosing scope then that variable is
picked to be read/modified:

     a = 1
     {
         a = 5
         puts@a
     }()
     puts@a

Here \"a\" is defined in the global scope, then modified from block
scope and printed from there, and its value doesn\'t change.

If a variable is defined inside a block, then all embedded blocks that
reference that variable will get its value at definition time (a
closure):

     {
         a = 1
         return {
             puts @a
         }
     }()()

## Quotes, blocks and functions

There are single and double quoting: anything embedded inside single
quotes \'\', is treated literally (no escaping takes place) and as a
single token.

Variable and function interpolations (respectively via \"**\@{\...}**\"
and \"**@(\...)**\") are used inside double quotes \"\", external
command substitution quotes \`\` and external window id substitution
**\<(\...)**.

Escaping (\"\\n\\t\\r\\f\\v\\b\") takes place only inside double quotes
\"\".

     a = 'my string'; puts "this is:	 @{a}"
     puts "sum of 1 + 5 is: @(add(1, 5))"

Anything embedded inside **{}** is treated as a code block and no
variable substitution is done at definition time.

To \"execute\" a block, simply use parens **()**. If arguments are
given, they can be referenced inside block by using **\@n**, where \"n\"
is a number which represents a specific argument by position, with
indexes starting at 1 (works with command line parameters too). To refer
to all arguments (at once) given to the block use **@\*** operator (when
in string interpolation, argument separator is a space).

     v = myvar
     a = {puts "here is @{v}"}
     a()

Here the variable \"v\" is not substituted when assigning the block to
\"a\"; it\'s substituted later, when function \"a\" is called.

     {
         puts join("string length is: ", len(@1))
     }("home")

Here instead, the block is defined and executed immediately, using
\"home\" as the first argument.

Being a block a piece of code, it can be used to define functions by
simply being assigned to a variable:

     fn = {return add(@1, @2)}
     puts join("my func res:", fn(2, 4)) 

In addition, when defining a new function, it\'s possible to
\"overwrite\" the builtin functions in the current scope.

When returning from a function, instead, it\'s possible to return
multiple arguments (the remaining phrase) to the caller:

     fn = {return 1 2 3}
     puts join(fn())

## Conditionals

Anything equal to 0, \"0\" or empty (like **\'\'**, **\"\"**, **{}**) is
false, true otherwise. This is useful with **if** and **else** commands:

     if 1 { |b|<text } else { |b|<neverhere }

or

     if ge(@a,4) { |l|<4 }

## Loops and iteration

Here an example of a **while** loop:

     a = 1
     after 4 {a = 0}
     while {eq(@a, 1)} {
         wait 1 puts 'true'
     }
     puts 'end'

And here another one using **each** function:

     each({puts @1}, 1, 2, 3, 4, 5})

And another one of a **for** loop:

     for x in 1 2 3 4: { puts @x }

## Tail recursion optimization (TCO)

Since **2.2.1** version, guish supports tail recursion optimization too,
effectively turning a tail recursive function into a loop:

     upto = {
         if gt(@1, @2) {
             return
         }
         puts @1
         return upto(add(@1, 1), @2)
     }
     upto(1, 7)

To use TCO, the last phrase of a function must use **return** command
directly (not inside another block like if-else).

## External window id substitution

Everything inside **\<( )** is recognized as an external command (X11
GUI), forked and executed, and its window id is substituted (using
\_NET_WM_PID).

Note that this method will not work with programs that fork.

For example:

     a = <(xterm)

xterm program is spawn, and can be driven (almost) like a normal
element.

## External command substitution

Everything inside **\`\`** is recognized as an external command and
executed. Then the command output (STDOUT) is substituted inside source
code and interpreted after that.

For example:

     |b|<`echo clickme`

## Slicing

Anything inside **\[\]** is trated as a slice expression. The usage is
**\[\<n\>\[,\<x\>\]\]**, when \<n\> is an index, and \<x\> is an
optional end index (always inclusive); if the preceding argument is a
regular token, then the expression will return its characters as
specified by index(es), otherwise if it\'s a block, the expression will
return its tokens:

     puts {1, 2, 3, 4, {a, b}, cat}[4][1]

The output of this example is \'b\'.

If the index(es) given are out of range, an empty token is returned.

# SPECIAL VARIABLES

**SW**

:   primary screen width, in pixels.

**SH**

:   primary screen height, in pixels.

**X**

:   pointer\'s x coord.

**Y**

:   pointer\'s y coord.

**self**

:   variable holding the window id when in signal code.

**args**

:   refers to the block in which there are positional arguments and
    function arguments (alternative syntax).

**FILE**

:   variable holding the path of current source file.

**LINE**

:   variable holding current line number at \"that\" point in source
    code.

# ENVIRONMENT VARIABLES

**GUISH_MAXWIDWAIT**

:   the maximum number of seconds to wait when applying external window
    id substitution (defaults to 3 seconds)

# ELEMENTS

Available elements are:

**b**

:   A button.

**i**

:   An input box.

**l**

:   A label.

**p**

:   A page (container of other elements; show and hide are applied to
    all subelements).

**c**

:   A checkbox.

**t**

:   A label with a totally transparent background (requires a
    compositor).

# SPECIAL COMMANDS

Special commands are not tied to elements, they are like statements.

**=\> \<signal\> \<subcmd\>**

:   register a sub-command \<subcmd\> to run when \<signal\> triggers.
    For normal signals (eg. signals tied to elements), there must exist
    an implied subject.

**!\> \<signal\>**

:   unregister a sub-command \<subcmd\> previously registered on signal
    \<signal\>.

**q**

:   quit guish (exit status 0).

**exit \<status\>**

:   quit guish (exit status \<status\>).

**cd \<path\>**

:   change current working directory using \<path\>.

**run \<cmd\>**

:   execute shell command \<cmd\>.

**puts \[\<\...\>\]**

:   prints remaining phrase to stdout with a newline added.

**p \[\<\...\>\]**

:   prints remaining phrase to stdout.

**e \[\<\...\>\]**

:   prints remaining phrase to stderr.

**unset \<name\|num\|wid\> \[\<attr\>\]**

:   unsets a variable (\<name\>) or timed scheduled procedure registered
    with \<num\>, see **every** command. If, instead of \<name\|num\>,
    an element id \<wid\> is given and a name attribute \<attr\> is
    given, deletes that element attribute.

**source \<file\>**

:   execute commands from file.

**vars \[\<wid\>\]**

:   shows all variables present in the current scope or, if \<wid\>
    element id is given, shows all element attributes (uses stderr).

**ls**

:   display all existing widgets (stderr).

**del \<wid\>**

:   delete a widget with id \<wid\>.

**every \<seconds\> \<block\>**

:   schedule \<code\> to run after \<seconds\> seconds are elapsed.

**after \<seconds\> \<block\>**

:   schedule \<block\> to run once after \<seconds\> seconds are
    elapsed.

**wait \<seconds\>**

:   stop command execution and wait \<seconds\> seconds before resuming
    (accepts decimal values too). XEvent handling, schedules actions and
    signal execution are unaffected by this option.

**if \<condition\> \<block\>**

:   executes \<block\> if condition evaluates to true (see
    Conditionals).

**unless \<condition\> \<block\>**

:   executes \<block\> if condition evaluates to false (see
    Conditionals).

**else \<block\>**

:   executes \<block\> if last conditional command was successful (see
    Conditionals).

**return \<phrase\>**

:   when used inside a function, returns all its arguments (the
    remaining phrase) to the caller.

**while \<condition\> \<block\>**

:   executes \<block\> until \<condition\> evaluates to true (see
    Conditionals).

**until \<condition\> \<block\>**

:   executes \<block\> until \<condition\> evaluates to true (see
    Conditionals).

**for \[\<var1\>, \<var2\>, \...\] in \[\<val1\>, \<val2\>, \...\] : \<block\>**

:   executes the block \<block\> for each group of variables.

**break**

:   exit from current loop.

**rel \<element1\> \<element2\> \<alignment\>**

:   relates \<element1\> to \<element2\>, moving \<element1\> near
    \<element2\> using \<alignment\> (see alignment in STYLE AND
    ATTRIBUTES section, as \<alignment\> opts are similar) every time
    \<element2\> is moved. If \"0\" is specified as alignment, the
    relation is deleted.

**pass \[\<args\>\]**

:   do nothing, consuming remaining arguments.

**send \<wid\> \<keysequence\>**

:   send a keysequence to an element whose id si \<wid\> (must be
    enabled at compilation time).

**ctrl \<wid\> \<keysequence\>**

:   send control key sequence to an element whose id si \<wid\> (must be
    enabled at compilation time).

# GENERIC COMMANDS

Generic commands are applicable to any element, regardless of its type
(there are exceptions though).

**G**

:   element is undetectable in taskbar.

**F**

:   resize the element to fit the entire screen.

**d**

:   restore element\'s default window attributes.

**!**

:   bypass window manager.

**!!**

:   follow window manager as default.

**n**

:   center element in its parent.

**-**

:   hide element.

**+**

:   show element.

**c**

:   click element.

**f**

:   focus element.

**t**

:   make element stay at top.

**b**

:   make element stay at bottom.

**x**

:   hide element, or quit guish if quit-on-last-close is on and the
    element is the last closed one.

**l**

:   lower the element.

**r**

:   raise the element.

**M**

:   maximize the element.

**D**

:   disable the element.

**E**

:   enable the element.

**o**

:   fits element\'s size to its content.

**w**

:   resize an element in right-bottom directions to fit its parent,
    respecting limits of other elements.

**nfill**

:   resize an element in right-bottom directions to fit its parent.

**rfill**

:   resize an element in right direction to fit its parent, respecting
    limits of other elements.

**nrfill**

:   resize an element in right direction to fit its parent.

**bfill**

:   resize an element in bottom direction to fit its parent, respecting
    limits of other elements.

**nbfill**

:   resize an element in bottom direction to fit its parent.

**L**

:   clear element\'s data.

**: \<title\>**

:   set element title.

**s \<text\>**

:   set element style (see STYLE AND ATTRIBUTES section).

**z \<w\> \<h\>**

:   resize element by width and height.

**m \<x\> \<y\>**

:   move element to coords \<x\> \<y\>.

**/ \[\<l\|L\|a\|A\|p\|x\|n\> \[\<\...\>\]\]**

:   draws/fills lines, points and arcs depending on operation (See
    Drawing operations subsection). Origin coordinates correspond to the
    bottom-left corner as default Cartesian axes (instead of upper-left
    one used for windows/elements). If no operation is given, then all
    drawings are discarded from the implied element.

**A \<element\> \<alignment\>**

:   moves implied element to \<element\> using \<alignment\> (see
    alignment in STYLE AND ATTRIBUTES section, as \<alignment\> opts are
    similar).

**\< \<text\>**

:   set element text using \<text\>.

**\<+ \<text\>**

:   add additional text to element text using \<text\>.

**\> \<var\>**

:   define a variable named \<var\> using element text to set its value.

**g**

:   enable/disable (toggle) moving the parent of the element by
    click-and-drag it. Enabling this will automatically exclude x/y
    moving flags below.

**X**

:   enable/disable (toggle) moving an element inside its parent by
    click-and-drag on x axis Enabling this will automatically exclude
    the flag to click-and-drag and move parent.

**Y**

:   enable/disable (toggle) moving an element inside its parent by
    click-and-drag on y axis Enabling this will automatically exclude
    the flag to click-and-drag and move parent.

## Drawing operations

**l \[\<color\> \<x1\> \<y1\> \<x2\> \<y2\> \[\<\...\>\]\]**

:   draws lines between given points using color \<color\> (beware this
    command consumes all the phrase). If no arguments are given, then
    all element lines are discarded.

**L \[\<color\> \<x1\> \<y1\> \<x2\> \<y2\> \[\<\...\>\]\]**

:   fills the polygon described by given points using color \<color\>
    (beware this command consumes all the phrase). If no arguments are
    given, then all filled polygons are discarded.

**a \[\<color\> \<x\> \<y\> \<w\> \<h\> \<alpha\> \<beta\> \[\<\...\>\]\]**

:   draws an arc using color \<color\> and whose \"center\" is at \<x\>
    \<y\>, major and minor axes are respectively \<w\> and \<h\>, start
    and stop angles are \<alpha\> and \<beta\> (consumes all the
    phrase). If no arguments are given, then all element arcs are
    discarded.

**A \[\<color\> \<x\> \<y\> \<w\> \<h\> \<alpha\> \<beta\> \[\<\...\>\]\]**

:   fills an arc using color \<color\> and whose \"center\" is at \<x\>
    \<y\>, major and minor axes are respectively \<w\> and \<h\>, start
    and stop angles are \<alpha\> and \<beta\> (consumes all the
    phrase). If no arguments are given, then all element arcs are
    discarded.

**p \[\<color\> \[\<\...\>\]\]**

:   draws given points using color \<color\> (beware this command
    consumes all the phrase). If no arguments are given, then all points
    are discarded.

**x \[\<color\> \[\<\...\>\]\]**

:   draws given pixels using color \<color\> (beware this command
    consumes all the phrase). If no arguments are given, then all pixels
    are discarded.

**n \[\<color\> \<name\> \<x\> \<y\>\]**

:   draws given point using color \<color\> and putting the text
    \<name\> at that point. If no arguments are given, then all points
    are discarded.

# NORMAL COMMANDS

## checkbox commands

**C**

:   check.

**U**

:   uncheck.

## input commands

**S**

:   show input data as normal while typing.

**H**

:   hide input data while typing.

**P**

:   use password mode, displaying just asterisks.

**W**

:   toggles \"go to the next line\" when hitting return (triggers return
    signal anyway).

## page commands

**Q**

:   make subelements equals (in size).

**S \<style\>**

:   style all subelements.

**P**

:   free all embedded elements.

**\<\< \<element\>**

:   embeds element (or an external client).

**\<\<\< \<element\>**

:   embeds element (or an external client), fitting the page to its
    content.

**\>\> \<element\>**

:   free element, reparenting it to root window.

**\>\>\> \<element\>**

:   free element, reparenting it to root window and fitting the page to
    its content.

**v**

:   set vertical layout readjusting all subwidgets.

**h**

:   set horizontal layout readjusting all subwidgets.

**Z \<e1\> \<e2\>**

:   swaps sub-elements position.

**N**

:   inverts the order of sub-elements.

# SPECIAL SIGNALS

Special signals are independent from elements, and are tied to internal guish events.

:   

**q**

:   triggered at program exit.

**t**

:   triggered when program receives a SIGINT or a SIGTERM.

# GENERIC SIGNALS

Generic signals are common to all elements.

:   

**x**

:   triggered when element is closed.

**c**

:   triggered when element is clicked.

**lc**

:   triggered when element is left-clicked.

**rc**

:   triggered when element is right-clicked.

**mc**

:   triggered when element is middle-clicked.

**cc**

:   triggered when element is double clicked.

**lcc**

:   triggered when element is double left-clicked.

**rcc**

:   triggered when element is double right-clicked.

**mcc**

:   triggered when element is double middle-clicked.

**p**

:   triggered when element is pressed.

**lp**

:   triggered when element is left-pressed.

**rp**

:   triggered when element is right-pressed.

**mp**

:   triggered when element is middle-pressed.

**r**

:   triggered when element is released.

**lr**

:   triggered when element is left-released.

**rr**

:   triggered when element is right-released.

**mr**

:   triggered when element is middle-released.

**m**

:   triggered when element is moved.

**s**

:   triggered when element scrolled down.

**S**

:   triggered when element scrolled up.

**z**

:   triggered when element is resized.

**e**

:   triggered when mouse pointer \"enters\" the element.

**l**

:   triggered when mouse pointer \"leaves\" the element.

**f**

:   triggered when focusing the element.

**u**

:   triggered when un-focusing the element.

# NORMAL SIGNALS

Normal signals are per element.

:   

## checkbox signals

**U**

:   triggered when checkbox is unchecked.

**C**

:   triggered when checkbox is checked.

## input signals

**R**

:   triggered when input has focus and \"return\" is hit.

# REDUNDANT TOKENS

These tokens are ignored: \"**,**\", \"**-\>**\".

# EVALUATION ORDER AND SUBSTITUTIONS

Every time a new phrase is evaluated, it goes through a series of
special substitutions/evaluations before it\'s commands are interpreted.

The evaluation order is: hex substitution (at tokenizer level),
evaluation of expressions (where code evaluation/execution,
substitutions and functions are computed), evaluation of special
commands and execution of generic/normal commands.

Moreover if after the execution phase (the last one) the phrase is not
empty, the evaluation cycle will restart from evaluation of expressions
phase, and it will continue until there are no more tokens in the
phrase.

Every phrase is reduced to an empty phrase while evaluating:

     a=234;{i1=|i|;<'input1'+}();{i2=|i|;<'input2'+}()|b|<btn+

This example is composed by 2 phrases, and the code block in each phrase
is executed before each assignment.

## Hex substitution

Non quoted tokens are subject to hex substitution: if a \"\\x\" plus 2
hexadecimal characters is found, it\'s substituted with corresponding
ascii characters.

     puts \x68\x6F\x6D\x65

Here, the string \"home\" is printed.

## Globbing

If a \"**\***\" is given, then all widgets wids are substituted.

     |b||b||b|+
     puts *

## Variable substitution

With the **=** operator (actually, it\'s a special statement command),
it\'s possible to assign values to a variable, reusing it later by
simply referencing it using **@** operator when not inside quotes or by
wrapping it inside **\@{}** when in double quotes, shell command
substitution quotes **\`\`**, or external window id substitution **\<(
)**.

     b = 123; puts @a

There are two methods to define/create empty variables: by explicitely
assing an empty string to a variable (ex. a = \"\") or by simply omit
the value (ex. a =).

In addition, if there is more than one value to assign, a block is
automatically created (embedding those values) and assigned to that
variable:

     a = 1              # this simply assigns '1' to the variable 'a'
     b = 1, 2, 3, 4     # this instead assigns the block '{1, 2, 3, 4}' to the variable 'a'
     c = {1, 2, 3, 4}   # same but explicit

Each block has it\'s own scope, and variable resolution works by
searching from the last scope to the first. Ex:

     a = 1
     puts(@a)
     {
         a=345
         b=6534
     }()
     puts@a
     puts"b:@{b}"

In the last example, a is set to 1 and printed, then it\'s changed to
345 from another scope, in which another variable (b) is set. After code
block, just \"a\" is updated, and \"b\" doesn\'t exist anymore.

For example:

     gname = MY_GRIP_NAME
     |l|<@gname

or

     gname = MY_GRIP_NAME
     name = 'random name'
     puts "@{gname} is maybe @{name}"

## Element expressions

Anything inside **\|\|** is an element expression; a widget of a given
element is created and its X11 window id substituted. The synopsis is:
\|\<element\>\[{\<width\>, \<height\>}\]\| Ex.

     |b|+

If an integer is given, instead of one of available element types, then
the program tries to find an existing program having that integer as
window id. Ex.

     |12341234|-

This creates a widget (\"external\") for the external program and hides
it.

## Shell command substitution

Anything inside **\`\`** is treated as a shell command, and it\'s output
is substituted.

     d = `date`
     ols = `ls -l`

## External window id substitution

Everything inside **\<( )** is recognized as an external command (X11
GUI), forked and executed, and its window id is substituted (using
\_NET_WM_PID).

For example:

     a = <(xterm)

xterm program is spawn, and can be driven (almost) like a normal
element.

# OPERATORS

## Binary

**\<s\> .. \<e\>**

:   returns integers starting at \<s\> and ending at \<e\> (inclusive)
    as multiple tokens.

**\<var\> = \[\<val\>, \[\<val1\>, \...\]\]**

:   defines a variable (consumes all the phrase). If no value is given,
    an empty token is assigned to the variable. If a single value is
    given, that value is assigned to the variable. If multiple values
    are given, then all these values are wrapped inside a block, and
    this block is assigned to the variable.

**\<attr\> .= \[\<val\>, \[\<val1\>, \...\]\]**

:   defines an element using the implied subject attribute (consumes all
    the phrase). If no value is given, an empty token is assigned to the
    variable. If a single value is given, that value is assigned to the
    variable. If multiple values are given, then all these values are
    wrapped inside a block, and this block is assigned to the variable.

## Unary

**@\<varname\|num\>**

:   dereferences a variable name (or positional argument).

**@\***

:   returns all function parameters as tokens (usable with command line
    parameters too).

**\[\<eid\>\].\<attr\>**

:   dereferences an element attribute; if \<eid\> is given, uses that
    element, otherwise uses implied subject.

# ELEMENT ATTRIBUTES

Every element can have some default readonly attributes and a variable
number of custom attributes (which can be set by using assignment only,
not by using **let** function). To set an attribute, use the \"**.=**\"
operator; to get it instead use the \"**.**\" operator.

     b = |b|; myattr .= 'here'; puts(@b.myattr)

In the last example, a custom attribute, \"myattr\" is created and used.

The following are default attributes (readonly):

**t**

:   widget\'s type.

**w**

:   widget\'s width.

**h**

:   widget\'s height.

**x**

:   widget\'s x coord.

**y**

:   widget\'s y coord.

**b**

:   widget\'s border width.

**g**

:   widget\'s margin width.

**d**

:   widget\'s text data.

**T**

:   widget\'s title.

**c**

:   widget\'s checked/unchecked status (only for checkbox).

**n**

:   widget\'s number of subwidgets (only for page).

**s**

:   widget\'s subwidgets ids (one token each, only for page).

**pid**

:   process ID associated with the widget.

**v**

:   widget is visible.

**e**

:   widget is enabled (freezed/unfreezed).

**f**

:   widget is focused.

# BUILTIN FUNCTIONS

Symbol \"**\...**\" means a variable number of arguments.

**exists(\<eid\>)**

:   returns 1 if a widget with id \<eid\> exists, 0 otherwise.

**read(\[\<file\>\])**

:   reads and returns a line (excluding newline) from standard input; if
    an existing file is given, reads and returns all its content. Beware
    that this function blocks the GUI events, and returns nothing when
    reading from stdin and source is non-blocking.

**write(\<text\>, \<file\>)**

:   writes text into file and returns the number of characters written.
    Creates the file if it doesn\'t exist yet.

**append(\<text\>, \<file\>)**

:   append text to the end of file and returns the number of characters
    written. Creates the file if it doesn\'t exist yet.

**eval(\...)**

:   evaluates code by first stringifying all given arguments and then
    returns the result of evaluation if any. Beware that this function
    runs in the \"current\" scope, and can modify it.

**builtin(\<func\>, \...)**

:   gets the name of a builtin function and a variable number of
    arguments, then calls the builtin function with those arguments and
    returns the result (if any). It\'s useful when overriding builtin
    functions.

**each(\<function\>, \...)**

:   executes \<function\> for each additional argument given passing it
    as the first argument to the block. If return values are present,
    they will be accumulated and then returned.

**env(\<var\>)**

:   returns the value of the environment variable \"var\".

**cwd()**

:   returns the value of the current working directory.

**rev(\[\<block\>\], \...)**

:   if a block is given as first argument, its element will be returned
    in reverse, otherwise **rev** will return all its arguments in
    reverse. This function is somewhat special, as when there are no
    arguments to get, it\'ll return nothing (statement behaviour).

**let(\[\<var\>, \<val\>\], \...)**

:   sets variables, works exactly like assignment with special operator
    **=**, but in expressions. This function is somewhat special, it\'ll
    return nothing (statement behaviour).

**if(\<cond\>, \[\<v1\>, \[\<v2\>\]\])**

:   if \<cond\> is true and \<v1\> is given, returns \<v1\>, else if
    \<cond\> is false and \<v2\> is given, returns \<v2\>.

**unless(\<cond\>, \[\<v1\>, \[\<v2\>\]\])**

:   if \<cond\> is false and \<v1\> is given, returns \<v1\>, else if
    \<cond\> is true and \<v2\> is given, returns \<v2\>.

**and(\...)**

:   returns the first true argument; if there are no true arguments,
    returns the last one which is false. The function evaluates any
    block given.

**or(\...)**

:   returns the last true argument if all arguments are true, otherwise
    returns the first false argument. The function evaluates any block
    given.

**flat(\...)**

:   returns all given arguments; if a block is found, then it is
    flatted.

**block(\...)**

:   returns a code block, embedding the given arguments into **{}**.

**some(\...)**

:   returns given arguments. If nothing is given, returns an empty
    token.

**puts(\...)**

:   prints given arguments to stdout. This function is somewhat special,
    it\'ll return nothing (statement behaviour).

**push(\...)**

:   pushes given arguments to current function arguments (works with
    command line parameters too). This function is somewhat special,
    it\'ll return nothing (statement behaviour).

**pushb(\...)**

:   pushes given arguments to current function arguments from the
    beginning (works with command line parameters too). This function is
    somewhat special, it\'ll return nothing (statement behaviour).

**pop()**

:   pops the last argument from function arguments (works with command
    line parameters too). This function is somewhat special, as if there
    are no arguments to pop, it\'ll return nothing (statement
    behaviour).

**popb()**

:   pops the first argument from function arguments (works with command
    line parameters too). This function is somewhat special, as if there
    are no arguments to pop.

**times(\<n\>, \<arg\>)**

:   returns a sequence of tokens made by \<n\> times \<arg\>. This
    function is somewhat special, as when there are 0 tokens to
    replicate, it\'ll return nothing (statement behaviour).

**get(\<name\>)**

:   returns the value of the variable with name \<name\>, or an empty
    token if the variable does not exist.

**true(\<arg\>)**

:   returns 1 if \<arg\> is true, 0 otherwise.

**false(\<arg\>)**

:   returns 0 if \<arg\> is true, 1 otherwise.

**in(\<n\>, \<heap\>)**

:   returns 1 if \<n\> is found in \<heap\>, 0 otherwise; the arguments
    can be of any type (single tokens and blocks).

**join(\...)**

:   joins blocks and/or tokens by applying the following rules to all
    arguments given, and accumulates the result as the first operand. If
    the operands are blocks, then a single new block is created by
    joining them; if the operands are tokens, then a single new token is
    created by joining them, and its type will be that of the \"second\"
    token; if the operands are mixed (eg. a block and a token), then the
    token will be embedded inside the block.

**isdef(\<token\>)**

:   returns 1 if \<token\> is a variable, 0 otherwise.

**isvar(\<token\>)**

:   returns 1 if \<token\> is a (type) variable, 0 otherwise.

**isfunc(\<token\>)**

:   returns 1 if \<token\> refers to a function, 0 otherwise.

**isblock(\...)**

:   returns 1 if just one argument is given and is a block, 0 otherwise.

**isint(\...)**

:   returns 1 if just one argument is given and is an integer, 0
    otherwise.

**len(\<arg\>)**

:   if a block is given, returns the number of its element, otherwise
    returns the number of characters of the token.

**split(\<token\>, \<sep\>)**

:   splits \"token\" using separator \"sep\" and returns resulting
    tokens having their type equal to that of \"token\".

**csplit(\<token\>, \<sep\>)**

:   splits \"token\" using separator \"sep\" and returns resulting
    tokens as normal commands.

**seq(\<t1\>, \<t2\>, \...)**

:   returns 1 if all arguments are equal (string comparison), 0
    otherwise.

**add(\...)**

:   perform addition.

**sub(\...)**

:   perform subtraction.

**mul(\...)**

:   perform multiplication

**div(\...)**

:   perform division.

**mod(\...)**

:   perform modulus.

**rand()**

:   returns a random positive integer.

**sqrt(\<n\>)**

:   returns the square root of \<n\>.

**cbrt(\<n\>)**

:   returns the cube root of \<n\>.

**pow(\<n\>, \<e\>)**

:   returns the power of \<n\> raised to \<e\>.

**log(\<n\>)**

:   returns the base 10 logarithm of \<n\>.

**ln(\<n\>)**

:   returns the natural logarithm of \<n\>.

**sin(\<n\>)**

:   returns the sine of \<n\> (degrees).

**cos(\<n\>)**

:   returns the cosine of \<n\> (degrees).

**tan(\<n\>)**

:   returns the tangent of \<n\> (degrees).

**hex(\<n\>)**

:   returns \<n\> in its hexadecimal representation.

**int(\<n\>)**

:   returns integral part of given number \<n\>; rounds to nearest
    integer.

**xor(\<n1\>, \<n2\>, \...)**

:   perform bitwise XOR.

**band(\<n1\>, \<n2\>, \...)**

:   perform bitwise AND.

**bor(\<n1\>, \<n2\>, \...)**

:   perform bitwise OR.

**lsh(\<n1\>, \<n2\>, \...)**

:   perform bitwise left shift.

**rsh(\<n1\>, \<n2\>, \...)**

:   perform bitwise right shift.

**not(\<n\>)**

:   perform negation

**eq(\<n1\>, \<n2\>, \...)**

:   equal-to

**ne(\<n1\>, \<n2\>, \...)**

:   not-equal-to

**lt(\<n1\>, \<n2\>, \...)**

:   less-than

**gt(\<n1\>, \<n2\>, \...)**

:   greater-than

**le(\<n1\>, \<n2\>, \...)**

:   less-equal-than

**ge(\<n1\>, \<n2\>, \...)**

:   greater-equal-than

**abs(\<n\>)**

:   perform absolute value

**neg(\<n\>)**

:   unary minus.

# STYLE AND ATTRIBUTES

Each element has some properties/attributes which can be set by using
style command. This command takes a string that has a format like CSS:

     |l| s 'bg: blue; fg: white'

Here, we create a label with a blue background and white foreground.

Each field must be separated by newlines, **;** or **\|**. Colors can be
specified by using a common shortname, such as \"yellow\", or by using
RGB value, such as \"#ff32ae\".

**background \| bg: \<color\|/\<path to image\>\>**

:   set background color or a background image by specifying image path;
    if the string \"null\" is given as \<path to image\>, current image
    is removed. (Background image loading requires building guish with
    Imlib2 support.)

**color \| foreground \| fg: \<color\>**

:   set foreground color

**pressed-background \| pbg: \<color\>**

:   background color when element is pressed

**pressed-color \| pfg: \<color\>**

:   foreground color when element is pressed

**hovered-background \| hbg: \<color\>**

:   background color when element is hovered

**hovered-color \| hfg: \<color\>**

:   foreground color when element is hovered

**border-color \| bc: \<color\>**

:   set border color

**width \| w: \<value in pixels\>**

:   set width

**height \| h: \<value in pixels\>**

:   set height

**border \| b: \<value in pixels\>**

:   set border width

**line \| l: \<value in pixels\>**

:   set line width (use with \"/\" command)

**margin \| g: \<value in pixels\>**

:   set margin width

**mode \| m: \<expanding mode\>**

:   set expanding mode type (See Expanding mode)

**align \| a: \<alignment\>**

:   set alignment type (See Alignment)

**f \| font: \<font name\>**

:   set font type using a X11 font name

## Expanding mode

**fixed \| f**

:   width and height are fixed (default for all elements)

**wfixed \| w**

:   width is fixed, height can change

**hfixed \| h**

:   height is fixed, width can change

**relaxed \| r**

:   width and height can change

## Alignment

Any element that\'s not a page has a particular text alignment that can
be changed. If an alignment is specified for a page element instead,
(whose defaults alignments are top-center for horizontal layout, and
middle-left for vertical one), then its sub-elements will be aligned
accordingly, depending from page layout type too.

**l \| left \| middle-left**

:   show text at middle left

**r \| right \| middle-right**

:   show text at middle right

**c \| center \| middle-center**

:   show text at middle center

**tl \| top-left**

:   show text at top left

**tr \| top-right**

:   show text at top right

**t \| top-center**

:   show text at top center

**bl \| bottom-left**

:   show text at bottom left

**br \| bottom-right**

:   show text at bottom right

**b \| bottom-center**

:   show text at bottom center

# AUTHOR

Francesco Palumbo \<phranz.dev@gmail.com\>

# THANKS

La vera Napoli, Carme, Pico, Titina, Molly, Leo, i miei amati nonni, mio
padre, mia madre, e tutti coloro su cui ho potuto e posso contare.
Grazie mille.

# LICENSE

**GPL-3.0-or-later** see COPYING
