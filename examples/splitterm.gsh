#!/usr/bin/env -S guish -qk
########################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

orient = 0

widxt1 = <(xterm -ls +sb -bg black -fg green)
    z 400 200!
widxt2 = <(xterm -ls +sb -bg black -fg green)
    z 400 200!

for x in * : {
    @x
    -
    s 'b:2|bc:gray'
}

grip = |l|
    !g < sterm s'm:h|h:20|a:c|bg:yellow'
e =
main =
t=|b|
    => c {
        unless @orient {
            orient = 1
            @main h,o
            @cont o
            for x in @t,@e,@grip : {
                @x w
            }
        } else {
            orient = 0
            @main v,o
            for x in @t,@e,@grip : {
                @x s'w:15|h:15'
            }
            @cont o
            for x in @t,@e,@grip : {
                @x w
            }
        }
    } <'toggle orientation' s'm:h|w:15|h:15'

e = |b|
    => c{q} < quit
    s'm:h|w:15|h:15'

main=|p|
    v <<< @widxt1 <<< @widxt2
cont = |p|
    v!:'sterm' <<< @grip <<< @t <<< @main <<< @e

for x in @t,@e,@grip : {@x w}
for x in*:{@x+}

after 2 {
    send @widxt1 'ls -l'
    ctrl @widxt1  Return
}
