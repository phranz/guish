#!/usr/bin/env guish
########################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

slider = {
    bl = @1
    if and({not(isblock(@bl))}, {not(isfunc(some(@bl)))})() {
        e "[slider] at '@{LINE}': error, no function given!\n"
        return 0
    }
    p=|p|
        v!s'b:1|bc:#dbff10|g:2|a:m|h:200|w:20'X,Y
    l=|l|
        s'bg:gray|b:2|bc:black|h:40|w:15'Y
        => m {
            t = div(mul(@self.y, 100), sub(@p.h, @p.g, @self.h, mul(@self.b, 2)))
            if not(eq(@self.x, @t)) {
                bl(@t)
            }
        }
    @p << @l+
    return @p
}

s = slider({puts(@1)})
