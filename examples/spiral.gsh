#!/usr/bin/env guish
#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

COLS = {yellow, magenta, blue, red, green, cyan, gray}

W = or(get(1), @SH)
D = or(get(2), 30)
L = or(get(3), 3)
C = or(get(4), {@COLS[mod(rand(), len(@COLS))]})

t=|t{@W,@W}|
    !X,Y

=> s {
    /
    draw(sub(.D, 2), sub(@self.L, 1))
} => S {
    /
    draw(add(.D, 2), add(@self.L, 1))
} => rc {
    q
}

draw = {
    D .= if(le(@1, 0), 1, @1)
    L .= if(le(@2, 0), 1, @2)

    op = sub
    y = div(@W, 2)
    sx = div(@W, 2)
    x = add(@sx, div(.D, 4))
    r = @W
    s = 0
    i = 180
    s join("l:", .L)
    arcs =
    while {gt(@r, 0)} { 
        arcs = flat(@arcs) @x @y @r @r @s @i
        r = sub(@r, @t.D)
        s = add(@s, @i)
        x = op(@sx, div(@t.D,4))
        if ge(@s, 360) {
            s = 0
        }
        op = if(seq(@op, sub), add, sub) 
    }
    / a @C flat(@arcs)
}
draw(@D, @L)
+
