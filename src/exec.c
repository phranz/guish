/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*************************************************************************/

#include <errno.h>

#include "exec.h"
#include "widgets.h"
#include "dectypes.h"
#include "tokenizer.h"
#include "sourcedriver.h"
#include "syntax.h"
#include "evaluator.h"
#include "debug.h"

extern vec_strline_t* strace;
extern unsigned long flags;
extern unsigned long options;
extern widget_t* it;
extern map_widwidget_t* widgets;

extern map_widloci_t* widlines;
extern map_widloci_t* widareas;
extern map_widarcssets_t* widarcssets;
extern map_widarcssets_t* widarcsareas;
extern map_widloci_t* widloci;
extern map_widloci_t* widpixels;
extern map_widnamedpoints_t* widnamedpoints;

static cmd valid_subphrase(element E, cmd c, vec_token_t* v) {
    if (!it || (E != E_ALL && (element)it->etype != E))
        return CMD_NONE;

    return ((int)v->count - (int)(cargs(c))) >= 0 ? c : CMD_NONE;
}

static int load_subject(vec_token_t* v) {
    showtokens(v);
    token_t* ft;
    unsigned long wid;
    widget_t* w;
    if (!v->first(v, &ft))
        return 0;
    errno = 0;
    wid = strtoul(ft->data, NULL, 10);
    if (errno != EINVAL && errno != ERANGE && wid) {
        if (!widgets->get(widgets, wid, &w))
            err(ft->source, ft->lineno, "error, unknown window id '%lu'.\n", wid);
        it = w;
        debug("subject loaded: %lu\n", it->wid);
        v->pop_back(v, &ft);
        release(ft);
        return 1;
    }
    return 0;
}

size_t exec(vec_token_t* v) {
    showtokens(v);
    cmd c = CMD_NONE;
    token_t* b = NULL;
    token_t* o = NULL;

    if (!v->first(v, &o))
        return 0;
    if (o->type == T_BLOCK) {
        token_t* t;
        o->block->data->first(o->block->data, &t);  
        err(t->source, t->lineno, "error, cannot execute a block as a command!\n");
    }

    unsigned long ln = 0;
    char* sn = NULL;
    while (v->count) {
        v->first(v, &o);
        if (kid(o->data))
            return v->count;
        if (load_subject(v))
            continue;
        if (!it)
            err(o->source, o->lineno, "error, no implied subject or unknown command '%s'.\n", o->data);
        ln = o->lineno;
        sn = o->source;
        v->pop_back(v, &o);
        c = cidbe(it->etype, o->data);
        debug("cidbe for e: '%d' data: '%s', c: %d\n", it->etype, o->data, c);
        if (!c)
            c = cid(o->data);
        debug("from token '%s', command: %d\n", o->data, c);

        // generic widget commands
        switch (c) {
            case CMD_TRAY:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                tray(it);
                break;
            case CMD_GHOST:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                ghost(it);
                break;
            case CMD_FULLSCREEN:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fullscreen(it);
                break;
            case CMD_CENTER:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                center(it);
                break;
            case CMD_SHOW:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                show(it);
                break;
            case CMD_DRAW: {
                if (!v->count) {
                    loci_t* l;
                    if (widlines && widlines->get(widlines, it->wid, &l)) {
                        widlines->remove(widlines, it->wid);
                        release(l);
                    }
                    arcssets_t* a;
                    if (widarcssets && widarcssets->get(widarcssets, it->wid, &a)) {
                        widarcssets->remove(widarcssets, it->wid);
                        release(a);
                    }
                    if (widarcsareas && widarcsareas->get(widarcsareas, it->wid, &a)) {
                        widarcsareas->remove(widarcsareas, it->wid);
                        release(a);
                    }
                    if (widareas && widareas->get(widareas, it->wid, &l)) {
                        widareas->remove(widareas, it->wid);
                        release(l);
                    }
                    if (widloci && widloci->get(widloci, it->wid, &l)) {
                        widloci->remove(widloci, it->wid);
                        release(l);
                    }
                    if (widpixels && widpixels->get(widpixels, it->wid, &l)) {
                        widpixels->remove(widpixels, it->wid);
                        release(l);
                    }
                    namedpoints_t* n;
                    if (widnamedpoints && widnamedpoints->get(widnamedpoints, it->wid, &n)) {
                        widnamedpoints->remove(widnamedpoints, it->wid);
                        release(n);
                    }
                    it->draw(it);
                    break;
                }
                char type;
                v->pop_back(v, &b);
                assert_notblock(b, "draw operation", o->data);
                type = *b->data;
                release(b);

                switch (type) {
                case 'l':
                case 'p':
                case 'L':
                case 'P':
                case 'x': {
                    map_widloci_t** set = NULL;
                    void (*op)(widget_t*, char*, vec_int_t*) = NULL;
                    switch (type) {
                        case 'l': set = &widlines; op = addlines; break;
                        case 'p': set = &widloci; op = addpoints; break;
                        case 'x': set = &widpixels; op = addpixels; break;
                        case 'L':
                        case 'P': set = &widareas; op = addarea; break;
                    }
                    if (!v->count) {
                        loci_t* l;
                        if (*set && (*set)->get(*set, it->wid, &l)) {
                            (*set)->remove(*set, it->wid);
                            release(l);
                        }
                        it->draw(it);
                        break;
                    }
                    token_t* col;
                    if (v->count < 3)
                        goto badnargs;

                    v->pop_back(v, &col);
                    assert_notblock(col, "color", o->data);

                    vec_int_t* ps = alloc(vec_int_t);
                    while (v->count) {
                        v->pop_back(v, &b);
                        assert_notblock(b, "coord", o->data);
                        int x = tolong(b);
                        release(b);
                        if (!v->pop_back(v, &b))
                            err(sn, ln, "error, missing coord.\n");
                        assert_notblock(b, "coord", o->data);
                        int y = tolong(b);
                        release(b);
                        ps->push(ps, x);
                        ps->push(ps, y);
                    }
                    op(it, col->data, ps);
                    release(col);
                    release(ps);
                    break;
                }
                case 'a': 
                case 'A': {
                    if (!v->count) {
                        map_widarcssets_t** set = (c == 'a' ? &widarcssets : &widarcsareas);
                        arcssets_t* a;
                        if (*set && (*set)->get(*set, it->wid, &a)) {
                            (*set)->remove(*set, it->wid);
                            release(a);
                        }
                        it->draw(it);
                        break;
                    }
                    if (v->count < 7)
                        goto badnargs;

                    token_t* col;
                    v->pop_back(v, &col);
                    assert_notblock(col, "color", o->data);
                    vec_int_t* arcs = alloc(vec_int_t);
                    while (v->count) {
                        if (v->count < 6)
                            goto badnargs;
                        v->pop_back(v, &b);
                        assert_notblock(b, "coord", o->data);
                        int x = tolong(b);
                        release(b);
                        v->pop_back(v, &b);
                        assert_notblock(b, "coord", o->data);
                        int y = tolong(b);
                        release(b);
                        v->pop_back(v, &b);
                        assert_notblock(b, "width", o->data);
                        int ww = tolong(b);
                        release(b);
                        v->pop_back(v, &b);
                        assert_notblock(b, "height", o->data);
                        int hh = tolong(b);
                        release(b);
                        v->pop_back(v, &b);
                        assert_notblock(b, "angle", o->data);
                        int angle1 = tolong(b);
                        release(b);
                        v->pop_back(v, &b);
                        assert_notblock(b, "angle", o->data);
                        int angle2 = tolong(b);
                        release(b);

                        arcs->push(arcs, x);
                        arcs->push(arcs, y);
                        arcs->push(arcs, ww);
                        arcs->push(arcs, hh);
                        arcs->push(arcs, angle1);
                        arcs->push(arcs, angle2);
                    }
                    addarcs(it, type == 'A', col->data, arcs);
                    release(arcs);
                    release(col);
                    break;
                }
                case 'n': {
                    if (!v->count) {
                        namedpoints_t* l;
                        if (widnamedpoints && widnamedpoints->get(widnamedpoints, it->wid, &l)) {
                            widnamedpoints->remove(widnamedpoints, it->wid);
                            release(l);
                        }
                        it->draw(it);
                        break;
                    }
                    if (v->count < 4)
                        goto badnargs;

                    token_t* col;
                    token_t* name;
                    v->pop_back(v, &col);
                    assert_notblock(col, "color", o->data);

                    v->pop_back(v, &name);
                    assert_notblock(name, "name", o->data);

                    v->pop_back(v, &b);
                    assert_notblock(b, "coord", o->data);
                    int x = tolong(b);
                    release(b);
                    v->pop_back(v, &b);
                    assert_notblock(b, "coord", o->data);
                    int y = tolong(b);
                    release(b);
                    addnamedpoint(it, col->data, name->data, x, y);
                    release(col);
                    release(name);
                    break;
                }
                default:
                    err(sn, ln, "error, unknown draw operation '%c'.\n", type);
                }
                break;
            }
            case CMD_HIDE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                hide(it);
                break;           
            case CMD_FOCUS:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                focus(it);
                break;
            case CMD_TOP:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                settop(it);
                break;
            case CMD_BOTTOM:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                setbottom(it);
                break;
            case CMD_CLOSE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                wclose(it);
                break;
            case CMD_CLICK:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                trigger(it->wid, SIG_CLICKED);
                break;
            case CMD_LOWER:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                lower(it);
                break;
            case CMD_RAISE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                wraise(it);
                break;
            case CMD_MAXIMIZE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                maximize(it);
                break;
            case CMD_FREEZE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                freeze(it);
                break;
            case CMD_UNFREEZE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                unfreeze(it);
                break;
            case CMD_FIT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fit(it);
                break;
            case CMD_FILL:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fill(it, 0);
                break;
            case CMD_FILLNOLIMIT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fill(it, 1);
                break;
            case CMD_FILLRIGHT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fill_right(it, 0);
                break;
            case CMD_FILLRIGHTNOLIMIT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fill_right(it, 1);
                break;
            case CMD_FILLBOTTOM:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fill_bottom(it, 0);
                break;
            case CMD_FILLBOTTOMNOLIMIT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                fill_bottom(it, 1);
                break;
            case CMD_SETDEFAULT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                setdefault(it);
                break;
            case CMD_BYPASSWM:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                bypasswm(it, 1);
                if (it->flags & F_VISIBLE) {
                    hide(it);
                    show(it);
                }
                break;
            case CMD_FOLLOWWM:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                bypasswm(it, 0);
                if (it->flags & F_VISIBLE) {
                    hide(it);
                    show(it);
                }
                break;
            case CMD_CLEAR:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                clear(it);
                break;
            case CMD_SETTEXT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                b = repr(b, 1);
                settext(it, b->data);
                release(b);
                break;
            case CMD_ADDTEXT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                b = repr(b, 1);
                addtext(it, b->data);
                release(b);
                break;
            case CMD_DEFTEXT:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                token_t* val = alloc(token_t);
                val->type = T_QUOTE;
                val->lineno = b->lineno;
                val->source = b->source;
                val->data = salloc(it->data);
                define(b, val, NULL, NULL); 
                release(b);
                release(val);
                break;
            case CMD_ENTITLE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                b = repr(b, 1);
                entitle(it, b->data);
                release(b);
                break;
            case CMD_STYLE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                b = repr(b, 1);
                style(it, b->data);
                release(b);
                break;
            case CMD_RESIZE: {
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                int wx = tolong(b);
                release(b);
                v->pop_back(v, &b);
                assert_notblock(b, "second", o->data);
                int hx = tolong(b);
                resize(it, wx, hx);
                release(b);
                break;
            }
            case CMD_MOVE: {
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                int x = tolong(b);
                release(b);
                v->pop_back(v, &b);
                assert_notblock(b, "second", o->data);
                int y = tolong(b);
                release(b);
                move(it, x, y);
                break;
            }
            case CMD_MOVEALIGN: {
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                v->first(v, &b);
                assert_notblock(b, "first", o->data);
                unsigned long wid = tolong(b);
                widget_t* r;
                if (!wid || !widgets->get(widgets, wid, &r)) {
                    err(b->source, b->lineno, "error, no widget with id '%lu'.\n", wid);   
                } else {
                    v->pop_back(v, &b);
                    release(b);
                    v->pop_back(v, &b);
                    assert_notblock(b, "second", o->data);
                    movealigns(it, r, b->data);
                    release(b);
                }
                break;
            }
            case CMD_TGRIP:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                tgrip(it);
                break;
            case CMD_TXMOVABLE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                txmov(it);
                break;
            case CMD_TYMOVABLE:
                if(!valid_subphrase(E_ALL, c, v))
                    goto badnargs;
                tymov(it);
                break;
            case CMD_SHOWINPUT:
                if(!valid_subphrase(E_INPUT, c, v))
                    goto badnargs;
                showinput(it);
                break;
            case CMD_HIDEINPUT:
                if(!valid_subphrase(E_INPUT, c, v))
                    goto badnargs;
                hideinput(it);
                break;
            case CMD_PASSWORD:
                if(!valid_subphrase(E_INPUT, c, v))
                    goto badnargs;
                password(it);
                break;
            case CMD_TRET:
                if(!valid_subphrase(E_INPUT, c, v))
                    goto badnargs;
                tret(it);
                break;
            case CMD_EQUALIZE:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                equalize(it);
                break;
            case CMD_STYLESUBS:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                b = repr(b, 1);
                stylesubs(it, b->data);
                release(b);
                break;
            case CMD_VERTICAL:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                vertical(it);
                break;
            case CMD_HORIZONTAL:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                horizontal(it);
                break;
            case CMD_SWAP:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                token_t* fw;
                token_t* sw;
                v->pop_back(v, &fw);
                assert_notblock(fw, "first", o->data);
                v->pop_back(v, &sw);
                assert_notblock(sw, "second", o->data);
                swap(it, fw->data, sw->data);
                release(fw);
                release(sw);
                break;
            case CMD_INVERT:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                invert(it);
                break;
            case CMD_EXPLODE:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                explode(it);
                break;
            case CMD_EMBED:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                embed(it, b->data);
                release(b);
                break;
            case CMD_FITEMBED:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                fitembed(it, b->data);
                release(b);
                break;
            case CMD_UNTIE:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                untie(it, b->data);
                release(b);
                break;
            case CMD_FITUNTIE:
                if(!valid_subphrase(E_PAGE, c, v))
                    goto badnargs;
                v->pop_back(v, &b);
                assert_notblock(b, "first", o->data);
                fituntie(it, b->data);
                release(b);
                break;
            case CMD_CHECK:
                if(!valid_subphrase(E_CHECKBOX, c, v))
                    goto badnargs;
                check(it);
                break;
            case CMD_UNCHECK:
                if(!valid_subphrase(E_CHECKBOX, c, v))
                    goto badnargs;
                uncheck(it);
                break;
            default:
                err(o->source, o->lineno, "error, unknown command '%s'\n", o->data);
        }
        release(o);
    }
    showtokens(v);
    return v->count;
    badnargs:
    err(sn, ln, "error, invalid number of parameters for command '%s'\n", o->data);
}
