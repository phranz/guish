/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>

#include "cutils.h"
#include "dectypes.h"
#include "evaluator.h"
#include "sourcedriver.h"
#include "tokenizer.h"
#include "parser.h"
#include "syntax.h"
#include "debug.h"
#include "main.h"

#define AUTHOR "Francesco Palumbo <phranz.dev@gmail.com>"
#define LICENSE "GPL-3.0-or-later"
#define VERSION "2.6.11"

int exit_status;
unsigned long options;
char buf[BSIZ];

#ifdef ENABLE_X11
#include <sys/wait.h>

#include "widget.h"
#include "exec.h"

// widgets never created or not
extern int dirty;
extern widget_t* it;

int dfd;

Display* display;
int screen;

Window root;
Atom delatom;
map_strcol_t* colors; 
GC gc;
XColor null;

map_widwidget_t* widgets;
map_pidwid_t* exts;

void x11_init() {
    static int x11_initialized;

    if (x11_initialized || display)
        return;
    if (!getenv("DISPLAY"))
        return;
    display = XOpenDisplay(NULL);
    if (!display)
        return;
    dfd = ConnectionNumber(display);
    screen = DefaultScreen(display);
    root = DefaultRootWindow(display);

    delatom = XInternAtom(display, "WM_DELETE_WINDOW", False);
    colors = alloc(map_strcol_t);
    colors->strcmp = 1;

    colors->insert(colors, salloc("black"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("white"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("gray"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("lightgray"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("red"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("green"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("blue"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("yellow"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("cyan"), calloc(1, sizeof(XColor)));
    colors->insert(colors, salloc("magenta"), calloc(1, sizeof(XColor)));

    XColor* c;
    colors->get(colors, "black", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "black", c, &null);
    colors->get(colors, "white", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "white", c, &null);
    colors->get(colors, "gray", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "gray", c, &null);
    colors->get(colors, "lightgray", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "lightgray", c, &null);
    colors->get(colors, "red", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "red", c, &null);
    colors->get(colors, "green", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "green", c, &null);
    colors->get(colors, "blue", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "blue", c, &null);
    colors->get(colors, "yellow", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "yellow", c, &null);
    colors->get(colors, "cyan", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "cyan", c, &null);
    colors->get(colors, "magenta", &c);
    XAllocNamedColor(display, DefaultColormap(display, screen), "magenta", c, &null);

    widgets = alloc(map_widwidget_t);
    exts = alloc(map_pidwid_t);
    x11_initialized = 1;
}

static void x11_clean() {
    if (!display)
        return;
    strcol_t* c;
    eachitem(colors, strcol_t, c,
        free((char*)c->key);
        free(c->val);
    );
    release(colors);
    if (gc)
        XFreeGC(display, gc);
    XFlush(display);
    XCloseDisplay(display);
    release(exts);
}

static int last_closed() {
    if (!widgets || (!widgets->count && !dirty))
        return 0;

    size_t closed = 0;
    widwidget_t* w;
    eachitem(widgets, widwidget_t, w,
        if (w->val->flags & F_CLOSED)
            ++closed;
    );
    return closed == widgets->count;
}

static void manage_exts() {
    if (exts->count) {
        int stat;
        pidwid_t* p;
        eachitem(exts, pidwid_t, p,
            if (kill(p->key, 0) < 0 && errno == ESRCH) {
                if (p->val == it)
                    it = NULL;
                del(p->val->wid);
                exts->remove(exts, p->key);
                continue;
            }
            if (waitpid(p->key, &stat, WNOHANG) > 0 && !stat) {
                if (p->val == it)
                    it = NULL;
                del(p->val->wid);
                exts->remove(exts, p->key);
            }
        );
    }
}

void process_events() {
    XEvent e;
    widget_t* w;

    while (XPending(display)) {
        XNextEvent(display, &e);
        if (widgets->get(widgets, e.xany.window, &w) && w)
            event(w, &e);
    }
}

void poll_and_process_events(int us) {
    struct timeval tv;
    fd_set ins;
    int r;

    FD_ZERO(&ins);
    FD_SET(dfd, &ins);
    tv.tv_usec = us;
    tv.tv_sec = 0;
    r = select(dfd+1, &ins, NULL, NULL, &tv);
    if (r > 0)
        process_events();
}

static void showfonts() {
    int r;
    char** f;

    display = XOpenDisplay(NULL);
    if (!display) {
        fprintf(stderr, "Error, unable to open display!\n");
        exit(EXIT_FAILURE);
    }
    f = XListFonts(display, "*", 2000, &r);
    for (size_t i=0; i<(size_t)r; ++i)
        printf("%s\n", f[i]);
    XFreeFontNames(f);
    XCloseDisplay(display);
    display = NULL;
}
#else
#define showfonts(X)
#define exec(X) (0)
#endif

extern map_sigtoken_t actions;
extern map_int_timetoken_t* schedules;
extern map_int_timetoken_t* onetimes;
extern scope_t* cs;
extern sig_atomic_t sq;
extern unsigned long flags;
extern sourcedata_t* current;

static void help(int fd, char** argv) {
    dprintf(fd, 
        "Usage: %s [-c <code>][-s][-q][-k][-t][-f][-v][-b][-h][<file>][<arguments>]\n"
        "\n"
        "   -c <code>       read and execute commands from command line.\n"
        "   -s              display elements when created.\n"
        "   -q              quit when closing last element/window.\n"
        "   -k              terminate all external programs when quitting.\n"
        "   -t              fallback on tty after reading data from other inputs.\n"
        "   -f              show available X11 fonts.\n"
        "   -v              show version.\n"
        "   -b              show build (compiled in) options.\n"
        "   -h              guess.\n"
        "\nAuthor: %s"
        "\nLicense: %s\n",
        argv[0], AUTHOR, LICENSE);
}

static void build_info() {
    printf("%s" "\n%s" "\n%s" "\n%s\n",
        #ifdef ENABLE_X11
        "X11 support (Xlib): yes"
        #else
        "X11 support (Xlib): no"
        #endif
        ,
        #ifdef ENABLE_CONTROL
        "Control support (Xtst): yes"
        #else
        "Control support (Xtst): no"
        #endif
        ,
        #ifdef ENABLE_IMAGES
        "Images support (Imlib2): yes"
        #else
        "Images support (Imlib2): no"
        #endif
        ,
        #ifdef DEBUG
        "Debug activated: yes"
        #else
        "Debug activated: no"
        #endif
    );
}

static void interpret(vec_token_t* args) {
    phrase_t* p = NULL;
    vec_token_t* objs = NULL;
    while (!(flags & EV_QUITCALLED) && !sq) {
        p = tokenize();
        if (!p)
            break;
        objs = parse_phrase(p, cs);
        release(p);
        if (!objs)
            break;
        while (!sq && objs && eval_expressions(objs, args) && !sq && eval_statements(objs, args) &&
            !(flags & EV_QUITCALLED) && !sq && exec(objs));
        release_phrase(objs);
        objs = NULL;
    }
    release_phrase(objs);
}

int loop(int s, char* data, vec_token_t* args) {
    debug("initial source %d\n", s);
    if (!s)
        new_source(S_STDIN, NULL);
    else
        new_source(s, data);
    #ifdef ENABLE_X11
    x11_init();
    if (display) {
        while (!sq && !(flags & EV_QUITCALLED) && !(options & QUIT_ON_LAST_CLOSED && last_closed())) {
            process_events();
            schedule();
            if (!valid_source()) {
                if ((!widgets || !widgets->count) &&
                    (!onetimes || !onetimes->count) &&
                    (!schedules || !schedules->count))
                    break;
                poll_and_process_events(100000);
                continue;
            }
            interpret(args);
            manage_exts();
            if (sq || flags & EV_QUITCALLED)
                break;
            if (source_exahusted())
                next_source();
            process_events();
            if (current && current->nb)
                poll_and_process_events(100000);
        }
    #else
    if (0) {
    #endif
    } else {
        while (!sq && !(flags & EV_QUITCALLED) && valid_source()) {
            schedule();
            interpret(args);
            if (sq || flags & EV_QUITCALLED)
                break;
            if (source_exahusted())
                next_source();
            if (!current || current->nb)
                usleep(10);
        }
    }
    if (sq)
        trigger(0, SIG_TERM);
    else
        trigger(0, SIG_EXIT);
    return exit_status;
}

int main(int argc, char **argv) {
    int opt;
    int initial_source = S_NOSRC;
    char* source_data = NULL;

    while ((opt = getopt(argc, argv, "c:sqktfvbh")) > 0) {
        switch (opt) {
            case 'c':
                initial_source = S_CMDLINE;
                source_data = salloc(optarg);
                break;
            case 's':
                options |= SHOW_ON_CREATE;
                break;
            case 'q':
                options |= QUIT_ON_LAST_CLOSED;
                break;
            case 'k':
                options |= KILL_EXTS_ON_QUIT;
                break;
            case 't':
                options |= FALLBACK_ON_TTY;
                break;
            case 'f':
                showfonts();
                return EXIT_SUCCESS;
            case 'v':
                printf("%s\n", VERSION);
                return EXIT_SUCCESS;
            case 'b':
                build_info();
                return EXIT_SUCCESS;
            case 'h':
                help(1, argv);
                return EXIT_SUCCESS;
            default:
                help(2, argv);
                return EXIT_FAILURE;
        }
    }
    debug("optind: %d argc: %d\n", optind, argc);
    if (optind < argc && !source_data) {
        initial_source = S_SOURCE;
        source_data = salloc(argv[optind++]);
    }
    sourcedriver_init();
    evaluator_init();
    syntax_init();
    vec_token_t* args = NULL;
    while (optind < argc) {
        if (!args)
            args = alloc(vec_token_t);
        token_t* arg = alloc(token_t);
        arg->type = T_QUOTE;
        arg->data = salloc(argv[optind++]);
        args->push(args, arg);
    }
    int ret = loop(initial_source, source_data, args);
    release_phrase(args);
    if (source_data)
        zfree(source_data);
    evaluator_free();
    sourcedriver_free();
    syntax_free();
    #ifdef ENABLE_X11
    x11_clean();
    #endif
    return ret;
}
