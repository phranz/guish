/************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef DB_H
#define DB_H

#define dbg(m, ...) (fprintf(stderr, "[\033[0;33m%s\033[0m][\033[1;33m%s\033[0m]('%d') " m, __FILE__, __func__, __LINE__  ,## __VA_ARGS__))

#define ptokens(x) (printokens(x, 1, __FILE__,  __func__, __LINE__))
#define ptoken(x) (printoken(x, __FILE__, __func__, __LINE__))

#ifdef DEBUG
    #define debug(m, ...) (dbg(m ,## __VA_ARGS__))
    #define showtokens(x) (printokens(x, 1, __FILE__, __func__, __LINE__))
    #define showtoken(x) (printoken(x, 1, __FILE__, __func__, __LINE__))
#else
    #define debug(m, ...)
    #define showtokens(x)
    #define showtoken(x)
#endif

typedef struct token_t token_t;
typedef struct vec_token_t vec_token_t;

void printoken(token_t*, const char*, const char*, int);
void printokens(vec_token_t*, int, const char*, const char*, int);

#endif
