/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef PAGE_H
#define PAGE_H

#include "../widget.h"

typedef struct vec_widget_t vec_widget_t;

enum ltype {
    HL,
    VL
};

typedef struct page_t {
    widget_t w;

    int l;
    vec_widget_t* subs;
    int max_w;
    int max_h;
    align valign;

    void (*free)(struct page_t*);
} page_t;

page_t* page_t_init(page_t*, int, int);
void page_t_free(page_t*);

void equalize(widget_t*);
void stylesubs(widget_t*, const char*);
void showsubs(widget_t*);
void hidesubs(widget_t*);
void freezesubs(widget_t*);
void unfreezesubs(widget_t*);
void setlayout(page_t*, int, int);
void embed(widget_t*, const char*);
void fit_to_cont(widget_t*);
void fitembed(widget_t*, const char*);
void untie(widget_t*, const char*);
void fituntie(widget_t*, const char*);
void explode(widget_t*);
void child_going(widget_t*, widget_t*);
void horizontal(widget_t*);
void swap(widget_t*, const char*, const char*);
void invert(widget_t*);
void vertical(widget_t*);

#endif
