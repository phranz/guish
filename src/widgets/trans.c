/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include "trans.h"
#include "../dectypes.h"

extern GC gc;
extern Atom delatom;
extern Display* display;
extern Window root;
extern int screen;
extern map_strcol_t* colors; 

extern map_widloci_t* widlines;
extern map_widloci_t* widareas;
extern map_widarcssets_t* widarcssets;
extern map_widarcssets_t* widarcsareas;
extern map_widloci_t* widloci;
extern map_widloci_t* widpixels;
extern map_widnamedpoints_t* widnamedpoints;

static void draw(widget_t* w) {
    if (w->flags & F_HOVERED && w->flags & F_APPLYHOVER) {
        XSetWindowBackground(display, w->wid, w->hbg->pixel);
        XSetForeground(display, gc, w->hfg->pixel);
    #ifdef ENABLE_IMAGES
    } else if (w->img) {
        updateimg(w);
    #endif
    } else {
        XSetWindowBackground(display, w->wid, 0);
        XSetForeground(display, gc, w->fg->pixel);
    }
    XSetFont(display, gc, w->fs->fid);
    XClearWindow(display, w->wid);
    write_align(w, w->data, w->align, NULL);
    if (widareas)
        drawareas(w);
    if (widarcsareas)
        drawarcsareas(w);
    if (widlines)
        drawlines(w);
    if (widarcssets)
        drawarcs(w);
    if (widloci)
        drawpoints(w);
    if (widpixels)
        drawpixels(w);
    if (widnamedpoints)
        drawnamedpoints(w);
}

void trans_t_free(trans_t* p) {
    ((widget_t*)p)->free((widget_t*)p);;
    free(p);
}

trans_t* trans_t_init(trans_t* p, int ww, int hh) { 
    if (!p)
        return NULL;
    memset(p, 0, sizeof(trans_t));
    widget_t* w = (widget_t*)p;
    widget_t_init(w);

    p->free = trans_t_free;
    w->draw = draw;

    w->x = 0;
    w->y = 0;
    w->w = ww ? ww : 120;
    w->h = hh ? hh : 120;
    w->b = 0;
    w->m = 0;

    colors->get(colors, "lightgray", &w->bg);
    colors->get(colors, "black", &w->fg);

    colors->get(colors, "lightgray", &w->pbg);
    colors->get(colors, "black", &w->pfg);

    colors->get(colors, "lightgray", &w->hbg);
    colors->get(colors, "black", &w->hfg);

    XSetWindowAttributes wa = {0};   

    XVisualInfo vinfo;
    if (!XMatchVisualInfo(display, screen, 32, TrueColor, &vinfo)) {
        fprintf(stderr, "error, unable to use real transparency.");
        exit(EXIT_FAILURE);
    }
    wa.colormap = XCreateColormap(display, root, vinfo.visual, AllocNone);
    wa.border_pixel = 0;
    wa.background_pixel = 0;
    wa.background_pixmap = None;
    wa.save_under = True; 
    w->wid = XCreateWindow(display, root, w->x, w->y, w->w, w->h, w->b, vinfo.depth, InputOutput, vinfo.visual, CWColormap | CWBorderPixel | CWBackPixel | CWOverrideRedirect, &wa);

    w->mask = VisibilityChangeMask|ExposureMask|StructureNotifyMask;
    XSelectInput(display, w->wid, w->mask);

    XSetWMProtocols(display, w->wid, &delatom, 1);
    fixed(w, 0);

    return p;
}

