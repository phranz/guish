/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <sys/time.h>
#include "page.h"
#include "../dectypes.h"

extern GC gc;
extern map_widwidget_t* widgets;
extern Atom delatom;
extern Display* display;
extern Window root;
extern int screen;
extern map_strcol_t* colors; 
extern vec_wid_t* todel;

extern map_widloci_t* widlines;
extern map_widloci_t* widareas;
extern map_widarcssets_t* widarcssets;
extern map_widarcssets_t* widarcsareas;
extern map_widloci_t* widloci;
extern map_widloci_t* widpixels;
extern map_widnamedpoints_t* widnamedpoints;

static int fitflag = 0;

static void draw(widget_t* w) {
    if (w->flags & F_HOVERED && w->flags & F_APPLYHOVER) {
        XSetWindowBackground(display, w->wid, w->hbg->pixel);
        XSetForeground(display, gc, w->hfg->pixel);
    #ifdef ENABLE_IMAGES
    } else if (w->img) {
        updateimg(w);
    #endif
    } else {
        XSetWindowBackground(display, w->wid, w->bg->pixel);
        XSetForeground(display, gc, w->fg->pixel);
    }
    XClearWindow(display, w->wid);
    if (widareas)
        drawareas(w);
    if (widarcsareas)
        drawarcsareas(w);
    if (widlines)
        drawlines(w);
    if (widarcssets)
        drawarcs(w);
    if (widloci)
        drawpoints(w);
    if (widpixels)
        drawpixels(w);
    if (widnamedpoints)
        drawnamedpoints(w);
}

void equalize(widget_t* w) {
    page_t* p = (page_t*)w;
    if (!p || !p->subs->count)
        return;

    widget_t* s;

    int tw = w->w;
    int th = w->h;
    int ww = 0;
    int wh = 0;
    int px = 0;
    int py = 0;

    if (p->l == HL) {
        while ((tw % p->subs->count) || (tw % 2))
            ++tw;
        ww = tw / p->subs->count;
        wh = th;

        if (w->w != tw || w->h != th)
            resize(w, tw, th);

        for (size_t i=0; i<p->subs->count; ++i) {
            p->subs->get(p->subs, i, &s);
            resize(s, ww-(s->b*2), wh-(s->b*2));
            move(s, px, py);
            px = s->x + s->w + (s->b*2);
        }
    } else {
        while ((th % p->subs->count) || (th % 2))
            ++th;
        ww = w->w;
        wh = w->h / p->subs->count;

        if (w->w != tw || w->h != th)
            resize(w, tw, th);

        for (size_t i=0; i<p->subs->count; ++i) {
            p->subs->get(p->subs, i, &s);
            resize(s, ww-(s->b*2), wh-(s->b*2));
            move(s, px, py);
            py = s->y + s->h + (s->b*2);
        }
    }
}

void showsubs(widget_t* x) {
    page_t* p = (page_t*)x;
    widget_t* s;
    each(p->subs, s, show(s););
}

void hidesubs(widget_t* x) {
    page_t* p = (page_t*)x;
    widget_t* s;
    each(p->subs, s, hide(s););
}

void stylesubs(widget_t* w, const char* s) {
    page_t* p = (page_t*)w;
    widget_t* r;
    each(p->subs, r, style(r, s););
}

void freezesubs(widget_t* x) {
    page_t* p = (page_t*)x;
    widget_t* s;
    each(p->subs, s, freeze(s););
}

void unfreezesubs(widget_t* x) {
    page_t* p = (page_t*)x;
    widget_t* s;
    each(p->subs, s, unfreeze(s););
}

void setlayout(page_t* p, int t, int force) {
    if (t == p->l && !force)
        return;

    p->l = t;
    if (!p->subs->count)
        return;

    int xpos = 0;
    int ypos = 0;

    widget_t* w = (widget_t*)p;

    if (fitflag) {
        w->w = w->b*2 + w->m*2;
        w->h = w->b*2 + w->m*2;
    } else {
        w->w = p->max_w > w->w ? p->max_w : w->w;
        w->h = p->max_h > w->h ? p->max_h : w->h;
    }
    if (p->subs->count) {
        for (size_t i=0; i<p->subs->count; ++i) {
            widget_t* g;
            p->subs->get(p->subs, i, &g);
            if (t == HL) {
                if (fitflag) {
                    w->w += g->w + (g->b*2);
                    if (g->h + (g->b*2) > w->h)
                        w->h = g->h + (g->b*2) + (w->m*2);
                }
                if (!i) {
                    xpos = w->m;
                } else {
                    widget_t* h;
                    p->subs->get(p->subs, i-1, &h);
                    xpos = h->x + h->w + (h->b*2);
                }
                ypos = 0;
                do {
                    if ((fitflag && g->h + (g->b*2)) == p->max_h) {
                        ypos += w->m;
                        break;
                    }
                    if (w->align == A_MC) {
                        ypos += (p->max_h / 2) - ((g->h + (g->b*2)) / 2);
                    } else if (w->align == A_BC) {
                        ypos += p->max_h - (g->h + (g->b*2)) - w->m;
                    } else {
                        ypos += w->m;
                    }
                } while (0);
            } else {
                if (fitflag) {
                    w->h += g->h + (g->b*2);
                    if ((g->w + (g->b*2)) > w->w)
                        w->w = g->w + (g->b*2) + (w->m*2);
                }
                if (!i) {
                    ypos = w->m;
                } else {
                    widget_t* h;
                    p->subs->get(p->subs, i-1, &h);
                    ypos = h->y + h->h + (h->b*2);
                }
                xpos = 0;
                do {
                    if ((g->w + (g->b*2)) == p->max_w) {
                        xpos += w->m;
                        break;
                    }
                    if (p->valign == A_MR) {
                        xpos += p->max_w - (g->w + (g->b*2)) - w->m;
                    } else if (p->valign == A_MC) {
                        xpos += (p->max_w / 2) - ((g->w + (g->b*2)) / 2);
                    } else {
                        xpos += w->m;
                    }
                } while (0);
            }
            XMoveWindow(display, g->wid, xpos, ypos);
            g->x = xpos;
            g->y = ypos;
        }
        XResizeWindow(display, w->wid, w->w, w->h);
    }
}

void embed(widget_t* x, const char* id) {
    page_t* p = (page_t*)x;
    widget_t* w = NULL;
    char* s;

    vec_str_t* ids = alloc(vec_str_t);
    ssplit(id, ',', ids, NULL);

    for (size_t i=0; i<ids->count; ++i) {
        ids->get(ids, i, &s);

        unsigned long long eid = strtoull(s, 0, 10);
        do {
            if (widgets->get(widgets, eid, &w)) {
                XUnmapWindow(display, w->wid);
                XSync(display, False);
                bypasswm(w, 1);
                XSelectInput(display, w->wid, w->mask & ~StructureNotifyMask);
                XReparentWindow(display, w->wid, x->wid, 0, 0);
                w->x = 0;
                w->y = 0;
                if (p->max_w < (w->w + (w->b*2)) + (x->m*2))
                    p->max_w = (w->w + (w->b*2) + (x->m*2));
                if (p->max_h < (w->h + (w->b*2)) + (x->m*2))
                    p->max_h = (w->h + (w->b*2) + (x->m*2));
                XSync(display, False);
                if (w->flags & F_VISIBLE) {
                    hide(w);
                    show(w);
                }
                p->subs->push(p->subs, w);
                w->parent = x;
                break;
            }
        } while (0);
        zfree(s);
    }
    release(ids);
    setlayout(p, p->l, 1);
}

void fit_to_cont(widget_t* w) {
    page_t* p = (page_t*)w;
    fitflag = 1;
    setlayout(p, p->l, 1);
    fitflag = 0;
}

void fitembed(widget_t* w, const char* id) {
    fitflag = 1;
    embed(w, id);
    fitflag = 0;
}

void untie(widget_t* x, const char* id) {
    page_t* p = (page_t*)x;
    widget_t* w;
    widget_t* c;
    char* s;

    vec_str_t* ids = alloc(vec_str_t);
    ssplit(id, ',', ids, NULL);
    for (size_t i=0; i<ids->count; ++i) {
        ids->get(ids, i, &s);

        unsigned long long eid = strtoull(s, 0, 10);
        do {
            if (widgets->get(widgets, eid, &w)) {
                if (p->subs->count) {
                    for (size_t i=0; i<p->subs->count; ++i) {
                        p->subs->get(p->subs, i, &c);
                        if (w->wid == c->wid) {
                            XUnmapWindow(display, w->wid);
                            bypasswm(w, 0);
                            XSelectInput(display, w->wid, w->mask);
                            XSync(display, False);
                            XReparentWindow(display, w->wid, root, 0, 0);
                            if (w->flags & F_VISIBLE) {
                                hide(w);
                                show(w);
                            }
                            c->parent = NULL;
                            p->subs->remove(p->subs, i);
                            break;
                        }
                    }
                    break;
                }
            }
        } while (0);
        zfree(s);
    }
    release(ids);
    setlayout(p, p->l, 1);
}

void fituntie(widget_t* w, const char* id) {
    fitflag = 1;
    untie(w, id);
    fitflag = 0;
}

void explode(widget_t* x) {
    page_t* p = (page_t*)x;

    widget_t* w;
    p->max_w = 0;
    p->max_h = 0;
    each(p->subs, w,
        XUnmapWindow(display, w->wid);
        XSync(display, False);
        bypasswm(w, 0);
        XSelectInput(display, w->wid, w->mask);
        XSync(display, False);
        XReparentWindow(display, w->wid, root, 0, 0);
        if (w->flags & F_VISIBLE) {
            hide(w);
            show(w);
        }
        w->parent = NULL;
    );
    p->subs->clear(p->subs);
}

void child_going(widget_t* w, widget_t* c) {
    page_t* p = (page_t*)w;
    widget_t* x;
    each(p->subs, x,
        if (x == c) {
            p->subs->remove(p->subs, __i);
            x->parent = NULL;
        }
    );
}

void horizontal(widget_t* x) {
    page_t* p = (page_t*)x;
    widget_t* w;
    p->max_w = 0;
    p->max_h = 0;
    for (size_t i=0; i<p->subs->count; ++i) {
        p->subs->get(p->subs, i, &w);
        if (p->max_w < (w->w + (w->b*2)) + (x->m*2))
            p->max_w = (w->w + (w->b*2) + (x->m*2));
        if (p->max_h < (w->h + (w->b*2)) + (x->m*2))
            p->max_h = (w->h + (w->b*2) + (x->m*2));
    }
    setlayout(p, HL, 1);
}

void swap(widget_t* x, const char* e1, const char* e2) {
    page_t* p = (page_t*)x;
    widget_t* w1;
    widget_t* w2;
    widget_t* w;

    if (!p->subs || p->subs->count < 2)
        return;

    unsigned long long e1id = strtoull(e1, 0, 10);
    unsigned long long e2id = strtoull(e2, 0, 10);

    if (!e1 || !e2)
        return;

    if (!widgets->get(widgets, e1id, &w1))
        return;
    if (!widgets->get(widgets, e2id, &w2))
        return;

    int i1 = -1;
    int i2 = -1;
    for (size_t i=0; i<p->subs->count; ++i) {
        p->subs->get(p->subs, i, &w);
        if (w == w1)
            i1 = i;
        if (w == w2)
            i2 = i;
    }
    if (i1 < 0 || i2 < 0)
        return;

    p->subs->put(p->subs, i1, w2);
    p->subs->put(p->subs, i2, w1);
    setlayout(p, p->l, 1);
}

void invert(widget_t* x) {
    page_t* p = (page_t*)x;

    if (!p->subs || p->subs->count < 2)
        return;
    vlrev(widget_t*, p->subs);
    setlayout(p, p->l, 1);
}

void vertical(widget_t* x) {
    page_t* p = (page_t*)x;
    widget_t* w;
    p->max_w = 0;
    p->max_h = 0;
    for (size_t i=0; i<p->subs->count; ++i) {
        p->subs->get(p->subs, i, &w);
        if (p->max_w < (w->w + (w->b*2)) + (x->m*2))
            p->max_w = (w->w + (w->b*2) + (x->m*2));
        if (p->max_h < (w->h + (w->b*2)) + (x->m*2))
            p->max_h = (w->h + (w->b*2) + (x->m*2));
    }
    setlayout(p, VL, 1);
}

void page_t_free(page_t* p) {
    widget_t* s;
    each(p->subs, s,
        s->flags |= F_GARBAGE;
        todel->push(todel, s->wid);
    );
    explode((widget_t*)p);
    release(p->subs);
    ((widget_t*)p)->free((widget_t*)p);
    free(p);
}

page_t* page_t_init(page_t* p, int ww, int hh) { 
    if (!p)
        return NULL;
    memset(p, 0, sizeof(page_t));
    widget_t* w = (widget_t*)p;
    widget_t_init(w);

    p->subs = alloc(vec_widget_t);
    p->l = HL;

    p->free = page_t_free;
    w->draw = draw;

    w->x = 0;
    w->y = 0;
    w->w = ww ? ww : 200;
    w->h = hh ? hh : 200;
    w->b = 0;

    colors->get(colors, "lightgray", &w->bg);
    colors->get(colors, "black", &w->fg);

    colors->get(colors, "gray", &w->pbg);
    colors->get(colors, "black", &w->pfg);

    colors->get(colors, "gray", &w->hbg);
    colors->get(colors, "black", &w->hfg);

    XSetWindowAttributes wa = {0};
    w->wid = XCreateWindow(display, root, w->x, w->y, w->w, w->h, w->b, DefaultDepth(display, screen), InputOutput, DefaultVisual(display, screen), CWOverrideRedirect, &wa);

    w->mask = VisibilityChangeMask|ExposureMask|StructureNotifyMask;
    XSelectInput(display, w->wid, w->mask);

    XSetWMProtocols(display, w->wid, &delatom, 1);
    fixed(w, 0);

    w->align = A_TC;
    p->valign = A_ML;
    return p;
}

