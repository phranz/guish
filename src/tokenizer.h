/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef TOKENIZER_H
#define TOKENIZER_H


typedef struct vec_token_t vec_token_t;
typedef struct vec_token_t phrase_t;
typedef struct block_t block_t;

enum toktype {
    T_ELEM,
    T_DEFINE,
    T_DEFATTR,
    T_RANGE,
    T_CODE,
    T_ENDCODE,
    T_QUOTE,
    T_DQUOTE,
    T_CMD,
    T_EXPR,
    T_ENDEXPR,
    T_SLICE,
    T_ENDSLICE,
    T_SHELLSUB,
    T_WIDSUB,
    T_VARSUB,
    T_ARGSSUB,
    T_ATTR,
    T_GLOB,
    T_INVALID,
    T_BLOCK,
    T_END,
};

#define TOKNAMES()             \
    const char* toknames[] = { \
        "T_ELEM",              \
        "T_DEFINE",            \
        "T_DEFATTR",           \
        "T_RANGE",             \
        "T_CODE",              \
        "T_ENDCODE",           \
        "T_QUOTE",             \
        "T_DQUOTE",            \
        "T_CMD",               \
        "T_EXPR",              \
        "T_ENDEXPR",           \
        "T_SLICE",             \
        "T_ENDSLICE",          \
        "T_SHELLSUB",          \
        "T_WIDSUB",            \
        "T_VARSUB",            \
        "T_ARGSSUB",           \
        "T_ATTR",              \
        "T_GLOB",              \
        "T_INVALID",           \
        "T_BLOCK",             \
        "T_END",               \
    }

typedef struct token_t {
    int type;
    union {
        char* data;
        block_t* block;
    };
    unsigned long long lineno;
    char* source;
    void (*free)(struct token_t*);
} token_t;

token_t* copy_token(token_t*);
token_t* token_t_init(token_t*);
void token_t_free(token_t*);

void release_phrase(vec_token_t*);
phrase_t* copy_phrase(phrase_t*);
phrase_t* tokenize();

#endif
