/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef WIDGET_H
#define WIDGET_H

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define MOUSE_LEFT Button1
#define MOUSE_MIDDLE Button2
#define MOUSE_RIGHT Button3
#define MOUSE_SCROLLUP Button4
#define MOUSE_SCROLLDOWN Button5

#define sw() (DisplayWidth(display, screen))
#define sh() (DisplayHeight(display, screen))

typedef struct intint_t intint_t;
typedef struct widint_t widint_t;
typedef struct vec_str_t vec_str_t;
typedef struct vec_int_t vec_int_t;
typedef struct vec_points_t vec_points_t;
typedef struct vec_arcs_t vec_arcs_t;
typedef struct map_widint_t map_widint_t;
typedef struct intint_intint_t intint_intint_t;
typedef struct timeval timeval;

#ifdef ENABLE_CONTROL
typedef struct vec_uint_t vec_uint_t;
#endif
#ifdef ENABLE_IMAGES
#include <Imlib2.h>
#endif

typedef enum {
    A_TC,
    A_MC,
    A_BC,
    A_TL,
    A_ML,
    A_BL,
    A_TR,
    A_MR,
    A_BR,
} align;

typedef struct points_t {
    XPoint* ps;
    size_t count;
    void (*free)(struct points_t*);
} points_t;

points_t* points_t_init(points_t*);
void points_t_free(points_t*);

typedef struct namedpoints_t {
    vec_int_t* ps;
    vec_str_t* names;
    vec_str_t* cols;
    void (*free)(struct namedpoints_t*);
} namedpoints_t;

namedpoints_t* namedpoints_t_init(namedpoints_t*);
void namedpoints_t_free(namedpoints_t*);

typedef struct loci_t {
    vec_points_t* loci;
    vec_str_t* cols;
    void (*free)(struct loci_t*);
} loci_t;

loci_t* loci_t_init(loci_t*);
void loci_t_free(loci_t*);

typedef struct arcssets_t {
    vec_arcs_t* arcssets;
    vec_str_t* cols;
    void (*free)(struct arcssets_t*);
} arcssets_t;

arcssets_t* arcssets_t_init(arcssets_t*);
void arcssets_t_free(arcssets_t*);

typedef struct arcs_t {
    XArc* as;
    size_t count;
    void (*free)(struct arcs_t*);
} arcs_t;

arcs_t* arcs_t_init(arcs_t*);
void arcs_t_free(arcs_t*);

typedef struct widget_t {
    struct widget_t* parent;

    int etype;
    #define F_VISIBLE 1
    #define F_GHOST (1 << 1)
    #define F_CLOSED (1 << 2)
    #define F_LPRESSED (1 << 3)
    #define F_RPRESSED (1 << 4)
    #define F_MPRESSED (1 << 5)
    #define F_WFIXED (1 << 6)
    #define F_HFIXED (1 << 7)
    #define F_LDCLICKED (1 << 8)
    #define F_RDCLICKED (1 << 9)
    #define F_MDCLICKED (1 << 10)
    #define F_FREEZED (1 << 11)
    #define F_EXT (1 << 12)
    #define F_GRIP (1 << 13)
    #define F_CHECKED (1 << 14)
    #define F_HOVERED (1 << 15)
    #define F_APPLYHOVER (1 << 16)
    #define F_NLONRETURN (1 << 17)
    #define F_TRAYED (1 << 18)
    #define F_BYPASS (1 << 19)
    #define F_FOCUSED (1 << 20)
    #define F_XMOV (1 << 21)
    #define F_YMOV (1 << 22)
    #define F_GARBAGE (1 << 23)
    unsigned long flags;

    char* title;
    char* data;

    int x;
    int y;
    int w;
    int h;
    int b;
    int m;
    int mask;
    int trans;
    size_t maxwcont;
    size_t maxhcont;
    int fh;

    vec_str_t* ll;
    align align;
    Window wid;
    int pid;
    XColor* fg;
    XColor* bg;
    XColor* pfg;
    XColor* pbg;
    XColor* hfg;
    XColor* hbg;
    int lw;
    XFontStruct* fs;

    widint_t* relal;
    map_widint_t* related;
    #ifdef ENABLE_CONTROL
    vec_uint_t* to_release;
    #endif

    #ifdef ENABLE_IMAGES
    Imlib_Image img;
    #endif

    struct timeval tpress;

    void (*draw)(struct widget_t*);
    void (*free)(struct widget_t*);
} widget_t;

widget_t* widget_t_init(widget_t*);
void widget_t_free(widget_t*);

intint_t getcenter(widget_t*);
void closed(widget_t*);
void write_align(widget_t*, const char*, align, intint_t*);
void relaxed(widget_t*, int);
void fixed(widget_t*, int);
void wfixed(widget_t*, int);
void hfixed(widget_t*, int);
void clear(widget_t*);
void setdefault(widget_t*);
void tray(widget_t*);
void transparent(widget_t*);
void ghost(widget_t*);
void show(widget_t*);
void hide(widget_t*);
void focus(widget_t*);
void center(widget_t*);
void settop(widget_t*);
void setbottom(widget_t*);
void wclose(widget_t*);
void lower(widget_t*);
void wraise(widget_t*);
void maximize(widget_t*);
void resize(widget_t*, int, int);
void move(widget_t*, int, int);
void movealigns(widget_t*, widget_t*, const char*);
void style(widget_t*, const char*);
void width(widget_t*);
void height(widget_t*);
void freeze(widget_t*);
void unfreeze(widget_t*);
void fit(widget_t*);
void fill(widget_t*, int);
void fill_right(widget_t*, int);
void fill_bottom(widget_t*, int);
void entitle(widget_t*, const char*);
void fullscreen(widget_t*);
void settext(widget_t*, const char*);
void addtext(widget_t*, const char*);
void tgrip(widget_t*);
void txmov(widget_t*);
void tymov(widget_t*);

void pressed(widget_t*, int);
void scroll(widget_t*, int);
void released(widget_t*, int);
void moved(widget_t*);
void resized(widget_t*);
void penter(widget_t*);
void pleave(widget_t*);
void focused(widget_t*);
void unfocused(widget_t*);

XColor* addcol(const char*);
int iswin(Window, Window);
Window widpid(Window, int);
void bypasswm(widget_t*, int);
void event(widget_t*, XEvent*);

void addpoints(widget_t*, char*, vec_int_t*);
void addpixels(widget_t*, char*, vec_int_t*);
void addlines(widget_t*, char*, vec_int_t*);
void addarcs(widget_t*, int, char*, vec_int_t*);

void addarea(widget_t*, char*, vec_int_t*);
void addarcarea(widget_t*, char*, int, int, int, int, int, int);
void addnamedpoint(widget_t*, char*, char*, int, int);

void drawnamedpoints(widget_t*);
void drawpoints(widget_t*);
void drawpixels(widget_t*);
void drawlines(widget_t*);
void drawarcs(widget_t*);
void drawareas(widget_t*);
void drawarcsareas(widget_t*);

widget_t* make(int, Window, int, int);
void del(unsigned long long);
intint_intint_t pcoords();
intint_t textpos(widget_t*, const char*, align, size_t, size_t);
void relate(struct widget_t*, struct widget_t*, const char*);

#ifdef ENABLE_CONTROL
void sendkeys(widget_t*, const char*);
void sendcontrolkeys(widget_t*, const char*);
#endif

#ifdef ENABLE_IMAGES
void loadimg(widget_t*, const char*);
void updateimg(widget_t*);
#endif

#endif
