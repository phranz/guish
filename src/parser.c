/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*************************************************************************/

#include "dectypes.h"
#include "cutils.h"
#include "tokenizer.h"
#include "evaluator.h"
#include "debug.h"

void decap_block(vec_token_t* block) {
    token_t* t;
    block->pop_back(block, &t);
    release(t);
    block->pop(block, &t);
    release(t);
}

token_t* repr(token_t* x, int decap) {
    if (!x)
        return NULL;
    if (x->type != T_BLOCK)
        return x;

    token_t* r;
    if (decap)
        decap_block(x->block->data);
    r = alloc(token_t);
    r->type = T_QUOTE;
    for (size_t i=0; i<x->block->data->count; ++i) {
        token_t* t;
        x->block->data->get(x->block->data, i, &t);
        if (!i) {
            r->lineno = t->lineno;
            r->source = t->source;
        }
        r->data = sadd(r->data, t->data);
        if (i < x->block->data->count-1)
            r->data = sadd(r->data, " ");
    }
    release(x);
    return r;
}

vec_token_t* parse_phrase(vec_token_t* phrase, scope_t* cs) {
    if (!phrase)
        return NULL;
    showtokens(phrase);
    vec_token_t* objects = alloc(vec_token_t);
    token_t* arg;
    token_t* t;
    while (phrase->first(phrase, &t)) {
        if (t->type == T_CODE) {
            arg = alloc(token_t);
            int c = 0;
            arg->type = T_BLOCK;
            arg->lineno = t->lineno;
            arg->source = t->source;
            arg->block = alloc(block_t);
            arg->block->env = copy_scope(cs);
            while (phrase->pop_back(phrase, &t)) {
                if (t->type == T_CODE)
                    ++c;
                else if (t->type == T_ENDCODE)
                    --c;
                if (!c)
                    break;
                arg->block->data->push(arg->block->data, t);
            }
            if (t->type == T_ENDCODE)
                arg->block->data->push(arg->block->data, t);
        } else {
            phrase->pop_back(phrase, &t);
            arg = t;
        }
        objects->push(objects, arg);
    }
    return objects;
}

vec_phrase_t* parse_block(vec_token_t* block) {
    if (!block)
        return NULL;

    token_t* t;
    int code = 1;
    vec_phrase_t* ps = alloc(vec_phrase_t);
    phrase_t* p = alloc(vec_token_t);

    block->pop_back(block, &t);
    release(t);
    while (block->pop_back(block, &t)) {
        if (code < 2 && t->type == T_END) {
            ps->push(ps, p);
            p = alloc(vec_token_t);
            release(t);
            continue;
        }
        if (t->type == T_CODE)
            ++code;
        else if (t->type == T_ENDCODE)
            --code;
        if (code) {
            p->push(p, t);
        } else {
            if (p->count) {
                ps->push(ps, p);
                p = NULL;
                release(t);
                break;
            } else {
                release_phrase(p);
            }
            release(t);
        }  
    }
    return ps;
}
