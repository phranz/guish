/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef SOURCEDRIVER_H
#define SOURCEDRIVER_H

typedef struct vec_sourcedata_t vec_sourcedata_t;

enum sources {
    S_NOSRC,
    S_SOURCE,
    S_CMDLINE,
    S_TTY,
    S_STDIN,
    S_ALL,
};

typedef struct head_t {
    size_t pos;
    int c;
    int o;
} head_t;

typedef struct sourcedata_t {
    int s;
    char* d;
    char* n;
    FILE* i;
    head_t h;
    struct sourcedata_t* p;
    unsigned long long lineno;
    int nb;

    void (*free)(struct sourcedata_t*);
} sourcedata_t;

sourcedata_t* sourcedata_t_init(sourcedata_t*);
void sourcedata_t_free(sourcedata_t*);

void sourcedriver_init();
void sourcedriver_free();
void sinfo(const char*, unsigned long long);
int new_source(int, void*);
sourcedata_t* evalsrc(char*, unsigned long, char*);
void push(int);
head_t* pull();
int source_exahusted();
void next_source();

#define valid_source() (current && current->s != S_NOSRC)

#endif


