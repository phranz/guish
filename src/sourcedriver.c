/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h> 

#include "dectypes.h"
#include "sourcedriver.h"
#include "tokenizer.h"
#include "main.h"
#include "debug.h"

extern unsigned long options;

sourcedata_t* current;
static vec_sourcedata_t* allsources;

static void fill_source(sourcedata_t* ds, int ps, char* pd, char* pn, FILE* pi, int phpos, char phc, char pho, sourcedata_t* pp, unsigned long long pl) {
        ds->s = ps;
        ds->d = (char*)pd;
        ds->n = pn;
        ds->i = pi;
        ds->h.pos = phpos;
        ds->h.c = phc;
        ds->h.o = pho;
        ds->p = pp;
        ds->lineno = pl;
}

static sourcedata_t* has(int s, char* data) {
    if (!current)
        return NULL;

    for (size_t i=0; i<allsources->count; ++i) {
        sourcedata_t* ptr;
        allsources->get(allsources, i, &ptr);
        if (ptr->s == s && !strcmp(ptr->d, data))
            return ptr;
    }
    return NULL;
}

int new_source(int s, void* data) {
    if (s == S_NOSRC)
        return 0;

    debug("source type: %d\n", s);
    debug("allsources count: %lu\n", (unsigned long)allsources->count);

    sourcedata_t* p = NULL;
    switch (s) {
        case S_SOURCE: {
            if (!data)
                break;

            p = has(s, (char*)data);
            if (p) {
                current = p;
                p->h.pos = 0;
                p->h.c = 0;
                p->h.o = 0;
                rewind(p->i);
                p->lineno = 1;
                break;
            }
            char* t = salloc((char*)data);
            struct stat sb;

            if (stat(t, &sb) == -1) {
                perror("stat");
                fprintf(stderr, "error, unable to stat file '%s'\n", t);
                exit(EXIT_FAILURE);
            }
            FILE* f;
            sourcedata_t* ns = alloc(sourcedata_t);

            if (S_IFIFO == (sb.st_mode & S_IFMT)) {
                f = fopen(t, "r+");
                if (f)
                    fcntl(fileno(f), F_SETFL, O_NONBLOCK);
                ns->nb = 1;
            } else {
                f = fopen(t, "r");
            }
            if (!f) {
                perror("fopen");
                fprintf(stderr, "error, unable to open file '%s'\n", t);
                exit(EXIT_FAILURE);
            }
            fill_source(ns, s, t, salloc(t), f, 0, 0, 0, current ? current : NULL, 1);
            current = ns;
            allsources->push(allsources, current);
            break;
        }
        case S_CMDLINE: {
            if (!data || has(s, (char*)data))
                break;

            sourcedata_t* ns = alloc(sourcedata_t);
            fill_source(ns, s, salloc((char*)data), salloc("<cmdline>"), NULL, 0, 0, 0, current ? current : NULL, 1);
            current = ns;
            allsources->push(allsources, current);
            break;
        }
        case S_TTY: {
            char* tty = ctermid(NULL);
            if (!tty || has(s, tty))
                break;

            tty = salloc(tty);
            int fd = open(tty, O_RDONLY | O_NONBLOCK);
            FILE* f = fdopen(fd, "r");

            if (!f) {
                fprintf(stderr, "error, unable to open terminal '%s'\n", tty);
                exit(EXIT_FAILURE);
            }
            struct termios nt;
            struct termios ot;
            tcgetattr(fd, &ot);

            nt = ot;
            nt.c_cc[VEOF] = 0;
            tcsetattr(fd, TCSANOW, &nt);

            sourcedata_t* ns = alloc(sourcedata_t);
            ns->nb = 1;
            fill_source(ns, s, tty, salloc("<tty>"), f, 0, 0, 0, current ? current : NULL, 1);
            current = ns;
            allsources->push(allsources, current);
            break;
        }
        case S_STDIN: {
            sourcedata_t* ns = alloc(sourcedata_t);
            if (isatty(fileno(stdin))) {
                fcntl(fileno(stdin), F_SETFL, O_NONBLOCK);
                ns->nb = 1;
                struct termios nt;
                struct termios ot;
                tcgetattr(fileno(stdin), &ot);

                nt = ot;
                nt.c_cc[VEOF] = 0;
                tcsetattr(fileno(stdin), TCSANOW, &nt);
            }
            FILE* fin = fdopen(fileno(stdin), "r");
            fill_source(ns, s, NULL, salloc("<stdin>"), fin, 0, 0, 0, current ? current : NULL, 1);
            current = ns;
            allsources->push(allsources, current);
            break;
        }
        default:
            fprintf(stderr, "error, unknown source '%d'!\n", s);
            exit(EXIT_FAILURE);
    }
    return 1;
}

sourcedata_t* evalsrc(char* d, unsigned long ln, char* sn) {
    sourcedata_t* seval = alloc(sourcedata_t);
    
    seval->s = S_CMDLINE;
    seval->d = d;
    seval->n = sn;
    seval->i = NULL;
    seval->h.pos = 0;
    seval->h.c = 0;
    seval->h.o = 0;
    seval->lineno = ln;

    return seval;
}

head_t* pull() {
    if (!current || !current->s)
        return NULL;

    int s;
    FILE* i;
    head_t* h;
    char* d;

    s = current->s;
    i = current->i;
    h = &current->h;
    d = current->d;

    switch (s) {
        case S_CMDLINE:
            if (d && (h->pos < strlen(d))) {
                h->c = d[h->pos++];
                h->o = h->c;
            } else {
                h->c = EOF;
            }
            break;
        default:
            h->c = getc(i);
            h->pos++;
    }
    return h;
}

void push(int c) {
    if (c == EOF)
        return;
    switch (current->s) {
        case S_CMDLINE:
            --current->h.pos;
            break;
        default:
            if (current->i)
                ungetc(c, current->i);
    }
}

int source_exahusted() {
    head_t* h = pull();
    if (h && h->c != EOF) {
        push(h->c);
        return 0;
    }
    return 1;
}

void next_source() {
    if (!current || current->nb)
        return;

    int fb = options & FALLBACK_ON_TTY;
    switch (current->s) {
        case S_NOSRC: 
        case S_TTY:
            break;
        case S_STDIN:
        case S_CMDLINE:
            if (fb) {
                new_source(S_TTY, NULL);
                break;
            }
            current->s = S_NOSRC;
            break;
        case S_SOURCE: {
            current = current->p;
            if ((!current || !current->s) && fb)
                new_source(S_TTY, NULL);
            break;
        }
        default:
            current->s = S_NOSRC;
    }
}

void sinfo(const char* sname, unsigned long long tl) {
    fprintf(stderr, "[%s] at '%llu': ", sname, tl);
}

void sourcedata_t_free(sourcedata_t* d) {
    zfree(d->d);
    zfree(d->n);
    if (d->i)
        fclose(d->i);
    free(d);
}

sourcedata_t* sourcedata_t_init(sourcedata_t* d) {
    if (!d)
        return NULL;
    memset(d, 0, sizeof(sourcedata_t));
    d->free = sourcedata_t_free;
    return d;
}

void sourcedriver_free() {
    sourcedata_t* s;
    each(allsources, s, release(s););
    release(allsources);
}

void sourcedriver_init() {
    allsources = alloc(vec_sourcedata_t);
}
