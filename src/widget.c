/*************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include "dectypes.h"
#include "widgets.h"
#include "widget.h"
#include "syntax.h"
#include "evaluator.h"
#include "debug.h"
#include "main.h"

#define SPIXEL 2

typedef struct point_t {
    int x;
    int y;
} point_t;

int dirty;
extern char buf[BSIZ];

extern GC gc;
extern Atom delatom;
extern Display* display;
extern Window root;
extern int screen;
extern map_strcol_t* colors; 
extern unsigned long flags;

extern map_widloci_t* widlines;
extern map_widloci_t* widareas;
extern map_widarcssets_t* widarcssets;
extern map_widarcssets_t* widarcsareas;
extern map_widloci_t* widloci;
extern map_widloci_t* widpixels;
extern map_widnamedpoints_t* widnamedpoints;

extern map_widwidget_t* widgets;
extern unsigned long options;

static point_t lrpos;
static point_t fp;

#ifdef ENABLE_CONTROL

#include <X11/keysym.h>
#include <X11/extensions/XTest.h>

static map_charcstr_t* ksyms;

static void fillksyms() {
    if (ksyms)
        return;

    ksyms = alloc(map_charcstr_t);

    ksyms->insert(ksyms, ' ', "space");
    ksyms->insert(ksyms, '-', "minus");
    ksyms->insert(ksyms, '+', "plus");
    ksyms->insert(ksyms, '.', "period");
    ksyms->insert(ksyms, ',', "comma");
    ksyms->insert(ksyms, ':', "colon");
    ksyms->insert(ksyms, ';', "semicolon");
    ksyms->insert(ksyms, '!', "exclam");
    ksyms->insert(ksyms, '$', "dollar");
    ksyms->insert(ksyms, '%', "percent");
    ksyms->insert(ksyms, '&', "ampersand");
    ksyms->insert(ksyms, '\'', "apostrophe");
    ksyms->insert(ksyms, '(', "parenleft");
    ksyms->insert(ksyms, ')', "parenright");
    ksyms->insert(ksyms, '*', "asterisk");
    ksyms->insert(ksyms, '<', "less");
    ksyms->insert(ksyms, '=', "equal");
    ksyms->insert(ksyms, '>', "greater");
    ksyms->insert(ksyms, '?', "question");
    ksyms->insert(ksyms, '@', "at");
    ksyms->insert(ksyms, '[', "bracketleft");
    ksyms->insert(ksyms, '\\', "backslash");
    ksyms->insert(ksyms, '/', "slash");
    ksyms->insert(ksyms, ']', "bracketright");
    ksyms->insert(ksyms, '^', "asciicircum");
    ksyms->insert(ksyms, '_', "underscore");
    ksyms->insert(ksyms, '`', "grave");
    ksyms->insert(ksyms, '{', "braceleft");
    ksyms->insert(ksyms, '}', "braceright");
    ksyms->insert(ksyms, '~', "asciitilde");
}

void sendkeys(widget_t* w, const char* s) {
    if (!s)
        return;
    fillksyms();

    const char* r;
    char* d;
    char C[2] = {0, 0};

    focus(w);
    for (size_t i=0; i<strlen(s); ++i) {
        C[0] = s[i];
        d = sadd(NULL, C);
        const char* k = ksyms->get(ksyms, s[i], &r) ? r : d;
        unsigned int kc = XKeysymToKeycode(display, XStringToKeysym(k));
        zfree(d);
        XTestFakeKeyEvent(display, kc, True, 0);
        XTestFakeKeyEvent(display, kc, False, 0);
        XFlush(display);
    }
    unsigned int rl;
    each(w->to_release, rl,
        XTestFakeKeyEvent(display, rl, False, 0);
    );
    w->to_release->clear(w->to_release);
}

void sendcontrolkeys(widget_t* w, const char* s) {
    if (!s)
        return;
    fillksyms();

    char* r;
    vec_str_t* keys = alloc(vec_str_t);
    ssplit(s, ',', keys, NULL);

    focus(w);
    for (size_t i=0; i<keys->count; ++i) {
        int keep = 0;
        keys->get(keys, i, &r);
        if (r[0] == '+') {
            keys->remove(keys, i);
            keep = 1;
        }
        unsigned int kc = XKeysymToKeycode(display, XStringToKeysym(r));
        XTestFakeKeyEvent(display, kc, True, 0);
        if (keep)
            w->to_release->push(w->to_release, kc);
        else
            XTestFakeKeyEvent(display, kc, False, 0);

        XFlush(display);
        zfree(r);
    }
    release(keys);
}
#endif

#ifdef ENABLE_IMAGES
#include <Imlib2.h>

void loadimg(widget_t* w, const char* p) {
    if (!w || (!w->img && !p))
        return;

    if (!p) {
        if (w->img) {
            imlib_context_set_image(w->img);
            imlib_free_image();
            w->img = NULL;
        }
        return;
    }
    if (w->img) {
        imlib_context_set_image(w->img);
        imlib_free_image();
    }
    imlib_context_set_display(display);
    imlib_context_set_visual(DefaultVisual(display, screen));
    imlib_context_set_colormap(DefaultColormap(display, screen));

    w->img = imlib_load_image(p);
}

void updateimg(widget_t* w) {
    if (!w || !w->img)
        return;

    Imlib_Image buffer = imlib_create_image(w->w, w->h);
    imlib_context_set_image(w->img);

    int ww = imlib_image_get_width();
    int hh = imlib_image_get_height();

    imlib_context_set_blend(1);
    imlib_context_set_image(buffer);
    imlib_context_set_color(w->bg->red, w->bg->green, w->bg->blue, 255);
    imlib_image_fill_rectangle(0, 0, w->w, w->h);

    imlib_blend_image_onto_image(w->img, 0, 0, 0, ww, hh, 0, 0, w->w, w->h);

    imlib_context_set_blend(0);
    Pixmap pix;
    pix = XCreatePixmap(display, root, w->w, w->h, DefaultDepth(display, screen));

    imlib_context_set_drawable(pix);
    imlib_render_image_on_drawable_at_size(0, 0, w->w, w->h);
    XSetWindowBackgroundPixmap(display, w->wid, pix);

    imlib_context_set_image(buffer);
    imlib_free_image();
    XFreePixmap(display, pix);
}

#endif

void pass() {
}

intint_intint_t pcoords() {
    intint_t C;
    intint_t c;
    intint_intint_t r;

    Window w;
    unsigned int m;

    XQueryPointer(display, root, &w, &w, &C.key, &C.val, &c.key, &c.val, &m);
    r.key = C;
    r.val = c;

    return r;
}

static int getwidpid(Window w) {
    int pid = 0;
    Atom a = XInternAtom(display, "_NET_WM_PID", True);
    XTextProperty d;
    XGetTextProperty(display, w, &d, a);

    if (d.nitems) {
        pid = *((int*)(unsigned char*)d.value);
        XFree(d.value);
    }
    return pid;
}

static void setwidpid(Window w) {
    Atom a = XInternAtom(display, "_NET_WM_PID", False);
    sprintf(buf, "%d", getpid());
    XChangeProperty(display, w, a, XA_STRING, 8, PropModeReplace, (unsigned char*)buf, strlen(buf));
}

int iswin(Window p, Window id) {
    if (p == id)
        return 1;

    Window  rr;
    Window  pr;
    Window *cl = NULL;
    unsigned int l = 0;
    int found = 0;

    XQueryTree(display, p, &rr, &pr, &cl, &l);
    for (size_t i=0; i<l && !found; ++i)
        found = iswin(cl[i], id);
    XFree(cl);
    return found;
}

static Window parentof(Window w) {
    Window rr;
    Window pr;
    Window *cl = NULL;
    unsigned int l = 0;

    XQueryTree(display, w, &rr, &pr, &cl, &l);
    XFree(cl);
    return pr;
}

static intint_t coords(Window w) {
    XWindowAttributes wa;
    Window cr;
    intint_t c;
    c.key = 0;
    c.val = 0;

    Window rr;
    Window pr;
    Window *cl = NULL;
    unsigned int l = 0;

    XQueryTree(display, w, &rr, &pr, &cl, &l);
    XTranslateCoordinates(display, w, root, 0, 0, &c.key, &c.val, &cr);
    XGetWindowAttributes(display, w, &wa);

    c.key = c.key ? c.key - wa.x : wa.x;
    c.val = c.val ? c.val - wa.y : wa.y;
    XFree(cl);
    return c;
}

Window widpid(Window w, int p) {
    Window  rr;
    Window  pr;
    Window *cl = NULL;
    unsigned int l = 0;
    Window f = 0;

    int pid = getwidpid(w);

    debug("pid: %d, p: %d\n", pid, p);
    if (pid && p == pid)
        return w;
    XQueryTree(display, w, &rr, &pr, &cl, &l);
    for (size_t i=0; i<l; ++i)
        f = widpid(cl[i], p);
    XFree(cl);
    return f;
}

static void setclass(widget_t* w, const char* t) {
    pid_t pid = getpid();
    XClassHint ch;

    ch.res_name = (char*)(t ? t : ename(w->etype));

    memset(buf, 0, sizeof(buf));
    sprintf(buf, "guish-%d", pid);

    ch.res_class = buf;
    XSetClassHint(display, w->wid, &ch);
}

widget_t* make(int e, unsigned long wid, int ww, int hh) {
    widget_t* w = NULL;

    switch (e) {
        case E_BUTTON:
            w = (widget_t*)alloc(button_t, ww, hh);
            break;
        case E_INPUT:
            w = (widget_t*)alloc(input_t, ww, hh);
            break;
        case E_PAGE:
            w = (widget_t*)alloc(page_t, ww, hh);
            break;
        case E_LABEL:
            w = (widget_t*)alloc(label_t, ww, hh);
            break;
        case E_CHECKBOX:
            w = (widget_t*)alloc(checkbox_t, ww, hh);
            break;
        case E_TRANS:
            w = (widget_t*)alloc(trans_t, ww, hh);
            break;
        default: {
            if (!wid)
                break;
            w = alloc(widget_t);
            w->wid = wid;
            w->flags |= F_EXT;
            w->pid = getwidpid(w->wid);

            XWindowAttributes wa;
            XGetWindowAttributes(display, w->wid, &wa);
            w->w = wa.width;
            w->h = wa.height;
            w->b = wa.border_width;
        }
    }
    if (w) {
        dirty = 1;
        if (!gc) {
            XGCValues v = {0};
            gc = XCreateGC(display, w->wid, 0, &v);
        }
        if (!wid) {
            w->etype = e;
            setclass(w, NULL);
            setwidpid(w->wid);
        }
        if (options & SHOW_ON_CREATE)
            show(w);
        if (widgets->full(widgets))
            widgets->rehash(widgets);
        center(w);
        widgets->insert(widgets, w->wid, w);
        return w;
    }
    return NULL;
}

void del(unsigned long long id) {
    widget_t* w;
    if (widgets->get(widgets, id, &w)) {
        debug("deleting window %llu\n", id);
        widgets->remove(widgets, id);
        if (w) {
            switch (w->etype) {
                case E_BUTTON:
                    ((button_t*)w)->free((button_t*)w);
                    break;
                case E_INPUT:
                    ((input_t*)w)->free((input_t*)w);
                    break;
                case E_PAGE:
                    ((page_t*)w)->free((page_t*)w);
                    break;
                case E_LABEL:
                    ((label_t*)w)->free((label_t*)w);
                    break;
                case E_CHECKBOX:
                    ((checkbox_t*)w)->free((checkbox_t*)w);
                    break;
                case E_TRANS:
                    ((trans_t*)w)->free((trans_t*)w);
                    break;
                default: 
                    if (w->flags & F_EXT)
                        w->free(w);
            }
        }
    }
}

intint_t textpos(widget_t* w, const char* s, align a, size_t i, size_t t) {
    intint_t c;

    size_t l = strlen(s);
    size_t ls = XTextWidth(w->fs, s, l);

    c.key = w->m;
    c.val = w->m;

    if (w->maxwcont < ls)
        w->maxwcont = ls;

    switch (a) {
        case A_TC:
            c.key = (w->w - ls) / 2;
            c.val = (w->fh * (i + 1)) + w->m;
            break;
        case A_MC:
            c.key = (w->w - ls) / 2;
            c.val = ((w->h - (w->fh * t)) / 2) + (w->fh * (i + 1));
            break;
        case A_BC:
            c.key = (w->w - ls) / 2;
            c.val = (w->h - ((t - (i + 1)) * w->fh)) - w->m;
            break;
        case A_TL:
            c.key = w->m;
            c.val = (w->fh * (i + 1)) + w->m;
            break;
        case A_ML:
            c.key = w->m;
            c.val = ((w->h - (w->fh * t)) / 2) + (w->fh * (i + 1));
            break;
        case A_BL:
            c.key = w->m;
            c.val = (w->h - ((t - (i + 1)) * w->fh)) - w->m;
            break;
        case A_TR:
            c.key = w->w - ls - w->m;
            c.val = (w->fh * (i + 1)) + w->m;
            break;
        case A_MR:
            c.key = w->w - ls - w->m; 
            c.val = ((w->h - (w->fh * t)) / 2) + (w->fh * (i + 1));
            break;
        case A_BR:
            c.key = w->w - ls - w->m; 
            c.val = (w->h - ((t - (i + 1)) * w->fh)) - w->m;
            break;
    }
    return c;
}

static int getalign(const char* p) {
    if (!p)
        return A_TC;

    return
        (eq(p, "l") || eq(p, "left") || eq(p, "middle-left")) ? A_ML :
        (eq(p, "r") || eq(p, "right") || eq(p, "middle-right")) ? A_MR :
        (eq(p, "c") || eq(p, "center") || eq(p, "middle-center")) ? A_MC :
        (eq(p, "tl") || eq(p, "top-left")) ? A_TL :
        (eq(p, "tr") || eq(p, "top-right")) ? A_TR :
        (eq(p, "t") || eq(p, "top-center")) ? A_TC :
        (eq(p, "bl") || eq(p, "bottom-left")) ? A_BL :
        (eq(p, "br") || eq(p, "bottom-right")) ? A_BR :
        (eq(p, "b") || eq(p, "bottom-center")) ? A_BC :
        A_TC;
}

static void movealign(widget_t* w, widget_t* r, int a) {
    int x = 0;
    int y = 0;

    switch (a) {
        case A_ML:
            x = r->x - w->w - (w->b*2);
            y = (w->h + (w->b*2)) > (r->h + (r->b*2)) ? (r->y - ((w->h + (w->b*2) - r->h) / 2)) : (r->y + ((r->h - w->h) / 2));
            break;
        case A_MR:
            x = r->x + r->w + (r->b*2);
            y = (w->h + (w->b*2)) > (r->h + (r->b*2)) ? (r->y - ((w->h + (w->b*2) - r->h) / 2)) : (r->y + ((r->h - w->h) / 2));
            break;
        case A_MC:
            x = (w->w + (w->b*2)) > (r->w + (r->b*2)) ? (r->x - ((w->w + (w->b*2) - r->w) / 2)) : (r->x + ((r->w - w->w) / 2));
            y = (w->h + (w->b*2)) > (r->h + (r->b*2)) ? (r->y - ((w->h + (w->b*2) - r->h) / 2)) : (r->y + ((r->h - w->h) / 2));
            break;
        case A_TL:
            x = r->x - (w->b*2) - w->w;
            y = r->y - w->h - (w->b*2);
            break;
        case A_TR:
            x = r->x + r->w + (r->b*2);
            y = r->y - w->h - (w->b*2);
            break;
        case A_TC:
            x = (w->w + (w->b*2)) > (r->w + (r->b*2)) ? (r->x - ((w->w + (w->b*2) - r->w) / 2)) : (r->x + ((r->w - w->w) / 2));
            y = r->y - w->h - (w->b*2);
            break;
        case A_BL:
            x = r->x - (w->b*2) - w->w;
            y = r->y + r->h + (r->b*2);
            break;
        case A_BR:
            x = r->x + r->w + (r->b*2);
            y = r->y + r->h + (r->b*2);
            break;
        case A_BC:
            x = (w->w + (w->b*2)) > (r->w + (r->b*2)) ? (r->x - ((w->w + (w->b*2) - r->w) / 2)) : (r->x + ((r->w - w->w) / 2));
            y = r->y + r->h + (r->b*2);
            break;
        default:
            break;
    }
    XMoveWindow(display, w->wid, x, y);
    moved(w);
}

void closed(widget_t* w) {
    hide(w);
    w->flags |= F_CLOSED;
    trigger(w->wid, SIG_CLOSED);
    if (w->etype == E_PAGE) {
        widget_t* s;
        each(((page_t*)w)->subs, s, closed(s););
    }
}

void moved(widget_t* w) {
    trigger(w->wid, SIG_MOVED);

    if (w->related->count) {
        widint_t* p;
        widget_t* x;

        eachitem(w->related, widint_t, p,
            if (widgets->get(widgets, p->key, &x))
                movealign(x, w, p->val);
            else
                w->related->remove(w->related, p->key);
        );
    }
}

void resized(widget_t* w) {
    trigger(w->wid, SIG_RESIZED);

    if (w->relal->key) {
        widget_t* r;
        if (widgets->get(widgets, w->relal->key, &r))
            movealign(w, r, w->relal->val);
        else
            w->relal->key = 0;
    }
    if (w->related->count) {
        widint_t* p;
        widget_t* x;

        eachitem(w->related, widint_t, p,
            if (widgets->get(widgets, p->key, &x))
                movealign(x, w, p->val);
            else
                w->related->remove(w->related, p->key);
        );
    }
}

void relaxed(widget_t* w, int c) {
    if (c && !(w->flags & F_WFIXED) && !(w->flags & F_HFIXED))
        return;

    w->flags &= ~(F_WFIXED|F_HFIXED);

    XSizeHints* h = XAllocSizeHints();
    h->flags = USPosition;
    XSetWMSizeHints(display, w->wid, h, XA_WM_NORMAL_HINTS);
    XFree(h);
}

void fixed(widget_t* w, int c) {
    if (c && w->flags & F_WFIXED && w->flags & F_HFIXED)
        return;

    w->flags |= (F_WFIXED | F_HFIXED);

    XSizeHints* h = XAllocSizeHints();
    h->flags = USPosition | PMinSize | PMaxSize;
    h->min_width = h->max_width = w->w;
    h->min_height = h->max_height = w->h;
    XSetWMSizeHints(display, w->wid, h, XA_WM_NORMAL_HINTS);
    XFree(h);
}

void wfixed(widget_t* w, int c) {
    if (c && w->flags & F_WFIXED && !(w->flags & F_HFIXED))
        return;

    w->flags |= F_WFIXED;
    w->flags &= ~F_HFIXED;

    XSizeHints* h = XAllocSizeHints();
    h->flags = USPosition | PMinSize | PMaxSize;
    h->min_width = h->max_width = w->w;
    XSetWMSizeHints(display, w->wid, h, XA_WM_NORMAL_HINTS);
    XFree(h);
}

void hfixed(widget_t* w, int c) {
    if (c && w->flags & F_HFIXED && !(w->flags & F_WFIXED))
        return;

    w->flags |= F_HFIXED;
    w->flags &= ~F_WFIXED;

    XSizeHints* h = XAllocSizeHints();
    h->flags = USPosition | PMinSize | PMaxSize;
    h->min_height = h->max_height = w->h;
    XSetWMSizeHints(display, w->wid, h, XA_WM_NORMAL_HINTS);
    XFree(h);
}

void fullscreen(widget_t* w) {
    if (!w)
        return;

    XEvent e;
    e.xclient.type = ClientMessage;
    e.xclient.serial = 0;
    e.xclient.send_event = True;
    e.xclient.display = display;
    e.xclient.window  = w->wid;
    e.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    e.xclient.format = 32;

    e.xclient.data.l[0] = 1;
    e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);
    e.xclient.data.l[2] = 0;
    e.xclient.data.l[3] = 1;
    e.xclient.data.l[4] = 0;

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, &e);
    XSync(display, False);

    if (w->flags & F_GHOST)
        ghost(w);
    w->draw(w);
}

void show(widget_t* w) {
    if (!w)
        return;

    if (w->etype == E_PAGE)
        showsubs(w);
    
    XMapWindow(display, w->wid);
    w->flags |= F_VISIBLE;
    if (w->flags & F_CLOSED)
        w->flags &= ~F_CLOSED;
    if (w->flags & F_GHOST)
        ghost(w);

    if (w->parent || w->flags & F_BYPASS || w->flags & F_TRAYED)
        return;
    // wm workaround
    XResizeWindow(display, w->wid, w->w, w->h);
    
    Atom wmstate = XInternAtom(display, "WM_STATE", False);
    if (wmstate == None)
        return;

    Atom type;
    int format;
    unsigned long n = 0;
    unsigned long bafter;
    unsigned char *props = NULL;

    while (XGetWindowProperty(display, w->wid, wmstate, 0, ~(0l), False, AnyPropertyType, &type, &format, &n, &bafter, &props) == Success) {
        if (props && *props != WithdrawnState)
            break;
        usleep(1000);
        XFree(props);
    }
    XFree(props);
}

void hide(widget_t* w) {
    if (!w || !(w->flags & (F_VISIBLE|F_HOVERED)))
        return;

    XUnmapWindow(display, w->wid);
    w->flags &= ~(F_VISIBLE|F_HOVERED);
    if (w->parent || w->flags & F_BYPASS || w->flags & F_TRAYED)
        return;

    Atom wmstate = XInternAtom(display, "WM_STATE", False);
    if (wmstate == None)
        return;

    Atom type;
    int format;
    unsigned long n;
    unsigned long bafter;
    unsigned char *props;
    size_t count = 0;
    while (XGetWindowProperty(display, w->wid, wmstate, 0, ~(0l), False, AnyPropertyType, &type, &format, &n, &bafter, &props) == Success) {
        if ((props && *props == WithdrawnState) || count > 1000)
            break;
        usleep(1000);
        ++count;
        XFree(props);
    }
    XFree(props);

    if (w->etype == E_PAGE)
        hidesubs(w);
}

void entitle(widget_t* w, const char* t) {
    if (!w)
        return;

    if (w->title) {
        zfree(w->title);
        w->title = salloc((char*)t);
    }
    XStoreName(display, w->wid, t);
    setclass(w, t);
    XSync(display, False);
}

void settop(widget_t* w) {
    if (!w)
        return;

    XEvent e;
    e.xclient.type = ClientMessage;
    e.xclient.serial = 0;
    e.xclient.send_event = True;
    e.xclient.display = display;
    e.xclient.window  = w->wid;
    e.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    e.xclient.format = 32;

    e.xclient.data.l[0] = 1;
    e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_ABOVE", False);
    e.xclient.data.l[2] = 0;
    e.xclient.data.l[3] = 1;
    e.xclient.data.l[4] = 0;
    XSendEvent(display, root, False, SubstructureRedirectMask|SubstructureNotifyMask, &e);

    XSync(display, False);
    if (w->flags & F_VISIBLE)
        show(w);
}

void setdefault(widget_t* w) {
    if (!w)
        return;

    XEvent e;
    e.xclient.type = ClientMessage;
    e.xclient.serial = 0;
    e.xclient.send_event = True;
    e.xclient.display = display;
    e.xclient.window  = w->wid;
    e.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    e.xclient.format = 32;

    e.xclient.data.l[0] = 0;
    e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_ABOVE", False);
    e.xclient.data.l[2] = XInternAtom(display, "_NET_WM_STATE_BELOW", False);
    e.xclient.data.l[3] = 1;
    e.xclient.data.l[4] = 0;

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, &e);

    e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_MODAL", False);
    e.xclient.data.l[2] = XInternAtom(display, "_NET_WM_STATE_STICKY", False);

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, &e);

    e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", False);
    e.xclient.data.l[2] = XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", False);

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, &e);

    XSync(display, False);
    if (w->flags & F_VISIBLE)
        show(w);
}

void bypasswm(widget_t* w, int pass) {
    XSetWindowAttributes wa;
    memset(&wa, 0, sizeof(wa));
    wa.override_redirect = pass;
    XChangeWindowAttributes(display, w->wid, CWOverrideRedirect, &wa);
    if (pass)
        w->flags |= F_BYPASS;
    else
        w->flags &= ~F_BYPASS;
}

void setbottom(widget_t* w) {
    if (!w)
        return;

    XEvent e;
    e.xclient.type = ClientMessage;
    e.xclient.serial = 0;
    e.xclient.send_event = True;
    e.xclient.display = display;
    e.xclient.window  = w->wid;
    e.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    e.xclient.format = 32;

    e.xclient.data.l[0] = 1;
    e.xclient.data.l[2] = XInternAtom(display, "_NET_WM_STATE_BELOW", False);
    e.xclient.data.l[3] = 1;
    e.xclient.data.l[4] = 0;

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, &e);

    XSync(display, False);
    if (w->flags & F_VISIBLE)
        show(w);
}

void wclose(widget_t* w) {
    if (!w)
        return;

    hide(w);
    w->flags &= ~F_VISIBLE;
}

intint_t getcenter(widget_t* w) {
    intint_t r = {0};
    r.key = -1;
    r.val = -1;
    if (!w)
        return r;

    r.key = w->w / 2;
    r.val = w->h / 2;
    return r;
}

void center(widget_t* w) {
    if (!w)
        return;

    int x;
    int y;

    if (w->parent) {
        x = ((w->parent->w - (w->b*2)) / 2);
        y = ((w->parent->h - (w->b*2)) / 2);
    } else {
        x = (sw() / 2);
        y = (sh() / 2);
    }
    intint_t cpos = getcenter(w);
    move(w, x-cpos.key, y-cpos.val);
}

void wraise(widget_t* w) {
    if (!w)
        return;

    XRaiseWindow(display, w->wid);
    XSync(display, False);
}

void lower(widget_t* w) {
    if (!w)
        return;

    XLowerWindow(display, w->wid);
    XSync(display, False);
}

void maximize(widget_t* w) {
    if (!w)
        return;

    XClientMessageEvent e = {0};

    e.type = ClientMessage;
    e.window = w->wid;
    e.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    e.format = 32;
    e.data.l[0] = 1;
    e.data.l[1] = XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
    e.data.l[2] = XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_VERT", False);
    e.data.l[3] = 0;
    e.data.l[4] = 0;
    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, (XEvent*) &e);
    XSync(display, False);

    if (w->flags & F_GHOST)
        ghost(w);
}

void resize(widget_t* W, int w, int h) {
    if (!w || w <= 0 || h <= 0)
        return;

    W->w = w;
    W->h = h;
    XResizeWindow(display, W->wid, w, h);
    XSync(display, False);

    if (W->flags & F_WFIXED && W->flags & F_HFIXED)
        fixed(W, 0);
    else if (W->flags & F_WFIXED && !(W->flags & F_HFIXED))
        wfixed(W, 0);
    else if (W->flags & F_HFIXED && !(W->flags & F_WFIXED))
        hfixed(W, 0);
    else
        relaxed(W, 0);

    trigger(W->wid, SIG_RESIZED);
    resized(W);
}

static void setattr(widget_t* w, const char* k, const char* v) {
    if (!strcmp(k, "background") || !strcmp(k, "bg")) {
        if (0) {
        #ifdef ENABLE_IMAGES
        } else if (v[0] == '/') {
            if (ieq(v, "NULL") && w->img) {
                imlib_context_set_image(w->img);
                imlib_free_image();
                w->img = NULL;
                w->flags |= F_APPLYHOVER;
                w->draw(w);
            } else {
                w->flags &= ~F_APPLYHOVER;
                loadimg(w, v+1);
                w->draw(w);
            }
        }
        #endif
        else {
            w->bg = addcol(v);
            w->draw(w);
        }
    } else if (!strcmp(k, "color") || !strcmp(k, "foreground") || !strcmp(k, "fg")) {
        w->fg = addcol(v);
        w->draw(w);
    } else if (!strcmp(k, "pressed-background") || !strcmp(k, "pbg")) {
        w->pbg = addcol(v);
        w->draw(w);
    } else if (!strcmp(k, "pressed-color") || !strcmp(k, "pfg")) {
        w->pfg = addcol(v);
        w->draw(w);
    } else if (!strcmp(k, "hovered-background") || !strcmp(k, "hbg")) {
        w->mask |= EnterWindowMask|LeaveWindowMask;
        XSelectInput(display, w->wid, w->mask);
        w->flags |= F_APPLYHOVER;
        w->hbg = addcol(v);
        w->draw(w);
    } else if (!strcmp(k, "hovered-color") || !strcmp(k, "hfg")) {
        w->mask |= EnterWindowMask|LeaveWindowMask;
        XSelectInput(display, w->wid, w->mask);
        w->hfg = addcol(v);
        w->draw(w);
    } else if (!strcmp(k, "border-color") || !strcmp(k, "bc")) {
        XSetWindowBorder(display, w->wid, addcol(v)->pixel);
    } else if (!strcmp(k, "width") || !strcmp(k, "w")) {
        w->w = strtoul(v, 0, 10);
        resize(w, w->w, w->h);
    } else if (!strcmp(k, "height") || !strcmp(k, "h")) {
        w->h = strtoul(v, 0, 10);
        resize(w, w->w, w->h);
    } else if (!strcmp(k, "border") || !strcmp(k, "b")) {
        w->b = strtoul(v, 0, 10);
        XSetWindowBorderWidth(display, w->wid, w->b);
    } else if (!strcmp(k, "margin") || !strcmp(k, "g")) {
        w->m = strtoul(v, 0, 10);
        w->draw(w);
    } else if (!strcmp(k, "line") || !strcmp(k, "l")) {
        w->lw = strtoul(v, 0, 10);
        XSetLineAttributes(display, gc, w->lw, LineSolid, CapRound, JoinRound);
        w->draw(w);
    } else if (!strcmp(k, "mode") || !strcmp(k, "m")) {
        !strcmp(v, "fixed") || !strcmp(v, "f") ? fixed(w, 1) :
        !strcmp(v, "wfixed") || !strcmp(v, "w") ? wfixed(w, 1) :
        !strcmp(v, "hfixed") || !strcmp(v, "h") ? hfixed(w, 1) :
        !strcmp(v, "relaxed") || !strcmp(v, "r") ? relaxed(w, 1) :
        pass();
    } else if (!strcmp(k, "align") || !strcmp(k, "a")) {
        align al = 
            (!strcmp(v, "l") || !strcmp(v, "left") || !strcmp(v, "middle-left")) ? A_ML :
            (!strcmp(v, "r") || !strcmp(v, "right") || !strcmp(v, "middle-right")) ? A_MR :
            (!strcmp(v, "c") || !strcmp(v, "center") || !strcmp(v, "middle-center")) ? A_MC :
            (!strcmp(v, "tl") || !strcmp(v, "top-left")) ? A_TL :
            (!strcmp(v, "tr") || !strcmp(v, "top-right")) ? A_TR :
            (!strcmp(v, "t") || !strcmp(v, "top-center")) ? A_TC :
            (!strcmp(v, "bl") || !strcmp(v, "bottom-left")) ? A_BL :
            (!strcmp(v, "br") || !strcmp(v, "bottom-right")) ? A_BR :
            (!strcmp(v, "b") || !strcmp(v, "bottom-center")) ? A_BC :
            A_MC;
        if (w->etype == E_PAGE) {
            page_t* p = (page_t*)w;
            if (p->l == HL)
                w->align = al;
            else
                p->valign = al;
            setlayout(p, p->l, 1);
        } else {
            w->align = al;
        }
        w->draw(w);
    } else if (!strcmp(k, "f") || !strcmp(k, "font")) {
        if (!w->fs && !gc)
            return;
        XFreeFont(display, w->fs);
        debug("trying to load font: '%s'\n", v);
        w->fs = XLoadQueryFont(display, v);
        if (!w->fs) {
            debug("cannot load font '%s'\n", v);
            w->fs = XLoadQueryFont(display, "fixed");
        }
        if (!w->fs)
            return;
        w->fh = (w->fs->max_bounds.ascent + w->fs->max_bounds.descent);
        w->draw(w);
    } else {
        fprintf(stderr, "warning, unused attr: %s->%s\n", k, v);
    }
}

void style(widget_t* w, const char* s) {
    if (!w)
        return;
    const char* p = s;

    char v[BSIZ];
    char* k = buf;
    char* b = buf;
    int esc = 0;
    size_t i;

    for (i=0; *p && (i<BSIZ-1); ++p) {
        if (!esc) {
            switch (*p) {
            case '\\':
                if (!esc) {
                    esc = 1;
                    continue;
                }
                esc = 0;
                break;
            case ' ':
            case '\t':
                continue;
            case '\n':
            case '\r':
            case '\v':
            case '\f':
            case '|':
            case ';':
                b[i] = 0;
                if (i)
                    setattr(w, k, v);
                b = k;
                i = 0;
                continue;
            case ':':
                b[i] = 0;
                b = v;
                i = 0;
                continue;
            default:
                break;
            }
        }
        esc = 0;
        b[i++] = *p;
    }
    if (i) {
        b[i] = 0;
        setattr(w, k, v);
    }
}

XColor* addcol(const char* v) {
    XColor* c = NULL;
    if (!colors->contains(colors, v)) {
        c = calloc(1, sizeof(XColor));
        if (colors->full(colors))
            colors->rehash(colors);
        if (XParseColor(display, DefaultColormap(display, screen), v, c) &&
            XAllocColor(display, DefaultColormap(display, screen), c))
        {
            colors->insert(colors, salloc((char*)v), c);
        } else {
            zfree(c);
            colors->get(colors, "black", &c);
            return c;
        }
    }
    colors->get(colors, v, &c);
    return c;
}

void freeze(widget_t* w) {
    if (!w)
        return;
    if (w->etype == E_PAGE)
        freezesubs(w);
    w->flags |= F_FREEZED;
}

void unfreeze(widget_t* w) {
    if (!w)
        return;
    if (w->etype == E_PAGE)
        unfreezesubs(w);
    w->flags &= ~F_FREEZED;
}

void fit(widget_t* w) {
    if (!w)
        return;

    if (w->etype == E_PAGE) {
        fit_to_cont(w);
        return;
    }
    if (!w->fs || !w->data || !*w->data)
        return;

    resize(w, w->maxwcont+SPIXEL+(w->m*2), w->maxhcont+SPIXEL+(w->m*2));
}

static void nwidgets(widget_t* w, widgetwidget_t* n) {
    if (!w || !w->parent || !n)
        return;

    page_t* p = (page_t*)w->parent;

    n->key = NULL;
    n->val = NULL;

    for (size_t i=0; i<p->subs->count; ++i) {
        widget_t* g;
        p->subs->get(p->subs, i, &g);
        if (w == g) {
            if (i)
                p->subs->get(p->subs, i-1, &n->key);
            else
                n->key = NULL;
            if (i+1 < p->subs->count)
                p->subs->get(p->subs, i+1, &n->val);
            else
                n->val = NULL;
            break;
        }
    }
}

void fill_right(widget_t* w, int o) {
    if (!w || !w->parent)
        return;

    page_t* p = (page_t*)w->parent;
    widgetwidget_t n;
    widget_t* next;
    nwidgets(w, &n);
    next = n.val;
    if (!next || p->l == VL || o)
        resize(w, w->parent->w - w->x - (w->b*2) - w->parent->m, w->h);
    else
        resize(w, next->x - w->x - (w->b*2), w->h);
}

void fill_bottom(widget_t* w, int o) {
    if (!w || !w->parent)
        return;

    page_t* p = (page_t*)w->parent;
    widgetwidget_t n;
    widget_t* next;
    nwidgets(w, &n);
    next = n.val;

    if (!next || p->l == HL || o)
        resize(w, w->w, w->parent->h - w->y - (w->b*2) - w->parent->m);
    else
        resize(w, w->w, next->y - w->y - (w->b*2));
}

void fill(widget_t* w, int o) {
    if (!w || !w->parent)
        return;
    fill_right(w, o);
    fill_bottom(w, o);
}

void tray(widget_t* w) {
    if (!w || w->parent)
        return;

    w->flags |= F_TRAYED;

    XEvent ev;
    Atom sel = XInternAtom (display, "_NET_SYSTEM_TRAY_S0", False);

    if (sel == None)
        return;
    Window tray = XGetSelectionOwner(display, sel);
    if (tray == None)
        return;

    XSelectInput (display,tray,StructureNotifyMask);
    memset(&ev, 0, sizeof(ev));

    ev.xclient.type = ClientMessage;
    ev.xclient.window = tray;
    ev.xclient.message_type = XInternAtom(display, "_NET_SYSTEM_TRAY_OPCODE", False);
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = CurrentTime;
    ev.xclient.data.l[1] = 0;
    ev.xclient.data.l[2] = w->wid;
    ev.xclient.data.l[3] = 0;
    ev.xclient.data.l[4] = 0;

    XSendEvent(display, tray, False, NoEventMask, &ev);
    XSync(display, False);

    while (parentof(w->wid) == root)
        usleep(1000);
    if (!(w->flags & F_VISIBLE))
        XUnmapWindow(display, w->wid);

    intint_t tc = coords(w->wid);

    w->x = tc.key;
    w->y = tc.val;
}

void ghost(widget_t* w) {
    if (!w)
        return;
    w->flags |= F_GHOST;

    XClientMessageEvent e = {0};

    e.type = ClientMessage;
    e.window = w->wid;
    e.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    e.format = 32;
    e.data.l[0] = 1;
    e.data.l[1] = XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", False);
    e.data.l[2] = 0;
    e.data.l[3] = 0;
    e.data.l[4] = 0;

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureRedirectMask|SubstructureNotifyMask, (XEvent *) &e);
    XSync(display, False);
}

void movealigns(widget_t* w, widget_t* r, const char* p) {
    if (!w || !r || !p)
        return;

    movealign(w, r, getalign(p));
}

void move(widget_t* w, int x, int y) {
    if (!w)
        return;

    w->x = x;
    w->y = y;
    XMoveWindow(display, w->wid, x, y);
    moved(w);
}

void focus(widget_t* w) {
    if (!w || !(w->flags & F_VISIBLE))
        return;

    XSetInputFocus(display, w->wid, RevertToNone, CurrentTime);
}

void settext(widget_t* w, const char* t) {
    if (!t || !*t)
        return;
    if (w->data)
        free(w->data);
    w->data = salloc((char*)t);
    w->draw(w);
}

void addtext(widget_t* w, const char* t) {
    if (!t || !*t)
        return;
    w->data = w->data ? sadd(w->data, t) : salloc((char*)t);
    w->draw(w);
}

void pressed(widget_t* w, int b) {
    w->flags |= 
        (b == MOUSE_LEFT) ? F_LPRESSED :
        (b == MOUSE_RIGHT) ? F_RPRESSED :
        (b == MOUSE_MIDDLE) ? F_MPRESSED :
        F_LPRESSED;

    w->draw(w);
    trigger(w->wid,
        (b == MOUSE_LEFT) ? SIG_LPRESSED :
        (b == MOUSE_RIGHT) ? SIG_RPRESSED :
        (b == MOUSE_MIDDLE) ? SIG_MPRESSED :
        SIG_LPRESSED);

    trigger(w->wid, SIG_PRESSED);
    struct timeval now;
    time_t msec;

    if (!w->tpress.tv_usec) {
        gettimeofday(&w->tpress, NULL);
        return;
    }
    gettimeofday(&now, NULL);

    msec = ((now.tv_sec * 1000) + (now.tv_usec / 1000)) - 
        ((w->tpress.tv_sec * 1000) + (w->tpress.tv_usec / 1000));

    if (msec >= 0 && msec <= 300) {
        w->flags |=
            (b == MOUSE_LEFT) ? F_LDCLICKED :
            (b == MOUSE_RIGHT) ? F_RDCLICKED :
            (b == MOUSE_MIDDLE) ? F_MDCLICKED :
            F_LDCLICKED;
    }
    gettimeofday(&w->tpress, NULL);
}

void released(widget_t* w, int b) {
    w->flags &=
        (b == MOUSE_LEFT) ? ~F_LPRESSED :
        (b == MOUSE_RIGHT) ? ~F_RPRESSED :
        (b == MOUSE_MIDDLE) ? ~F_MPRESSED :
        ~F_LPRESSED;

    w->draw(w);
    trigger(w->wid,
        (b == MOUSE_LEFT) ? SIG_LRELEASED :
        (b == MOUSE_RIGHT) ? SIG_RRELEASED :
        (b == MOUSE_MIDDLE) ? SIG_MRELEASED :
        SIG_LRELEASED);

    trigger(w->wid, SIG_RELEASED);
    if (w->flags & 
        (b == MOUSE_LEFT ? F_LDCLICKED :
         b == MOUSE_RIGHT ? F_RDCLICKED :
         b == MOUSE_MIDDLE ? F_MDCLICKED :
         F_LDCLICKED))
    {
        trigger(w->wid, 
            (b == MOUSE_LEFT) ? SIG_LDCLICKED :
            (b == MOUSE_RIGHT) ? SIG_RDCLICKED :
            (b == MOUSE_MIDDLE) ? SIG_MDCLICKED :
            SIG_LDCLICKED);
        trigger(w->wid, SIG_DCLICKED);
        w->flags &=
            (b == MOUSE_LEFT) ? ~F_LDCLICKED :
            (b == MOUSE_RIGHT) ? ~F_RDCLICKED :
            (b == MOUSE_MIDDLE) ? ~F_MDCLICKED :
            ~F_LDCLICKED;
    } else {
        trigger(w->wid, 
            (b == MOUSE_LEFT) ? SIG_LCLICKED :
            (b == MOUSE_RIGHT) ? SIG_RCLICKED :
            (b == MOUSE_MIDDLE) ? SIG_MCLICKED :
            SIG_LCLICKED);
        trigger(w->wid, SIG_CLICKED);
    }
}

void scroll(widget_t* w, int d) {
    if (d)
        trigger(w->wid, SIG_SCROLLUP);
    else
        trigger(w->wid, SIG_SCROLLDOWN);
}

void write_align(widget_t* w, const char* s, align a, intint_t* r) {
    if (!s || !gc || !w->fs)
        return;
    size_t l = 0;
    char* ls;
    intint_t c = {0};

    each(w->ll, ls, zfree(ls););
    w->ll->clear(w->ll);

    ssplit(s, '\n', w->ll, "");

    for (size_t i=0; i<w->ll->count; ++i) {
        if (w->ll->get(w->ll, i, &ls)) {
            if (*ls) {
                char* n;
                if (w->ll->get(w->ll, i+1, &n) && !*n) {
                    zfree(n);
                    w->ll->remove(w->ll, i+1);
                }
            }
        }
    }
    w->maxhcont = w->ll->count * w->fh;
    for (size_t i=0; i<w->ll->count; ++i) {
        w->ll->get(w->ll, i, &ls);
        l = strlen(ls);
        c = textpos(w, ls, a, i, w->ll->count);
        XDrawString(display, w->wid, gc, c.key, c.val, ls, l);
    }
    if (r) {
        r->key = c.key;
        r->val = c.val;
    }
}

void addpoints(widget_t* w, char* col, vec_int_t* ps) {
    if (!w)
        return;
    if (!widloci)
        widloci = alloc(map_widloci_t);

    loci_t* s = NULL;
    if (!widloci->get(widloci, w->wid, &s)) {
        s = alloc(loci_t);
        widloci->insert(widloci, w->wid, s);
    }
    points_t* m = alloc(points_t);
    m->count = ps->count / 2;
    m->ps = malloc(sizeof(XPoint)*(ps->count/2));
    if (!m->ps) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    s->loci->push(s->loci, m);

    int x;
    int y;
    size_t i = 0;
    while (ps->count) {
        ps->pop_back(ps, &x);
        ps->pop_back(ps, &y);
        m->ps[i].x = x;
        m->ps[i].y = y;
        s->cols->push(s->cols, salloc(col));
        ++i;
    }
    w->draw(w);
}

void addpixels(widget_t* w, char* col, vec_int_t* ps) {
    if (!w)
        return;
    if (!widpixels)
        widpixels = alloc(map_widloci_t);

    loci_t* s = NULL;
    if (!widpixels->get(widpixels, w->wid, &s)) {
        s = alloc(loci_t);
        widpixels->insert(widpixels, w->wid, s);
    }
    points_t* m = alloc(points_t);
    m->count = ps->count / 2;
    m->ps = malloc(sizeof(XPoint)*(ps->count/2));
    if (!m->ps) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    s->loci->push(s->loci, m);

    int x;
    int y;
    size_t i = 0;
    while (ps->count) {
        ps->pop_back(ps, &x);
        ps->pop_back(ps, &y);
        m->ps[i].x = x;
        m->ps[i].y = y;
        s->cols->push(s->cols, salloc(col));
        ++i;
    }
    w->draw(w);
}

void addlines(widget_t* w, char* col, vec_int_t* ps) {
    if (!w)
        return;
    if (!widlines)
        widlines = alloc(map_widloci_t);

    loci_t* s = NULL;
    if (!widlines->get(widlines, w->wid, &s)) {
        s = alloc(loci_t);
        widlines->insert(widlines, w->wid, s);
    }
    XPoint* m = NULL;
    m = malloc(sizeof(XPoint)*(ps->count/2));
    if (!m) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    points_t* p = alloc(points_t);
    p->count = ps->count / 2;
    p->ps = m;

    int x;
    int y;
    for (size_t i=0; ps->count; ++i) {
        ps->pop_back(ps, &x);
        ps->pop_back(ps, &y);
        m[i].x = x;
        m[i].y = y;
    }
    s->cols->push(s->cols, salloc(col));
    s->loci->push(s->loci, p);
    w->draw(w);
}

void addarcs(widget_t* w, int area, char* col, vec_int_t* xas) {
    if (!w)
        return;
    map_widarcssets_t** as = area ? &widarcsareas : &widarcssets;

    if (!*as)
        *as = alloc(map_widarcssets_t);

    arcssets_t* s = NULL;
    if (!(*as)->get(*as, w->wid, &s)) {
        s = alloc(arcssets_t);
        (*as)->insert(*as, w->wid, s);
    }
    XArc* m = NULL;
    m = malloc(sizeof(XArc)*(xas->count/6));
    if (!m) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    arcs_t* a = alloc(arcs_t);
    a->count = xas->count / 6;
    a->as = m;
    
    for (size_t i=0; xas->count; ++i) {
        int x;
        int y;
        int ww;
        int hh;
        int alpha;
        int beta;

        xas->pop_back(xas, &x);
        xas->pop_back(xas, &y);
        xas->pop_back(xas, &ww);
        xas->pop_back(xas, &hh);
        xas->pop_back(xas, &alpha);
        xas->pop_back(xas, &beta);

        m[i].x = x;
        m[i].y = y;
        m[i].width = ww;
        m[i].height = hh;
        m[i].angle1 = alpha*64;
        m[i].angle2 = beta*64;
    }
    s->arcssets->push(s->arcssets, a);
    s->cols->push(s->cols, salloc(col));
    w->draw(w);
}

void addarea(widget_t* w, char* col, vec_int_t* ps) {
    if (!w)
        return;
    if (!widareas)
        widareas = alloc(map_widloci_t);
    loci_t* s = NULL;
    if (!widareas->get(widareas, w->wid, &s)) {
        s = alloc(loci_t);
        widareas->insert(widareas, w->wid, s);
    }

    points_t* m = alloc(points_t);
    m->ps = malloc(sizeof(XPoint)*(ps->count/2));
    m->count = ps->count / 2;
    if (!m->ps) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    int x;
    int y;
    for (size_t i=0; ps->count; ++i) {
        ps->pop_back(ps, &x);
        ps->pop_back(ps, &y);
        m->ps[i].x = x;
        m->ps[i].y = y;
    }
    s->loci->push(s->loci, m);
    s->cols->push(s->cols, salloc(col));
    w->draw(w);
}

void addnamedpoint(widget_t* w, char* col, char* n, int x, int y) {
    if (!w)
        return;
    if (!widnamedpoints)
        widnamedpoints = alloc(map_widnamedpoints_t);
    namedpoints_t* s = NULL;
    if (!widnamedpoints->get(widnamedpoints, w->wid, &s)) {
        s = alloc(namedpoints_t);
        widnamedpoints->insert(widnamedpoints, w->wid, s);
    }
    s->ps->push(s->ps, x);
    s->ps->push(s->ps, y);
    s->names->push(s->names, salloc(n));
    s->cols->push(s->cols, salloc(col));
    w->draw(w);
}

static XPoint* points_to_normal_coords(widget_t* w, points_t* ps) {
    XPoint* cps = malloc(sizeof(XPoint)*ps->count);
    memcpy(cps, ps->ps, sizeof(XPoint)*ps->count);
    if (!cps) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    for (size_t j=0; j<ps->count; ++j)
        cps[j].y = w->h-cps[j].y;
    return cps;
}

static XArc* arcs_to_normal_coords(widget_t* w, arcs_t* as) {
    XArc* cas = malloc(sizeof(XArc)*as->count);
    memcpy(cas, as->as, sizeof(XArc)*as->count);
    if (!cas) {
        fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    for (size_t i=0; i<as->count; ++i) {
        cas[i].x = cas[i].x - (cas[i].width / 2);
        cas[i].y = w->h - cas[i].y - (cas[i].height / 2);
    }
    return cas;
}

void drawnamedpoints(widget_t* w) {
    namedpoints_t* s;
    if (widnamedpoints->get(widnamedpoints, w->wid, &s)) {
        XColor* c;
        char* col;
        char* name;
        int x;
        int y;
        for (size_t i=0; i<s->cols->count; ++i) {
            s->cols->get(s->cols, i, &col);
            s->names->get(s->names, i, &name);
            s->ps->get(s->ps, i*2, &x);
            s->ps->get(s->ps, i*2+1, &y);
            c = addcol(col);
            XSetForeground(display, gc, c->pixel);
            XDrawArc(display, w->wid, gc, x, w->h-y, w->lw, w->lw, 0, 360*64);
            XDrawString(display, w->wid, gc, x, w->h-y, name, strlen(name));
        }
    }
}

void drawpixels(widget_t* w) {
    loci_t* s;
    if (widpixels->get(widpixels, w->wid, &s)) {
        XColor* c;
        char* col;
        points_t* ps;
        for (size_t i=0; i<s->cols->count; ++i) {
            s->cols->get(s->cols, i, &col);
            s->loci->get(s->loci, i, &ps);
            c = addcol(col);
            XSetForeground(display, gc, c->pixel);
            XPoint* cps = points_to_normal_coords(w, ps);
            XDrawPoints(display, w->wid, gc, cps, ps->count, CoordModeOrigin);
            free(cps);
        }
    }
}

void drawpoints(widget_t* w) {
    loci_t* s;
    if (widloci->get(widloci, w->wid, &s)) {
        XColor* c;
        char* col;
        points_t* ps;
        for (size_t i=0; i<s->cols->count; ++i) {
            s->cols->get(s->cols, i, &col);
            s->loci->get(s->loci, i, &ps);
            c = addcol(col);
            XSetForeground(display, gc, c->pixel);
            XArc* cas = malloc(sizeof(XArc)*ps->count);
            if (!cas) {
                fprintf(stderr, "FATAL ERROR: cannot allocate memory.\n");
                exit(EXIT_FAILURE);
            }
            for (size_t j=0; j<ps->count; ++j) {
                cas[j].x = ps->ps[j].x;
                cas[j].y = w->h - ps->ps[j].y;
                cas[j].width = w->lw;
                cas[j].height = w->lw;
                cas[j].angle1 = 0;
                cas[j].angle2 = 360*64;
            }
            XDrawArcs(display, w->wid, gc, cas, ps->count);
            free(cas);
        }
    }
}

void drawlines(widget_t* w) {
    loci_t* s;
    if (widlines->get(widlines, w->wid, &s)) {
        XColor* c;
        char* col;
        points_t* ps;
        for (size_t i=0; i<s->cols->count; ++i) {
            s->cols->get(s->cols, i, &col);
            s->loci->get(s->loci, i, &ps);
            c = addcol(col);
            XSetForeground(display, gc, c->pixel);
            XPoint* cps = points_to_normal_coords(w, ps);
            XDrawLines(display, w->wid, gc, cps, ps->count, CoordModeOrigin);
            free(cps);
        }
    }
}

static void drawarcs_fill(widget_t* w, int fill) {
    arcssets_t* s;
    int (*oparc)(Display*, Drawable, GC, XArc*, int); 
    map_widarcssets_t** set;

    if (fill) {
        oparc = XFillArcs;
        set = &widarcsareas;
    } else {
        oparc = XDrawArcs;
        set = &widarcssets;
    }
    if ((*set)->get(*set, w->wid, &s)) {
        XColor* c;
        char* col;
        for (size_t i=0; i<s->cols->count; ++i) {
            s->cols->get(s->cols, i, &col);
            c = addcol(col);
            XSetForeground(display, gc, c->pixel);
            for (size_t j=0; j<s->arcssets->count; ++j) {
                arcs_t* arcs;
                s->arcssets->get(s->arcssets, j, &arcs);
                XArc* carcs = arcs_to_normal_coords(w, arcs);
                oparc(display, w->wid, gc, carcs, arcs->count);
                free(carcs);
            }
        }
    }
}

void drawarcs(widget_t* w) {
    drawarcs_fill(w, 0);
}

void drawareas(widget_t* w) {
    loci_t* s = NULL;
    if (!widareas->get(widareas, w->wid, &s))
        return;

    for (size_t i=0; i<s->cols->count; ++i) {
        char* col;
        points_t* ps;
        s->cols->get(s->cols, i, &col);
        s->loci->get(s->loci, i, &ps);

        XColor* c = addcol(col);
        XSetForeground(display, gc, c->pixel);
        
        XPoint* cps = points_to_normal_coords(w, ps);
        XFillPolygon(display, w->wid, gc, cps, ps->count, Complex, CoordModeOrigin);
        free(cps);
    }
}

void drawarcsareas(widget_t* w) {
    drawarcs_fill(w, 1);
}

void clear(widget_t* w) {
    if (w->data)
        free(w->data);

    w->data = salloc("");
    w->draw(w);
    w->maxwcont = 0;
    w->maxhcont = 0;
}

void tgrip(widget_t* w) {
    if (w->flags & F_GRIP) {
        w->flags &= ~F_GRIP;
        if (!(w->flags & (F_XMOV|F_YMOV))) {
            w->mask &= ~ButtonMotionMask;
            XSelectInput(display, w->wid, w->mask);
        }
    } else {
        w->flags |= F_GRIP;
        w->flags &= ~(F_XMOV|F_YMOV);
        w->mask |= ButtonPressMask|ButtonReleaseMask|ButtonMotionMask;
        XSelectInput(display, w->wid, w->mask);
    }
}

void txmov(widget_t* w) {
    if (w->flags & F_XMOV) {
        w->flags &= ~F_XMOV;
        if (!(w->flags & F_YMOV)) {
            w->mask &= ~ButtonMotionMask;
            XSelectInput(display, w->wid, w->mask);
        }
    } else {
        w->flags &= ~F_GRIP;
        w->flags |= F_XMOV;
        w->mask |= ButtonPressMask|ButtonReleaseMask|ButtonMotionMask;
        XSelectInput(display, w->wid, w->mask);
    }
}

void tymov(widget_t* w) {
    if (w->flags & F_YMOV) {
        w->flags &= ~F_YMOV;
        if (!(w->flags & F_XMOV)) {
            w->mask &= ~ButtonMotionMask;
            XSelectInput(display, w->wid, w->mask);
        }
    } else {
        w->flags &= ~F_GRIP;
        w->flags |= F_YMOV;
        w->mask |= ButtonPressMask|ButtonReleaseMask|ButtonMotionMask;
        XSelectInput(display, w->wid, w->mask);
    }
}

void penter(widget_t* w) {
    trigger(w->wid, SIG_ENTER);
}

void pleave(widget_t* w) {
    trigger(w->wid, SIG_LEAVE);
}

void focused(widget_t* w) {
    trigger(w->wid, SIG_FOCUS);
}

void unfocused(widget_t* w) {
    trigger(w->wid, SIG_UNFOCUS);
}

void event(widget_t* w, XEvent* e) {
    if (!w || w->flags & F_GARBAGE)
        return;
    if (e->type == Expose) {
        if (w->flags & F_TRAYED) {
            intint_t tc = coords(w->wid);
            if (w->x != tc.key || w->y != tc.val) {
                w->x = tc.key;
                w->y = tc.val;
                moved(w);
            }
        }
        w->draw(w);
        return;
    } else if (e->type == ClientMessage && (Atom)e->xclient.data.l[0] == delatom) {
        closed(w);
        return;
    } else if (!w->parent && e->type == ConfigureNotify) {
        if (w->w != e->xconfigure.width || w->h != e->xconfigure.height) {
            w->w = e->xconfigure.width;
            w->h = e->xconfigure.height;
            resized(w);
            w->draw(w);
        } else if (w->x != e->xconfigure.x || w->y != e->xconfigure.y) {
            w->x = e->xconfigure.x;
            w->y = e->xconfigure.y;
            moved(w);
        }
        return;
    }
    if (w->flags & F_FREEZED)
        return;
    if (e->type == ButtonPress) {
        switch (e->xbutton.button) {
            case MOUSE_LEFT:
            case MOUSE_RIGHT:
            case MOUSE_MIDDLE:
                if (w->flags & (F_GRIP|F_XMOV|F_YMOV)) {
                    lrpos.x = e->xmotion.x_root;
                    lrpos.y = e->xmotion.y_root;
                    fp.x = e->xmotion.x;
                    fp.y = e->xmotion.y;
                }
                pressed(w, e->xbutton.button);
                break;
            case MOUSE_SCROLLUP:
                scroll(w, 1);
                break;
            case MOUSE_SCROLLDOWN:
                scroll(w, 0);
                break;
        }
    } else if (e->type == ButtonRelease) {
        switch (e->xbutton.button) {
            case MOUSE_LEFT:
            case MOUSE_RIGHT:
            case MOUSE_MIDDLE:
                released(w, e->xbutton.button);
                break;
            default:
                return;
        }
        if (w->etype == E_CHECKBOX)
            w->flags & F_CHECKED ? uncheck(w) : check(w);
    } else if (e->type == KeyPress && w->etype == E_INPUT) {
        buildtext(w, e);
        w->draw(w);
    } else if (e->type == KeyRelease && w->etype == E_INPUT) {
        buildtext(w, e);
        w->draw(w);
    } else if (e->type == MotionNotify && w->parent && w->flags & F_GRIP) {
        XMotionEvent* m = (XMotionEvent*)e;
        point_t np;
        widget_t* r = NULL;
        widget_t* p = NULL;
        int rx;
        int ry;
        int tx;
        int ty;

        if (!w->parent->parent && !(w->parent->flags & F_BYPASS)) {
            w->flags &= ~F_GRIP;
            return;
        }
        Window cr;

        p = w->parent;
        if (p->parent)
            r = p->parent;

        XTranslateCoordinates(display, root, r ? r->wid : root, m->x_root, m->y_root, &rx, &ry, &cr);
        XTranslateCoordinates(display, w->wid, p->wid, fp.x, fp.y, &tx, &ty, &cr);
        np.x = rx - tx;
        np.y = ry - ty;

        if (r) {
            move(p,
                (np.x + p->w + (p->b * 2)) > (r->w - r->m) ? r->w - r->m - p->w - (p->b * 2) :
                np.x > r->m ? np.x : 
                r->m,
                (np.y + p->h + (p->b * 2)) > (r->h - r->m) ? r->h - r->m - p->h - (p->b * 2) :
                np.y > r->m ? np.y : 
                r->m);
        } else {
            move(p, np.x, np.y);
        }
        lrpos.x = m->x_root;
        lrpos.y = m->y_root;
    } else if (e->type == MotionNotify && w->flags & (F_XMOV|F_YMOV)) {
        XMotionEvent* m = (XMotionEvent*)e;
        widget_t* p = NULL;
        Window cr;
        point_t np;
        int rx;
        int ry;

        if (!w->parent && !(w->flags & F_BYPASS)) {
            w->flags &= ~(F_XMOV|F_YMOV);
            return;
        }
        if (w->parent)
            p = w->parent;

        XTranslateCoordinates(display, root, p ? p->wid : root, m->x_root, m->y_root, &rx, &ry, &cr);
        np.x = rx - fp.x;
        np.y = ry - fp.y;

        if (p) {
            move(w,
                w->flags & F_XMOV ?
                    (np.x + w->w + (w->b * 2)) > (p->w - p->m) ? p->w - p->m - w->w - (w->b * 2) :
                    np.x > p->m ? np.x : 
                    p->m :
                    w->x
                ,
                w->flags & F_YMOV ?
                    (np.y + w->h + (w->b * 2)) >= (p->h - p->m) ? p->h - p->m - w->h - (w->b * 2):
                    np.y > p->m ? np.y : 
                    p->m :
                    w->y
                );
        } else {
            move(w, w->flags & F_XMOV ? np.x : w->x, w->flags & F_YMOV ? np.y : w->y);
        }
        lrpos.x = m->x_root;
        lrpos.y = m->y_root;
    } else if (e->type == EnterNotify) {
        penter(w);
        w->flags |= F_HOVERED;
        if (w->parent)
            focus(w);
        w->draw(w);
    } else if (e->type == LeaveNotify && e->xcrossing.detail != NotifyInferior) {
        pleave(w);
        w->flags &= ~F_HOVERED;
        w->draw(w);
    } else if (e->type == FocusIn) {
        if (w->etype == E_INPUT) {
            int ret = XGrabKeyboard(display, w->wid, False, GrabModeAsync, GrabModeAsync, CurrentTime);
            if (ret) {
                fprintf(stderr, "Error: unable to grab keyboard on window %lu, reason: %d\n", w->wid, ret);
                exit(EXIT_FAILURE);
            } else {
                XSync(display, True);
            }
        }
        focused(w);
        w->flags |= F_FOCUSED;
        w->draw(w);
    } else if (e->type == FocusOut) {
        if (w->etype == E_INPUT)
            XUngrabKeyboard(display, CurrentTime);
        unfocused(w);
        w->flags &= ~F_FOCUSED;
        w->draw(w);
    }
}

void relate(widget_t* w, widget_t* r, const char* p) {
    if (!w || !r || !p)
        return;

    if (eq(p, "0")) {
        r->related->remove(r->related, w->wid);
        w->relal->key = 0;
        return;
    }
    if (r->related->contains(r->related, w->wid))
        return;

    align a = getalign(p);
    w->relal->key = r->wid;
    w->relal->val = a;
    r->related->insert(r->related, w->wid, a);
    movealign(w, r, a);
}

void widget_t_free(widget_t* w) {
    #ifdef ENABLE_IMAGES
    if (w->img) {
        imlib_context_set_image(w->img);
        imlib_free_image();
    }
    #endif
    if (w->parent)
        child_going(w->parent, w);
    if (!(w->flags & F_EXT))
        XDestroyWindow(display, w->wid);
    if (w->fs) {
        XFreeFont(display, w->fs);
        w->fs = NULL;
    }
    release(w->relal);
    release(w->related);
    #ifdef ENABLE_CONTROL
    if (w->to_release)
        release(w->to_release);
    #endif

    if (w->data)
        zfree(w->data);
    if (w->title)
        zfree(w->title);

    if (w->ll) {
        char* s;
        each(w->ll, s, free(s););
        release(w->ll);
    }

    if (w->flags & F_EXT) {
        if (options & KILL_EXTS_ON_QUIT && (w->pid != getpid()))
            kill(w->pid, SIGTERM);
        free(w);
    }
}

widget_t* widget_t_init(widget_t* w) {
    if (!w || !display)
        return NULL;

    memset(w, 0, sizeof(widget_t));
    w->data = salloc("");
    w->title = salloc("");
    w->align = A_MC;
    w->m = 0;
    w->lw = 1;
    w->relal = alloc(widint_t);
    w->related = alloc(map_widint_t);
    w->ll = alloc(vec_str_t);
    #ifdef ENABLE_CONTROL
    w->to_release = alloc(vec_uint_t);
    #endif

    w->pid = getpid();
    w->fs = XLoadQueryFont(display, "fixed");
    if (w->fs)
        w->fh = (w->fs->max_bounds.ascent + w->fs->max_bounds.descent);

    w->free = widget_t_free;
    w->draw = NULL;
    return w;
}

void points_t_free(points_t* s) {
    free(s->ps);
    free(s);
}

points_t* points_t_init(points_t* s) {
    if (!s || !display)
        return NULL;

    memset(s, 0, sizeof(points_t));
    s->free = points_t_free;

    return s;
}

void arcs_t_free(arcs_t* s) {
    free(s->as);
    free(s);
}

arcs_t* arcs_t_init(arcs_t* s) {
    if (!s || !display)
        return NULL;

    memset(s, 0, sizeof(arcs_t));
    s->free = arcs_t_free;

    return s;
}

void namedpoints_t_free(namedpoints_t* s) {
    char* c;
    each(s->names, c, free(c););
    each(s->cols, c, free(c););
    release(s->ps);
    release(s->names);
    release(s->cols);
    free(s);
}

namedpoints_t* namedpoints_t_init(namedpoints_t* s) {
    if (!s || !display)
        return NULL;

    memset(s, 0, sizeof(namedpoints_t));
    s->ps = alloc(vec_int_t);
    s->names = alloc(vec_str_t);
    s->cols = alloc(vec_str_t);
    s->free = namedpoints_t_free;

    return s;
}

void loci_t_free(loci_t* s) {
    char* c;
    points_t* ps;
    each(s->cols, c, free(c););
    each(s->loci, ps, release(ps););
    release(s->cols);
    release(s->loci);
    free(s);
}

loci_t* loci_t_init(loci_t* s) {
    if (!s || !display)
        return NULL;

    memset(s, 0, sizeof(loci_t));
    s->loci = alloc(vec_points_t);
    s->cols = alloc(vec_str_t);
    s->free = loci_t_free;

    return s;
}

void arcssets_t_free(arcssets_t* s) {
    char* c;
    arcs_t* as;
    each(s->cols, c, free(c););
    each(s->arcssets, as, release(as););
    release(s->cols);
    release(s->arcssets);
    free(s);
}

arcssets_t* arcssets_t_init(arcssets_t* s) {
    if (!s || !display)
        return NULL;

    memset(s, 0, sizeof(arcssets_t));
    s->arcssets = alloc(vec_arcs_t);
    s->cols = alloc(vec_str_t);
    s->free = arcssets_t_free;

    return s;
}
