/************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef DECTYPES_H
#define DECTYPES_H

#include "syntax.h"
#include "cutils.h"

typedef struct sourcedata_t sourcedata_t;
typedef struct token_t token_t;
typedef struct scope_t scope_t;

declare_utils()

dectype_vector(int, vec_int_t)
dectype_vector(token_t*, vec_token_t)
dectype_pair(int, token_t*, sigtoken_t)
dectype_pair(char*, unsigned long, strline_t)
dectype_vector(strline_t*, vec_strline_t)
dectype_vector(sigtoken_t*, vec_sigtoken_t)
dectype_map(int, token_t*, map_sigtoken_t)

typedef struct vec_token_t phrase_t;

#ifdef ENABLE_X11
#include <X11/Xlib.h>

typedef struct arcs_t arcs_t;
typedef struct points_t points_t;
typedef struct namedpoints_t namedpoints_t;
typedef struct loci_t loci_t;
typedef struct arcssets_t arcssets_t;

typedef struct widget_t widget_t;

dectype_pair(const char*, XColor*, strcol_t)
dectype_pair(unsigned long, widget_t*, widwidget_t)
dectype_pair(widget_t*, widget_t*, widgetwidget_t)
dectype_pair(unsigned long, scope_t*, widscope_t)

dectype_vector(widget_t*, vec_widget_t)
dectype_vector(unsigned long, vec_wid_t)
dectype_vector(widwidget_t*, vec_widwidget_t)
dectype_vector(widscope_t*, vec_widscope_t)

dectype_map(const char*, XColor*, map_strcol_t)
dectype_map(unsigned long, int, map_widint_t)
dectype_map(unsigned long, widget_t*, map_widwidget_t)
dectype_vector(strcol_t*, vec_strcol_t)

dectype_map(unsigned long, vec_sigtoken_t*, map_wid_sigcodes_t)
dectype_map(unsigned long, scope_t*, map_widscope_t)
dectype_map(unsigned long, loci_t*, map_widloci_t)
dectype_map(unsigned long, arcssets_t*, map_widarcssets_t)
dectype_map(unsigned long, namedpoints_t*, map_widnamedpoints_t)

dectype_map(int, widget_t*, map_pidwid_t)
dectype_pair(int, widget_t*, pidwid_t)
dectype_pair(unsigned long, loci_t*, widloci_t)
dectype_pair(unsigned long, arcssets_t*, widarcssets_t)
dectype_pair(unsigned long, namedpoints_t*, widnamedpoints_t)

dectype_vector(points_t*, vec_points_t)
dectype_vector(arcs_t*, vec_arcs_t)
dectype_vector(namedpoints_t*, vec_namedpoints_t)
dectype_vector(widloci_t*, vec_widloci_t)
dectype_vector(widarcssets_t*, vec_widarcssets_t)
dectype_vector(widnamedpoints_t*, vec_widnamedpoints_t)

dectype_vector(pidwid_t*, vec_pidwid_t)

dectype_pair(unsigned long, vec_sigtoken_t*, wid_sigcodes_t)
dectype_pair(unsigned long, int, widint_t)

dectype_vector(wid_sigcodes_t*, vec_wid_sigcodes_t)
dectype_vector(widint_t*, vec_widint_t)
#endif

dectype_vector(phrase_t*, vec_phrase_t)
dectype_pair(int, token_t*, exprblock_t)
dectype_vector(exprblock_t*, vec_exprblock_t)

dectype_pair(time_t, token_t*, timetoken_t)
dectype_pair(int, timetoken_t*, int_timetoken_t)
dectype_pair(char*, int, strint_t)
dectype_pair(int, int, intint_t)
dectype_pair(intint_t, intint_t, intint_intint_t)
dectype_pair(char*, char*, strstr_t)
dectype_pair(char*, phrase_t*, strphrase_t)
dectype_pair(char*, token_t*, strtoken_t)

dectype_vector(unsigned int, vec_uint_t)
dectype_vector(char*, vec_str_t)
dectype_vector(strstr_t*, vec_strstr_t)
dectype_vector(int_timetoken_t*, vec_int_timetoken_t)

dectype_vector(long, vec_long_t)
dectype_vector(strphrase_t*, vec_strphrase_t)
dectype_vector(sourcedata_t*, vec_sourcedata_t)
dectype_vector(scope_t*, scopes_t)
dectype_vector(strint_t*, vec_strint_t)
dectype_vector(strtoken_t*, vec_strtoken_t)

dectype_map(char, const char*, map_charcstr_t)
dectype_map(char*, char*, map_strstr_t)
dectype_map(char*, int, map_strint_t)
dectype_map(char*, phrase_t*, map_strphrase_t)
dectype_map(int, timetoken_t*, map_int_timetoken_t)
dectype_map(char*, token_t*, map_strtoken_t)

#endif
