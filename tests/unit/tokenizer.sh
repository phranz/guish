#!/bin/sh -e
#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################
 
if test "$LEAKS" = 1
then
    printf -- '\n---------------------------\n'
    printf -- 'Testing tokenizer for leaks\n'
    printf -- '---------------------------\n\n'
else
    printf -- '\n-----------------\n'
    printf -- 'Testing tokenizer\n'
    printf -- '-----------------\n\n'
fi

ENABLE_X11="$(./guish -b | grep -q '(Xlib): yes' && printf 1)"
ENABLE_CONTROL="$(./guish -b | grep -q '(Xtst): yes' && printf 1)"
ENABLE_IMAGES="$(./guish -b | grep -q '(Imlib2): yes' && printf 1)"

tfunc="$(./guish -b | grep -q 'Debug activated: yes' && printf egtest || printf etest)"
test "$LEAKS" = 1 && tfunc=ltest

. tests/utils.sh

$tfunc -c "puts " "[<cmdline>] at '1': error, invalid character '0x3'."
$tfunc -c "(" "[<cmdline>] at '1': error, unterminated expression!"
$tfunc -c ")" "[<cmdline>] at '1': error, unterminated expression!"
$tfunc -c "{" "[<cmdline>] at '1': error, unterminated block!"
$tfunc -c "}" "[<cmdline>] at '1': error, unterminated block!"
$tfunc -c "puts @(" "[<cmdline>] at '1': error, unterminated expression!"
$tfunc -c "[" "[<cmdline>] at '1': error, unterminated slice expression!"
$tfunc -c "]" "[<cmdline>] at '1': error, unterminated slice expression!"
$tfunc -c "'" "[<cmdline>] at '1': error, unterminated token, expecting '''."
$tfunc -c "\`" "[<cmdline>] at '1': error, unterminated token, expecting '\`'."
$tfunc -c "\"" "[<cmdline>] at '1': error, unterminated token, expecting '\"'."
$tfunc -c "\x" "[<cmdline>] at '1': error, bad hex value."
$tfunc -c "\x10" "[<cmdline>] at '1': error, bad hex value."

if test "$ENABLE_X11"
then
    $tfunc -c "<(" "[<cmdline>] at '1': error, unterminated token, expecting ')'."
fi

printf '\n'
