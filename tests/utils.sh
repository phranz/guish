#!/bin/sh -e
#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################
 
etest () {
    printf '%s' "guish $1 \"$2\": "
    if test "$(./guish $1 "$2" 2<&1)" = "$3"
    then 
        printf 'ok\n'
    else
        printf 'failed\n'
        exit 1
    fi
}

egtest () {
    printf '%s' "guish $1 \"$2\": "
    if ./guish $1 "$2" 2<&1 | grep -Fq -- "$3"
    then 
        printf 'ok\n'
    else
        printf 'failed\n'
        exit 1
    fi
}

otest () {
    printf '%s' "Running '$2': "
    if test "$(./guish $1 "$2")" = "$3"
    then 
        printf 'ok\n'
    else
        printf 'failed\n'
        exit 1
    fi
}

ogtest () {
    printf '%s' "guish $1 \"$2\": "
    if ./guish $1 "$2" | grep -Fq -- "$3"
    then 
        printf 'ok\n'
    else
        printf 'failed\n'
        exit 1
    fi
}

ltest () {
    printf '%s' "guish $1 \"$2\": "
    if ./guish $1 "$2" 2<&1 | grep -Fq -- 'ERROR: LeakSanitizer:'
    then
        printf 'failed\n'
        exit 1
    else
        printf 'ok\n'
    fi
}
