.TH "guish" "1" 
.SH NAME
.BR
.P
guish - A language to make and modify GUIs

.SH SYNOPSIS
.BR
.P
.B guish 
[\fB-c\fR <code>][\fB-s\fR][\fB-q\fR][\fB-k\fR][\fB-t\fR][\fB-f\fR][\fB-v\fR][\fB-b\fR][\fB-h\fR][<file>][<arguments>]


.SH DESCRIPTION
.BR
.P
.B guish
is a language to create and modify GUIs; it can be used to tie different programs together by embedding, or to make simple GUIs.

This is version 2.x, which depends just on Xlib as all toolkits have been removed (there are some optional dependencies).

.SH OPTIONS
.BR
.P
.TP
.B -c <code>
read and execute commands from command line.
.TP
.B -s
display elements when created.
.TP
.B -q
quit when closing last element/window.
.TP
.B -k
terminate all external programs when quitting.
.TP
.B -t
fallback on tty after reading data from other inputs.
.TP
.B -f
show available X11 fonts.
.TP
.B -v
show version.
.TP
.B -b
show build (compiled in) options.
.TP
.B -h
guess.

.SH INPUTS AND SOURCES
.BR
.P
guish reads commands from multiple source sequentially; these source are:
command line (with \fB-c\fR option), file, standard input, and if \fB-t\fR option is given, controlling terminal. 
After reading commands from a source, it tries to switch to another one, and when there are no more sources, it simply stays idle.
.P
After executing from file or command line or standard input,
then finally it switchs to controlling terminal (if \fB-t\fR option is given)
and executes from there.

If the file given is a named pipe, then
it will be opened in non-blocking mode, allowing to issue
commands from the other end of the pipe.

.SH SYNTAX OVERVIEW
.BR
.P
Being a command interpreter, guish has a little set of syntax rules. 
They comprises expressions, commands, quotes and special operators.
Generic commands and signals are common to all elements,
while there are ones which are specific to each element.
A phrase is the execution unit, and ends if
the program encounters a newline-like character, or a semicolon (";").

.SS Comments
.BR
.P
Single line comments begin with a \fB#\fR, and end at newline like character:

.EX
 a = 4 # this is a comment
.EE

Multiline comments instead are embedded inside \fB#[\fR and \fB]#\fR (or
end at the end of source):

.EX
 a = 4 #[ This,
 is a multiline comment,
 ending here. ]# puts "a: @{a}"
.EE

.SS Elements and element expressions
.BR
.P
Elements are basically widgets and have some commands and signals associated with them;
they can be created using element expressions, enclosing element name within
\fB||\fR:

.EX
 |b|+
.EE

In this example, "|b|" is an expression which creates an element
of type button, and returns its X11 window id (ex, "65011718").

The \fB+\fR command will then show the button (all elements are hidden by default
unless \fB-s\fR option is given).

.SS Subject and implied subject
.BR
.P
To refer to the last created/modified element, you can use
it without naming it explicitly, for example:

.EX
 |i|;+
.EE

The "+" command will by applied to element created
by "|i|" expression automatically.

.SS Variables and variable substitution
.BR
.P

We can refer to elements by using variable substitution, instead of using their window ids:

.EX
 bt = |b|
.EE

and then:
.EX
 @bt < "new button"
.EE

.SS Commands
.BR
.P

Commands instead can be of three distinct types: special, generic and normal.

Special commands are meant to change guish behaviour, generic ones are meant to modify an element of any type and normal ones are for a specific element type (every element can have specialized commands).

.SS Signals
.BR
.P

Signals make it possible to run actions on UI changes (or,
more generally, on some events).

.EX
 |b|=>c{run free}
.EE

Here, the user creates a button
that when clicked will make guish execute a system command (free in this case)
(see \fBrun\fR in SPECIAL COMMANDS).

.EX
 |b|<generator =>c{|b|<clone}
.EE

A button which is a factory of buttons (run this with \fB-s\fR option).

Here the evaluation of the code block is done when
the click signal is triggered.

.SS Scopes and closures
.BR
.P
All variables (functions too, as blocks can be assigned to variables) have a specific scope:

.EX
 a = 1
 {b = 2}()
 puts "@{a}:@{b}"
.EE

Here the variable "a" is defined in the global scope, while
"b" is defined in a temporarily block scope; hence just "a"
is printed to stdout.

When accessing reading/defining a variable in a local scope, if
the variable is already defined in the enclosing scope
then that variable is picked to be read/modified:

.EX
 a = 1
 {
     a = 5
     puts@a
 }()
 puts@a
.EE

Here "a" is defined in the global scope, then modified from block scope
and printed from there, and its value doesn't change.

If a variable is defined inside a block, then
all embedded blocks that reference that variable
will get its value at definition time (a closure):

.EX
 {
     a = 1
     return {
         puts @a
     }
 }()()
.EE

.SS Quotes, blocks and functions
.BR
.P

There are single and double quoting: anything embedded inside
single quotes '', is treated literally (no escaping takes place) and as a single token. 

Variable and function interpolations (respectively via "\fB@{...}\fR" and "\fB@(...)\fR")
are used inside double quotes "", external command substitution quotes ``
and external window id substitution \fB<(...)\fR.

Escaping ("\\n\\t\\r\\f\\v\\b") takes place only inside double quotes "".

.EX
 a = 'my string'; puts "this is:\t @{a}"
 puts "sum of 1 + 5 is: @(add(1, 5))"
.EE

Anything embedded inside \fB{}\fR is treated as a code block
and no variable substitution is done at definition time.

To "execute" a block, simply use parens \fB()\fR.
If arguments are given, they can be referenced inside block by using \fB@n\fR, where "n" is a number
which represents a specific argument by position, with indexes starting at 1 (works with command line parameters too).
To refer to all arguments (at once) given to the block use \fB@*\fR operator
(when in string interpolation, argument separator is a space).

.EX
 v = myvar
 a = {puts "here is @{v}"}
 a()
.EE

Here the variable "v" is not substituted when assigning the block
to "a"; it's substituted later, when function "a" is called.

.EX
 {
     puts join("string length is: ", len(@1))
 }("home")
.EE

Here instead, the block is defined and executed immediately, using "home"
as the first argument.

Being a block a piece of code, it can be used to define functions
by simply being assigned to a variable:

.EX
 fn = {return add(@1, @2)}
 puts join("my func res:", fn(2, 4)) 
.EE

In addition, when defining a new function, it's possible
to "overwrite" the builtin functions in the current scope.

When returning from a function, instead, it's possible to
return multiple arguments (the remaining phrase) to the caller:

.EX
 fn = {return 1 2 3}
 puts join(fn())
.EE

.SS Conditionals
.BR
.P
Anything equal to 0, "0" or empty (like \fB''\fR, \fB""\fR, \fB{}\fR) is false, true otherwise.
This is useful with \fBif\fR and \fBelse\fR commands:
.P
.EX
 if 1 { |b|<text } else { |b|<neverhere }
.EE
or
.EX
 if ge(@a,4) { |l|<4 }
.EE

.SS Loops and iteration
.BR
.P
Here an example of a \fBwhile\fR loop:

.EX
 a = 1
 after 4 {a = 0}
 while {eq(@a, 1)} {
     wait 1 puts 'true'
 }
 puts 'end'
.EE

And here another one using \fBeach\fR function:

.EX
 each({puts @1}, 1, 2, 3, 4, 5})
.EE

And another one of a \fBfor\fR loop:

.EX
 for x in 1 2 3 4: { puts @x }
.EE

.SS Tail recursion optimization (TCO)
.BR
.P
Since \fB2.2.1\fR version, guish supports tail recursion optimization too,
effectively turning a tail recursive function into a loop:

.EX
 upto = {
     if gt(@1, @2) {
         return
     }
     puts @1
     return upto(add(@1, 1), @2)
 }
 upto(1, 7)
.EE

To use TCO, the last phrase of a function must use \fBreturn\fF command
directly (not inside another block like if-else).

.SS External window id substitution
.BR
.P
Everything inside \fB<( )\fR is recognized as an
external command (X11 GUI), forked and executed, and its window id
is substituted (using _NET_WM_PID).

Note that this method will not work with programs that fork.
.P
For example:

.EX
 a = <(xterm)
.EE

xterm program is spawn, and can be driven (almost) like
a normal element.

.SS External command substitution
.BR
.P
Everything inside \fB``\fR is recognized as an
external command and executed.
Then the command output (STDOUT) is substituted inside
source code and interpreted after that.
.P
For example:
.P
.EX
 |b|<`echo clickme`
.EE

.SS Slicing
.BR
.P
Anything inside \fB[]\fR  is trated as a slice expression.
The usage is \fB[<n>[,<x>]]\fR, when <n> is an index, and <x>
is an optional end index (always inclusive); if the preceding
argument is a regular token, then the expression will return
its characters as specified by index(es), otherwise if it's
a block, the expression will return its tokens:

.EX
 puts {1, 2, 3, 4, {a, b}, cat}[4][1]
.EE

The output of this example is 'b'.
.P
If the index(es) given are out of range, an empty token
is returned.

.SH SPECIAL VARIABLES
.BR
.P
.TP
.B SW
primary screen width, in pixels.
.TP
.B SH
primary screen height, in pixels.
.TP
.B X
pointer's x coord.
.TP
.B Y
pointer's y coord.
.TP
.B self
variable holding the window id when in signal code.
.TP
.B args
refers to the block in which there are positional arguments and
function arguments (alternative syntax).
.TP
.B FILE
variable holding the path of current source file.
.TP
.B LINE
variable holding current line number at "that" point in source code.

.SH ENVIRONMENT VARIABLES
.BR
.P
.TP
.B GUISH_MAXWIDWAIT
the maximum number of seconds to wait when applying external window id substitution
(defaults to 3 seconds)

.SH ELEMENTS
.BR
.P

Available elements are:

.TP
.B b
A button.
.TP
.B i
An input box.
.TP
.B l
A label.
.TP
.B p
A page (container of other elements; show and hide are applied to all subelements).
.TP
.B c
A checkbox.
.TP
.B t
A label with a totally transparent background (requires a compositor).

.SH SPECIAL COMMANDS
.BR
.P
Special commands are not tied to elements, they are like statements.

.TP
.B => <signal> <subcmd> 
register a sub-command <subcmd> to run when <signal> triggers.
For normal signals (eg. signals tied to elements), there must
exist an implied subject.
.TP
.B !> <signal>
unregister a sub-command <subcmd> previously registered on signal <signal>.
.TP
.B q
quit guish (exit status 0).
.TP
.B exit <status>
quit guish (exit status <status>).
.TP
.B cd <path>
change current working directory using <path>.
.TP
.B run <cmd>
execute shell command <cmd>.
.TP
.B puts [<...>]
prints remaining phrase to stdout with a newline added.
.TP
.B p [<...>]
prints remaining phrase to stdout.
.TP
.B e [<...>]
prints remaining phrase to stderr.
.TP
.B unset <name|num|wid> [<attr>]
unsets a variable (<name>) or timed scheduled procedure registered with <num>, see
.B every
command.
If, instead of <name|num>, an element id <wid> is given and
a name attribute <attr> is given, deletes that element attribute.
.TP
.B source <file>
execute commands from file.
.TP
.B vars [<wid>]
shows all variables present in the current scope or,
if <wid> element id is given, shows all element attributes
(uses stderr).
.TP
.B ls
display all existing widgets (stderr).
.TP
.B del <wid>
delete a widget with id <wid>.
.TP
.B every <seconds> <block>
schedule <code> to run after <seconds> seconds are elapsed.
.TP
.B after <seconds> <block>
schedule <block> to run once after <seconds> seconds are elapsed.
.TP
.B wait <seconds>
stop command execution and wait <seconds> seconds before resuming (accepts decimal values too).
XEvent handling, schedules actions and signal execution are unaffected by this option.
.TP
.B if <condition> <block>
executes <block> if condition evaluates to true (see Conditionals).
.TP
.B unless <condition> <block>
executes <block> if condition evaluates to false (see Conditionals).
.TP
.B else <block>
executes <block> if last conditional command was successful (see Conditionals).
.TP
.B return <phrase>
when used inside a function, returns all its arguments (the remaining phrase)
to the caller.
.TP
.B while <condition> <block>
executes <block> until <condition> evaluates to true (see Conditionals).
.TP
.B until <condition> <block>
executes <block> until <condition> evaluates to true (see Conditionals).
.TP
.B for [<var1>, <var2>, ...] in [<val1>, <val2>, ...] : <block>
executes the block <block> for each group of variables.
.TP
.B break
exit from current loop.
.TP
.B rel <element1> <element2> <alignment>
relates <element1> to <element2>, moving <element1> near <element2> using <alignment> (see alignment in STYLE AND ATTRIBUTES
section, as <alignment> opts are similar) every time <element2> is moved.
If "0" is specified as alignment, the relation is deleted.
.TP
.B pass [<args>]
do nothing, consuming remaining arguments.
.TP
.B send <wid> <keysequence>
send a keysequence to an element whose id si <wid> (must be enabled at compilation time).
.TP
.B ctrl <wid> <keysequence>
send control key sequence to an element whose id si <wid> (must be enabled at compilation time).

.SH GENERIC COMMANDS
.BR
.P
Generic commands are applicable to any element, regardless
of its type (there are exceptions though).

.TP
.B G
element is undetectable in taskbar.
.TP
.B F
resize the element to fit the entire screen.
.TP
.B d
restore element's default window attributes.
.TP
.B !
bypass window manager.
.TP
.B !!
follow window manager as default.
.TP
.B n
center element in its parent.
.TP
.B -
hide element.
.TP
.B +
show element.
.TP
.B c
click element.
.TP
.B f
focus element.
.TP
.B t
make element stay at top.
.TP
.B b
make element stay at bottom.
.TP
.B x
hide element, or quit guish if quit-on-last-close is on and the element is the last closed one.
.TP
.B l
lower the element.
.TP
.B r
raise the element.
.TP
.B M
maximize the element.
.TP
.B D
disable the element.
.TP
.B E
enable the element.
.TP
.B o
fits element's size to its content.
.TP
.B w
resize an element in right-bottom directions to fit its parent, respecting limits of other elements.
.TP
.B nfill
resize an element in right-bottom directions to fit its parent.
.TP
.B rfill
resize an element in right direction to fit its parent, respecting limits of other elements.
.TP
.B nrfill
resize an element in right direction to fit its parent.
.TP
.B bfill
resize an element in bottom direction to fit its parent, respecting limits of other elements.
.TP
.B nbfill
resize an element in bottom direction to fit its parent.
.TP
.B L
clear element's data.
.TP
.B : <title>
set element title.
.TP
.B s <text>
set element style (see STYLE AND ATTRIBUTES section).
.TP
.B z <w> <h>
resize element by width and height.
.TP
.B m <x> <y>
move element to coords <x> <y>.
.TP
.B / [<l|L|a|A|p|x|n> [<...>]]
draws/fills lines, points and arcs depending on operation (See Drawing operations subsection).
Origin coordinates correspond to the bottom-left corner as default Cartesian axes
 (instead of upper-left one used for windows/elements).
If no operation is given, then all drawings are discarded from the implied element.
.TP
.B A <element> <alignment>
moves implied element to <element> using <alignment> (see alignment in STYLE
AND ATTRIBUTES section, as <alignment> opts are similar).
.TP
.B < <text>
set element text using <text>.
.TP
.B <+ <text>
add additional text to element text using <text>.
.TP
.B > <var>
define a variable named <var> using element text to set its value.
.TP
.B g
enable/disable (toggle) moving the parent of the element by click-and-drag it.
Enabling this will automatically exclude x/y moving flags below.
.TP
.B X
enable/disable (toggle) moving an element inside its parent by click-and-drag on x axis
Enabling this will automatically exclude the flag to click-and-drag and move parent.
.TP
.B Y
enable/disable (toggle) moving an element inside its parent by click-and-drag on y axis
Enabling this will automatically exclude the flag to click-and-drag and move parent.

.SS Drawing operations
.BR
.P

.TP
.B l [<color> <x1> <y1> <x2> <y2> [<...>]]
draws lines between given points using color <color> (beware this command consumes all the phrase).
If no arguments are given, then all element lines are discarded.
.TP
.B L [<color> <x1> <y1> <x2> <y2> [<...>]]
fills the polygon described by given points using color <color> (beware this command consumes all the phrase).
If no arguments are given, then all filled polygons are discarded.
.TP
.B a [<color> <x> <y> <w> <h> <alpha> <beta> [<...>]]
draws an arc using color <color> and whose "center" is at <x> <y>, major and minor axes are respectively
<w> and <h>, start and stop angles are <alpha> and <beta> (consumes all the phrase).
If no arguments are given, then all element arcs are discarded.
.TP
.B A [<color> <x> <y> <w> <h> <alpha> <beta> [<...>]]
fills an arc using color <color> and whose "center" is at <x> <y>, major and minor axes are respectively
<w> and <h>, start and stop angles are <alpha> and <beta> (consumes all the phrase).
If no arguments are given, then all element arcs are discarded.
.TP
.B p [<color> [<...>]]
draws given points using color <color> (beware this command consumes all the phrase).
If no arguments are given, then all points are discarded.
.TP
.B x [<color> [<...>]]
draws given pixels using color <color> (beware this command consumes all the phrase).
If no arguments are given, then all pixels are discarded.
.TP
.B n [<color> <name> <x> <y>]
draws given point using color <color> and putting the text <name> at that point.
If no arguments are given, then all points are discarded.

.SH NORMAL COMMANDS
.BR
.P

.SS checkbox commands
.TP
.B C
check.
.TP
.B U
uncheck.

.SS input commands
.TP
.B S
show input data as normal while typing.
.TP
.B H
hide input data while typing.
.TP
.B P
use password mode, displaying just asterisks.
.TP
.B W
toggles "go to the next line" when hitting return (triggers return signal anyway).

.SS page commands
.TP
.B Q
make subelements equals (in size).
.TP
.B S <style>
style all subelements.
.TP
.B P
free all embedded elements.
.TP
.B << <element>
embeds element (or an external client).
.TP
.B <<< <element>
embeds element (or an external client), fitting the page to its content.
.TP
.B >> <element>
free element, reparenting it to root window.
.TP
.B >>> <element>
free element, reparenting it to root window and fitting the page to its content.
.TP
.B v
set vertical layout readjusting all subwidgets.
.TP
.B h
set horizontal layout readjusting all subwidgets.
.TP
.B Z <e1> <e2>
swaps sub-elements position.
.TP
.B N
inverts the order of sub-elements.

.SH SPECIAL SIGNALS
.BR
.P

.TP
Special signals are independent from elements, and are tied to internal guish events.

.TP
.B q
triggered at program exit.
.TP
.B t
triggered when program receives a SIGINT or a SIGTERM.

.SH GENERIC SIGNALS
.BR
.P

.TP
Generic signals are common to all elements.

.TP
.B x
triggered when element is closed.
.TP
.B c
triggered when element is clicked.
.TP
.B lc
triggered when element is left-clicked.
.TP
.B rc
triggered when element is right-clicked.
.TP
.B mc
triggered when element is middle-clicked.
.TP
.B cc
triggered when element is double clicked.
.TP
.B lcc
triggered when element is double left-clicked.
.TP
.B rcc
triggered when element is double right-clicked.
.TP
.B mcc
triggered when element is double middle-clicked.
.TP
.B p
triggered when element is pressed.
.TP
.B lp
triggered when element is left-pressed.
.TP
.B rp
triggered when element is right-pressed.
.TP
.B mp
triggered when element is middle-pressed.
.TP
.B r
triggered when element is released.
.TP
.B lr
triggered when element is left-released.
.TP
.B rr
triggered when element is right-released.
.TP
.B mr
triggered when element is middle-released.
.TP
.B m
triggered when element is moved.
.TP
.B s
triggered when element scrolled down.
.TP
.B S
triggered when element scrolled up.
.TP
.B z
triggered when element is resized.
.TP
.B e
triggered when mouse pointer "enters" the element.
.TP
.B l
triggered when mouse pointer "leaves" the element.
.TP
.B f
triggered when focusing the element.
.TP
.B u
triggered when un-focusing the element.

.SH NORMAL SIGNALS
.BR
.P

.TP
Normal signals are per element.
.TP

.SS checkbox signals
.TP
.B U
triggered when checkbox is unchecked.
.TP
.B C
triggered when checkbox is checked.

.SS input signals
.TP
.B R
triggered when input has focus and "return" is hit.

.SH REDUNDANT TOKENS
.BR
.P
These tokens are ignored: "\fB,\fR", "\fB->\fR".

.SH EVALUATION ORDER AND SUBSTITUTIONS
.BR
.P

Every time a new phrase is evaluated, it goes through
a series of special substitutions/evaluations before it's commands
are interpreted.

The evaluation order is: hex substitution (at tokenizer level), evaluation of expressions
(where code evaluation/execution, substitutions and functions are computed),
evaluation of special commands and execution of generic/normal commands.

Moreover if after the execution phase (the last one) the phrase is not empty,
the evaluation cycle will restart from evaluation of expressions phase,
and it will continue until there are no more tokens in the phrase.

Every phrase is reduced to an empty phrase while evaluating:

.EX
 a=234;{i1=|i|;<'input1'+}();{i2=|i|;<'input2'+}()|b|<btn+
.EE

This example is composed by 2 phrases, and
the code block in each phrase is executed before each assignment.

.SS Hex substitution
.BR
.P

Non quoted tokens are subject to hex substitution: if a "\\x" plus 2 hexadecimal characters is found, it's substituted with corresponding ascii characters.

.EX
 puts \\x68\\x6F\\x6D\\x65
.EE

Here, the string "home" is printed.

.SS Globbing
.BR
.P
If a "\fB*\fR" is given, then all widgets wids are
substituted.

.EX
 |b||b||b|+
 puts *
.EE

.SS Variable substitution
.BR
.P
With the \fB=\fR operator (actually, it's a special statement command), it's possible
to assign values to a variable, reusing it later by simply referencing it
using \fB@\fR operator when not inside quotes or by wrapping it
inside \fB@{}\fR when in double quotes, shell command substitution quotes \fB``\fR,
or external window id substitution \fB<( )\fR.

.EX
 b = 123; puts @a
.EE

There are two methods to define/create empty variables: by explicitely assing
an empty string to a variable (ex. a = "") or by simply omit the
value (ex. a =).

In addition, if there is more than one value to assign, a block is automatically
created (embedding those values) and assigned to that variable:

.EX
 a = 1              # this simply assigns '1' to the variable 'a'
 b = 1, 2, 3, 4     # this instead assigns the block '{1, 2, 3, 4}' to the variable 'a'
 c = {1, 2, 3, 4}   # same but explicit
.EE

Each block has it's own scope, and variable resolution works by searching from
the last scope to the first.
Ex:

.EX
 a = 1
 puts(@a)
 {
     a=345
     b=6534
 }()
 puts@a
 puts"b:@{b}"
.EE

In the last example, a is set to 1 and printed, then it's
changed to 345 from another scope, in which another variable
(b) is set.
After code block, just "a" is updated, and "b" doesn't exist anymore.

For example:
.P
.EX
 gname = MY_GRIP_NAME
 |l|<@gname
.EE

or

.EX
 gname = MY_GRIP_NAME
 name = 'random name'
 puts "@{gname} is maybe @{name}"
.EE

.SS Element expressions
.BR
.P

Anything inside \fB||\fR is an element expression; a widget of a given
element is created and its X11 window id substituted.
The synopsis is: |<element>[{<width>, <height>}]|
Ex.

.EX
 |b|+
.EE

If an integer is given, instead of one of available element types, then the program tries to find an existing program having that integer as window id.
Ex.

.EX
 |12341234|-
.EE

This creates a widget ("external") for the external program and hides it.

.SS Shell command substitution
.BR
.P

Anything inside \fB``\fR is treated as a shell
command, and it's output is substituted.

.EX
 d = `date`
 ols = `ls -l`
.EE

.SS External window id substitution
.BR
.P
Everything inside \fB<( )\fR is recognized as an
external command (X11 GUI), forked and executed, and its window id
is substituted (using _NET_WM_PID).
.P
For example:

.EX
 a = <(xterm)
.EE

xterm program is spawn, and can be driven (almost) like
a normal element.

.SH OPERATORS
.BR
.P
.SS Binary
.BR
.P
.TP
.B <s> \.\. <e>
returns integers starting at <s> and ending at <e> (inclusive)
as multiple tokens.
.TP
.B <var> = [<val>, [<val1>, ...]]
defines a variable (consumes all the phrase).
If no value is given, an empty token is assigned to the variable.
If a single value is given, that value is assigned to the variable.
If multiple values are given, then all these values are wrapped
inside a block, and this block is assigned to the variable.
.TP
.B <attr> .= [<val>, [<val1>, ...]]
defines an element using the implied subject attribute (consumes all the phrase).
If no value is given, an empty token is assigned to the variable.
If a single value is given, that value is assigned to the variable.
If multiple values are given, then all these values are wrapped
inside a block, and this block is assigned to the variable.

.SS Unary
.BR
.P
.TP
.B @<varname|num>
dereferences a variable name (or positional argument).
.TP
.B @*
returns all function parameters as tokens (usable with command line parameters too).
.TP
.B [<eid>]\.<attr>
dereferences an element attribute; if <eid> is given, uses
that element, otherwise uses implied subject.



.SH ELEMENT ATTRIBUTES
.BR
.P

Every element can have some default readonly attributes and a 
variable number of custom attributes (which can be set by using assignment
only, not by using \fBlet\fR function). To set an attribute, use
the "\fB.=\fR" operator; to get it instead use the "\fB.\fR" operator.

.EX
 b = |b|; myattr .= 'here'; puts(@b.myattr)
.EE

In the last example, a custom attribute, "myattr" is
created and used.

The following are default attributes (readonly):
.TP
.B t
widget's type.
.TP
.B w
widget's width.
.TP
.B h
widget's height.
.TP
.B x
widget's x coord.
.TP
.B y
widget's y coord.
.TP
.B b
widget's border width.
.TP
.B g
widget's margin width.
.TP
.B d
widget's text data.
.TP
.B T
widget's title.
.TP
.B c
widget's checked/unchecked status (only for checkbox).
.TP
.B n
widget's number of subwidgets (only for page).
.TP
.B s
widget's subwidgets ids (one token each, only for page).
.TP
.B pid
process ID associated with the widget.
.TP
.B v
widget is visible.
.TP
.B e
widget is enabled (freezed/unfreezed).
.TP
.B f
widget is focused.

.SH BUILTIN FUNCTIONS
.BR
.P
Symbol "\fB...\fR" means a variable number of arguments.

.TP
.B exists(<eid>)
returns 1 if a widget with id <eid> exists, 0 otherwise.
.TP
.B read([<file>])
reads and returns a line (excluding newline) from standard input;
if an existing file is given, reads and returns all its content.
Beware that this function blocks the GUI events, and returns nothing
when reading from stdin and source is non-blocking.
.TP
.B write(<text>, <file>)
writes text into file and returns the number of characters written.
Creates the file if it doesn't exist yet.
.TP
.B append(<text>, <file>)
append text to the end of file and returns the number of characters written.
Creates the file if it doesn't exist yet.
.TP
.B eval(...)
evaluates code by first stringifying all given arguments
and then returns the result of evaluation if any.
Beware that this function runs in the "current" scope,
and can modify it.
.TP
.B builtin(<func>, ...)
gets the name of a builtin function and a variable number of arguments, then calls
the builtin function with those arguments and returns the result (if any).
It's useful when overriding builtin functions.
.TP
.B each(<function>, ...)
executes <function> for each additional argument given passing it as the
first argument to the block. If return values are present, they
will be accumulated and then returned.
.TP
.B env(<var>)
returns the value of the environment variable "var".
.TP
.B cwd()
returns the value of the current working directory.
.TP
.B rev([<block>], ...)
if a block is given as first argument, its element will be
returned in reverse, otherwise \fBrev\fR will return all its arguments in reverse.
This function is somewhat special, as when there are no arguments to get,
it'll return nothing (statement behaviour).
.TP
.B let([<var>, <val>], ...)
sets variables, works exactly like assignment with special operator \fB=\fR,
but in expressions. This function is somewhat special, it'll return nothing (statement behaviour).
.TP
.B if(<cond>, [<v1>, [<v2>]])
if <cond> is true and <v1> is given, returns <v1>, else if <cond> is false and <v2> is given,
returns <v2>.
.TP
.B unless(<cond>, [<v1>, [<v2>]])
if <cond> is false and <v1> is given, returns <v1>, else if <cond> is true and <v2> is given,
returns <v2>.
.TP
.B and(...)
returns the first true argument; if there are no true
arguments, returns the last one which is false.
The function evaluates any block given.
.TP
.B or(...)
returns the last true argument if all arguments are true, otherwise
returns the first false argument.
The function evaluates any block given.
.TP
.B flat(...)
returns all given arguments; if a block is found, then it is flatted.
.TP
.B block(...)
returns a code block, embedding the given arguments into \fB{\fR\fB}\fR.
.TP
.B some(...)
returns given arguments. If nothing is given, returns an empty token.
.TP
.B puts(...)
prints given arguments to stdout.
This function is somewhat special, it'll return nothing (statement behaviour).
.TP
.B push(...)
pushes given arguments to current function arguments (works with command line parameters too).
This function is somewhat special, it'll return nothing (statement behaviour).
.TP
.B pushb(...)
pushes given arguments to current function arguments from the beginning (works with command line parameters too).
This function is somewhat special, it'll return nothing (statement behaviour).
.TP
.B pop()
pops the last argument from function arguments (works with command line parameters too).
This function is somewhat special, as if there are no arguments to pop,
it'll return nothing (statement behaviour).
.TP
.B popb()
pops the first argument from function arguments (works with command line parameters too).
This function is somewhat special, as if there are no arguments to pop.
.TP
.B times(<n>, <arg>)
returns a sequence of tokens made by <n> times <arg>.
This function is somewhat special, as when there are 0 tokens to replicate,
it'll return nothing (statement behaviour).
.TP
.B get(<name>)
returns the value of the variable with name <name>, or an empty
token if the variable does not exist.
.TP
.B true(<arg>)
returns 1 if <arg> is true, 0 otherwise.
.TP
.B false(<arg>)
returns 0 if <arg> is true, 1 otherwise.
.TP
.B in(<n>, <heap>)
returns 1 if <n> is found in <heap>, 0 otherwise;
the arguments can be of any type (single tokens and blocks).
.TP
.B join(...)
joins blocks and/or tokens by applying the following rules to
all arguments given, and accumulates the result as the first operand.
If the operands are blocks, then a single new block is created by joining them;
if the operands are tokens, then a single new token is created by joining them,
and its type will be that of the "second" token; if the operands are mixed
(eg. a block and a token), then the token will be embedded inside the block.
.TP
.B isdef(<token>)
returns 1 if <token> is a variable, 0 otherwise.
.TP
.B isvar(<token>)
returns 1 if <token> is a (type) variable, 0 otherwise.
.TP
.B isfunc(<token>)
returns 1 if <token> refers to a function, 0 otherwise.
.TP
.B isblock(...)
returns 1 if just one argument is given and is a block, 0 otherwise.
.TP
.B isint(...)
returns 1 if just one argument is given and is an integer, 0 otherwise.
.TP
.B len(<arg>)
if a block is given, returns the number of its element, otherwise returns
the number of characters of the token.
.TP
.B split(<token>, <sep>)
splits "token" using separator "sep" and returns resulting tokens
having their type equal to that of "token".
.TP
.B csplit(<token>, <sep>)
splits "token" using separator "sep" and returns resulting tokens
as normal commands.
.TP
.B seq(<t1>, <t2>, ...)
returns 1 if all arguments are equal (string comparison), 0 otherwise.
.TP
.B add(...)
perform addition.
.TP
.B sub(...)
perform subtraction.
.TP
.B mul(...)
perform multiplication
.TP
.B div(...)
perform division.
.TP
.B mod(...)
perform modulus.
.TP
.B rand()
returns a random positive integer.
.TP
.B sqrt(<n>)
returns the square root of <n>.
.TP
.B cbrt(<n>)
returns the cube root of <n>.
.TP
.B pow(<n>, <e>)
returns the power of <n> raised to <e>.
.TP
.B log(<n>)
returns the base 10 logarithm of <n>.
.TP
.B ln(<n>)
returns the natural logarithm of <n>.
.TP
.B sin(<n>)
returns the sine of <n> (degrees).
.TP
.B cos(<n>)
returns the cosine of <n> (degrees).
.TP
.B tan(<n>)
returns the tangent of <n> (degrees).
.TP
.B hex(<n>)
returns <n> in its hexadecimal representation.
.TP
.B int(<n>)
returns integral part of given number <n>; rounds to nearest integer.
.TP
.B xor(<n1>, <n2>, ...)
perform bitwise XOR.
.TP
.B band(<n1>, <n2>, ...)
perform bitwise AND.
.TP
.B bor(<n1>, <n2>, ...)
perform bitwise OR.
.TP
.B lsh(<n1>, <n2>, ...)
perform bitwise left shift.
.TP
.B rsh(<n1>, <n2>, ...)
perform bitwise right shift.
.TP
.B not(<n>)
perform negation
.TP
.B eq(<n1>, <n2>, ...)
equal-to
.TP
.B ne(<n1>, <n2>, ...)
not-equal-to
.TP
.B lt(<n1>, <n2>, ...)
less-than
.TP
.B gt(<n1>, <n2>, ...)
greater-than
.TP
.B le(<n1>, <n2>, ...)
less-equal-than
.TP
.B ge(<n1>, <n2>, ...)
greater-equal-than
.TP
.B abs(<n>)
perform absolute value
.TP
.B neg(<n>)
unary minus.

.SH STYLE AND ATTRIBUTES
.BR
.P
Each element has some properties/attributes which can be set by using style command. This command takes a string that has
a format like CSS:

.EX
 |l| s 'bg: blue; fg: white'
.EE

Here, we create a label with a blue background and white foreground.

Each field must be separated by newlines, \fB;\fR or \fB|\fR.
Colors can be specified by using a common shortname, such as "yellow", or by using RGB value, such as "#ff32ae".

.TP
.B background | bg: <color|/<path to image>>
set background color or a background image by specifying image path;
if the string "null" is given as <path to image>, current image is
removed. (Background image loading requires building guish with Imlib2 support.)
.TP
.B color | foreground | fg: <color>
set foreground color
.TP
.B pressed-background | pbg: <color>
background color when element is pressed
.TP
.B pressed-color | pfg: <color>
foreground color when element is pressed
.TP
.B hovered-background | hbg: <color>
background color when element is hovered
.TP
.B hovered-color | hfg: <color>
foreground color when element is hovered
.TP
.B border-color | bc: <color>
set border color
.TP
.B width | w: <value in pixels>
set width
.TP
.B height | h: <value in pixels>
set height
.TP
.B border | b: <value in pixels>
set border width
.TP
.B line | l: <value in pixels>
set line width (use with "/" command)
.TP
.B margin | g: <value in pixels>
set margin width
.TP
.B mode | m: <expanding mode>
set expanding mode type (See Expanding mode)
.TP
.B align | a: <alignment>
set alignment type (See Alignment)
.TP
.B f | font: <font name>
set font type using a X11 font name

.SS Expanding mode
.BR
.P
.TP
.B fixed | f
width and height are fixed (default for all elements)
.TP
.B wfixed | w
width is fixed, height can change
.TP
.B hfixed | h
height is fixed, width can change
.TP
.B relaxed | r
width and height can change

.SS Alignment
.BR
.P
Any element that's not a page has a particular text alignment
that can be changed.
If an alignment is specified for a page element instead,
(whose defaults alignments are top-center for horizontal
layout, and middle-left for vertical one),
then its sub-elements will be aligned accordingly, depending from page
layout type too.

.TP
.B l | left | middle-left
show text at middle left
.TP
.B r | right | middle-right
show text at middle right
.TP
.B c | center | middle-center
show text at middle center
.TP
.B tl | top-left
show text at top left
.TP
.B tr | top-right
show text at top right
.TP
.B t | top-center
show text at top center
.TP
.B bl | bottom-left
show text at bottom left
.TP
.B br | bottom-right
show text at bottom right
.TP
.B b | bottom-center
show text at bottom center

.SH AUTHOR
.BR
.P
Francesco Palumbo <phranz.dev@gmail.com>

.SH THANKS
.BR
.P
La vera Napoli, Carme, Pico, Titina, Molly, Leo, i miei amati nonni,
mio padre, mia madre, e tutti coloro su cui ho potuto e posso contare.
Grazie mille.

.SH LICENSE
.BR
.P
.B GPL-3.0-or-later
see COPYING
